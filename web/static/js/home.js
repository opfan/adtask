/**
 * @name home.js
 * @author chenls
 * @date 2021/2/8 15:13
 * @description: 首页脚本
 */

var home = new Vue({
  el: '#home',
  components: {
    'banner-swiper': httpVueLoader('./static/js/vue-components/BannerSwiper.vue?v=' + STATIC_VERSION),
    'banner-footer': httpVueLoader('./static/js/vue-components/BannerFooter.vue?v=' + STATIC_VERSION),
    'nearby-map': httpVueLoader('./static/js/vue-components/NearbyMap.vue?v=' + STATIC_VERSION),
    'footer-nav': httpVueLoader('./static/js/vue-components/FooterNav.vue?v=' + STATIC_VERSION),
    'share-button': httpVueLoader('./static/js/vue-components/ShareButton.vue?v=' + STATIC_VERSION),
  },
  data: {
    wrapContentH: null,
    homeSwiper: null,
    activeIndex: 0,
    mapBox: false,
    bannerSwiperStop: false,
    adListData: { data: [] },
    loading: false,
    bannerAdData: { data: [] },
    currentBannerAdId: null,
  },
  computed: {
    bannerSwiper: function () {
      return this.findDescendants('BannerSwiper');
    },

    indexAdList: function () {
      return (this.adListData.data || []);
    },

    currentBannerAdData: function () {
      var _this = this;

      return _this.bannerAdData.data.find(function (item) {
        return item.ad_id === _this.currentBannerAdId;
      });
    }
  },
  watch: {
    activeIndex: function (val, oldVal) {
      this.$nextTick(function () {
        var bannerRef = this.$refs.adBannerRef || {};
        var adListRef = (!isEmpty(this.$refs.adListRef) && (Array.isArray(this.$refs.adListRef) ? this.$refs.adListRef : [this.$refs.adListRef])) || [];
        var isBanner = adListRef.every(function (item) {
          return item.uid !== val;
        });

        if (isBanner) {
          bannerRef.swiperStart();
        } else if (bannerRef.uid === oldVal) {
          bannerRef.stop();
        }

        adListRef.forEach(function (item) {
          if (item.uid === oldVal) {
            item.stop();
          }
          if (item.uid === val) {
            item.swiperStart();
          }
        });
      });
    },
  },
  created: function () {
    var _this = this;

    Promise.all([
      _this.getBannerAd(),
      _this.getAdList(),
    ]);

    // 新用户注册进入提示奖励红包
    if (window.register) {
      var toast = _this.$createToast({
        time: 5000,
        txt: '恭喜您获得' + window.money + '元红包奖励请在个人中心查看。',
        type: "txt",
      });
      toast.show();
    }

  },
  mounted: function () {
    var _this = this;

    setTimeout(function () {
      _this.homeSwiper = new Swiper('#home-swiper', {
        loop: false,
        observer: true,
        slidesPerView: 'auto',
        direction: 'vertical',
        on: {
          slideChangeTransitionStart: function () {
            if (this.activeIndex) {
              _this.activeIndex = _this.indexAdList[this.activeIndex - 1].ad_id;
            } else {
              _this.activeIndex = _this.currentBannerAdId;
            }
          },
          transitionEnd: function () {
            if (this.activeIndex > 0) {
              _this.mapBox = true;
            } else {
              _this.mapBox = false;
            }
            if (this.progress >= 0.8) {
              // 加载分页
              if (_this.adListData.last_page > _this.adListData.current_page && !_this.loading) {
                _this.getAdList(_this.adListData.current_page + 1);
              }
            }
          }
        },
      });

      // 注册监听swiper组件实例操作
      _this.$bus.$on('handlerBannerSwiper', function (status) {
        _this.bannerSwiperStop = status;
      });
    }, 500);
  },
  methods: {
    findDescendants: function (componentName) {
      if (!componentName) return [];
      var descendants = findDescendants.call(this, componentName, this.$children);
      return descendants.length
        ? descendants
        : [];
    },
    updateSwiper: function () {
      this.$nextTick(function () {
        this.homeSwiper.update(true);
      });
    },
    getBannerAd: function (page) {
      var _this = this;
      var current_page = page || 1;
      var ad_id = getQueryStr('ad_id') || null;

      $http.get('/index.php?s=api/index/list&page=' + current_page + '&listRows=2', ad_id ? { ad_id: ad_id } : {})
        .then(function (res) {
          var _data = res.data;
          var adData = [];

          // 拼接每个广告，扁平广告图片数据
          _data.data.forEach(function (ad) {
            var m = ad;
            var url = ad.url;
            delete m.url;

            url = url.map(function (a) {
              return Object.assign(a, m);
            });

            adData = [].concat(adData, url);
          });

          _this.bannerAdData = Object.assign(_data, { data: [].concat(_this.bannerAdData.data, adData) });

          _this.$nextTick(function () {
            if (_data.current_page === 1 && _data.data.length) {
              _this.activeIndex = _data.data[0].ad_id;
              _this.currentBannerAdId = _data.data[0].ad_id;
            }
          })

        });
    },
    getAdList: function (page) {
      var _this = this;
      var current_page = page || 1;

      this.loading = true;

      $http.get('/index.php?s=api/index/list&page=' + current_page, { origin: 1 })
        .then(function (res) {
          var data = res.data;

          _this.adListData = Object.assign(data, {
            data: [].concat(_this.adListData.data, data.data),
          });
        })
        .catch(function () { })
        .then(function () {
          _this.$nextTick(function () {
            _this.loading = false;
          });
        });
    },
    getAddress: function (item) {
      var addres = item.address || [];

      if (addres.length) {
        var data = addres[0];

        return data.address;
      }
      return null;
    },

    transitionEnd: function (item, currentSwiper) {
      var _this = this;
      this.currentBannerAdId = item.ad_id;

      if (currentSwiper.progress >= 0.8 && _this.bannerAdData.current_page < _this.bannerAdData.last_page) {
        _this.getBannerAd(_this.bannerAdData.current_page + 1);
      }
    },
  },
  beforeDestroy: function () {
    // 移除监听
    this.$bus.$off('handlerBannerSwiper');
  }
})
