/**
 * @name user-child.js
 * @author chenls
 * @date 2021/3/12 09:10
 * @description: 用户下线列表
 */

var userChild = new Vue({
  el: '#user-child',

  components: {
    'footer-nav': httpVueLoader('./static/js/vue-components/FooterNav.vue?v=' + STATIC_VERSION),
  },

  data: {
    child_num: child_num,
    userChild: { data: [] },
    scrollOptions: {
      pullUpLoad: {
        threshold: 50,
        txt: {
          more: '加载更多数据',
          noMore: '无更多数据'
        }
      },
    },
  },

  created: function () {
    this.getUserChild();
  },

  methods: {
    getUserChild: function (page) {
      var _this = this;
      var current_page = page || 1;

      $http.get('/index.php?s=api/user/child_list&page=' + current_page)
        .then(function (res) {
          var data = res.data;

          if (data.current_page === 1) {
            _this.userChild = data;
          } else {
            _this.userChild = Object.assign(data, {
              data: [].concat(_this.userChild.data, data.data),
            });
          }

        })
        .catch(function () {

        })
        .then(function () {
          _this.$nextTick(function () {
            _this.$refs.scroll.forceUpdate();
          })
        });
    },

    scrollPullingUp: function () {
      if (this.userChild.current_page < this.userChild.last_page) {
        this.getUserChild(this.userChild.current_page + 1);
      } else {
        this.$refs.scroll.forceUpdate();
      }
    },
  }
})
