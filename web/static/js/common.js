isEmpty = function (val) {
  if (val instanceof Array) {
    if (val.length === 0) return true;
  } else if (val instanceof Object) {
    if (!Object.keys(val).length) return true;
  } else {
    if (val === 'null' || val === null || val === 'undefined' || val === undefined || val === '') return true;
    return false;
  }
  return false;
};

Vue.use({
  install() {
    Vue.prototype.$bus = new Vue();
  },
});

getQueryStr = function (r) { var e, n = String(window.document.location.href), t = new RegExp("(^|)" + r + "=([^&]*)(&|$)", "gi").exec(n); return (e = t) ? decodeURIComponent(e[2]) : "" }
