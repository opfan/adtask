/**
 * @name hb-send.js
 * @author chenls
 * @date 2021/2/19 15:56
 * @description: 已发红包列表脚本
 */

var HbSendPopup = httpVueLoader('./static/js/vue-components/HbSendPopup.vue?v=' + STATIC_VERSION);

var hdsendVue = new Vue({
  el: '#hbsend',

  components: {
    'segment-picker-date': httpVueLoader('./static/js/vue-components/SegmentPickerDate.vue?v=' + STATIC_VERSION),
    'task-item': httpVueLoader('./static/js/vue-components/TaskItem.vue?v=' + STATIC_VERSION),
    'footer-nav': httpVueLoader('./static/js/vue-components/FooterNav.vue?v=' + STATIC_VERSION),
  },

  filters: {
    sex: function (val) {
      switch (val) {
        case 0:
          return '全部';
        case 1:
          return '男';
        case 1:
          return '女';
      };
    },
  },

  data: {
    form: {
      origin: 1,
      type: 1,
      search: null,
      stime: null,
      etime: null,
    },
    timeDate: null,
    money_send: money_send,
    scrollOptions: {
      pullUpLoad: {
        threshold: 50,
        txt: {
          more: '加载更多数据',
          noMore: '无更多数据'
        }
      },
    },
    dataList: { data: [] },
  },

  created: function () {
    this.getDataList();
  },

  methods: {
    getDataList: function (page) {
      var _this = this;
      var current_page = page || 1;
      var form = {};
      Object.keys(_this.form).forEach(function (val) {
        if (_this.form[val]) {
          form[val] = _this.form[val];
        }
      });

      $http.get('/index.php?s=api/user/ad_list&page=' + current_page, form)
        .then(function (res) {
          var data = res.data;

          if (data.current_page === 1) {
            _this.dataList = data;
          } else {
            _this.dataList = Object.assign(data, {
              data: [].concat(_this.dataList.data, data.data),
            });
          }
        })
        .catch(function () {

        })
        .then(function () {
          _this.$nextTick(function () {
            _this.$refs.scroll.forceUpdate();
          })
        });
    },

    search: function (type) {
      if (type && typeof type === 'number') {
        this.form.type = type;
      }
      this.getDataList();
    },

    scrollPullingUp: function () {
      if (this.dataList.current_page < this.dataList.last_page) {
        this.getDataList(this.dataList.current_page + 1);
      } else {
        this.$refs.scroll.forceUpdate();
      }
    },

    showAdItem: function (item) {
      var _this = this;

      var popup = _this.$createPopup({}, function (h) {
        return h(HbSendPopup, {
          props: {
            item: item,
          },
          on: {
            close: function () {
              popup.remove();
            },
          },
        });
      }, true);

      popup.show();
    },

    timeOnSelectPicker: function (selectedDates, selectedVals, selectedTexts) {
      this.form.stime = selectedDates[0].getTime() / 1000;
      this.form.etime = selectedDates[1].getTime() / 1000;
      this.timeDate = selectedTexts[0].join('/') + ' - ' + selectedTexts[1].join('/');
    },
  },

});
