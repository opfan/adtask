/**
 * @name business.js
 * @author chenls
 * @date 2021/2/19 14:06
 * @description: 商业广告列表脚本
 */

var BusinessAdPopup = httpVueLoader('./static/js/vue-components/BusinessAdPopup.vue?v=' + STATIC_VERSION);

var businessVue = new Vue({
  el: '#business',

  components: {
    'task-item': httpVueLoader('./static/js/vue-components/TaskItem.vue?v=' + STATIC_VERSION),
  },

  filters: {
    sex: function (val) {
      switch(val) {
        case 0:
          return '全部';
        case 1:
          return '男';
        case 1:
          return '女';
      };
    },
  },

  data: {
    form: {
      origin: 2,
      type: 1,
      search: null,
    },
    scrollOptions: {
      pullUpLoad: {
        threshold: 50,
        txt: {
          more: '加载更多数据',
          noMore: '无更多数据'
        }
      },
    },
    dataList: {data: []},
  },

  created: function () {
    this.getDataList();
  },

  methods: {
    getDataList: function (page) {
      var _this = this;
      var current_page = page || 1;
      var form = {};
      Object.keys(_this.form).forEach(function (val) {
        if (_this.form[val]) {
          form[val] = _this.form[val];
        }
      });

      $http.get('/index.php?s=api/user/ad_list&page=' + current_page, form)
        .then(function (res) {
          var data = res.data;

          if (data.current_page === 1) {
            _this.dataList = data;
          } else {
            _this.dataList = Object.assign(data, {
              data: [].concat(_this.dataList.data, data.data),
            });
          }
        })
        .catch(function () {

        })
        .then(function () {
          _this.$nextTick(function () {
            _this.$refs.scroll.forceUpdate();
          })
        });
    },

    search: function (type) {
      if (type && typeof type === 'number') {
        this.form.type = type;
      }
      this.getDataList();
    },

    scrollPullingUp: function () {
      if (this.dataList.current_page < this.dataList.last_page) {
        this.getDataList(this.dataList.current_page + 1);
      } else {
        this.$refs.scroll.forceUpdate();
      }
    },

    showAdItem: function (item) {
      var _this = this;

      var popup = _this.$createPopup({}, function (h) {
        return h(BusinessAdPopup, {
          props: {
            item: item,
          },
          on: {
            close: function () {
              popup.remove();
            },
          },
        });
      }, true);

      popup.show();
    },
  },

});
