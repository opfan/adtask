/**
 * @name register.js
 * @author chenls
 * @date 2021/2/10 12:13
 * @description: 注册界面脚本
 */

new Vue({
  el: '#register',
  data: {
    form: {
      longitude: null,
      latitude: null,
      position_id: null,
      category_id: null,
      category_id: null,
    },
    picker: null,
    picker2: null,
    positionData: [],
    categoryData: [],
    categoryValue: '',
  },
  computed: {
    positionSwithData: function () {
      var _this = this;
      return this.positionData.find(function (item) {
        return item.position_id === _this.form.position_id;
      }) || {};
    },
    categorySwithText: function () {
      if (!this.categoryValue) return null;

      var _this = this;
      var text = [];
      var arr = _this.categoryValue.split(',');
      var data = _this.categoryData;

      for (var i = 0; i < arr.length; i++) {
        var item = data.find(function (d) {
          return d.category_id === +arr[i];
        })
        text.push(item.name);
        if (item.child) {
          data = item.child;
        }
      }

      return text.join('-');
    },
  },
  created: function () {
    var than = this;

    $http.get('/index.php?s=api/index/wx_config&api=getLocation')
      .then(function (res) {
        var data = JSON.parse(res.data);

        wx.config(Object.assign(data, {
          jsApiList:["getLocation"],
        }));

        wx.ready(function () {
          wx.getLocation({
            type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
            success: function (res) {
              than.form.latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
              than.form.longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
            }
          });
        })
      });

    Promise.all([
      $http.get('/index.php?s=/api/index/position'),
      $http.get('/index.php?s=/api/index/category'),
    ])
      .then(function (res) {
        than.positionData = res[0].data;
        var categoryData = res[1].data;

        for (var i = 0; i < categoryData.length; i++) {
          var item = Object.assign(categoryData[i], {
            children: categoryData[i].child || [],
          });

          than.categoryData.push(item);
        }

      });
  },
  methods: {
    switchCategory: function () {
      var _this = this;
      if (!this.picker2) {
        this.picker2 = this.$createCascadePicker({
          title: '选择从事行业',
          data: this.categoryData,
          alias: {value: 'category_id', text: 'name'},
          onSelect: function (e) {
            _this.categoryValue = e.join();
            _this.form.category_id = e.pop();
          }
        });
      }
      this.picker2.show();
    },
    switchPosition: function () {
      var _this = this;
      if (!this.picker) {
        this.picker = this.$createPicker({
          title: '选择职位',
          data: [this.positionData],
          alias: {value: 'position_id', text: 'name'},
          onSelect: function (e) {
            _this.form.position_id = e[0];
          }
        });
      }
      this.picker.show();
    },
    submit: function () {
      var _this = this;
      $http.post('/index.php?s=/api/user/update', this.form)
        .then(function (res) {

          // _this.close();

          if(res.code){
            var toast = _this.$createToast({
              time: 1500,
              txt: "添加成功",
              type: "correct",
            });
            toast.show();
            setTimeout(function () {
              window.location.replace(redirect);
            }, 1500);
          }else{
            var toast = _this.$createToast({
              time: 1000,
              txt: res.msg,
              type: "txt",
            });
            toast.show();
          }
        }) .catch(function (res) {
        var toast = _this.$createToast({
          time: 1000,
          txt: res.msg,
          type: "txt",
        });
        toast.show();
      });
    },
  },
});
