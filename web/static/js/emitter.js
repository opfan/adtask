"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/**
 * 查找当前祖先组件
 * @param {String} componentName
 * @param {Object} parent
 */
function findAncestor(componentName, parent) {
  var currentParent = parent;
  var name = currentParent && (currentParent.$options.componentName || currentParent.$options.name);

  while (currentParent && (!name || name !== componentName)) {
    currentParent = currentParent.$parent;

    if (currentParent) {
      name = currentParent.$options.componentName || currentParent.$options.name;
    }
  }

  return currentParent;
}
/**
 * 查找指定的所有后代组件
 * @param {String} componentName
 * @param {Array} children
 */


function findDescendants(componentName, children) {
  var descendants = [];

  if (children && children && children.length) {
    children.forEach(function (child) {
      var name = child.$options.componentName || child.$options.name;

      if (name === componentName) {
        descendants = [].concat(_toConsumableArray(descendants), [child]);
      } else {
        descendants = [].concat(_toConsumableArray(descendants), _toConsumableArray(findDescendants(componentName, child.$children)));
      }
    });
  }

  return descendants;
}