"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

var HttpFly = new Fly();

var isEmpty = function isEmpty(val) {
  if (val === 'null' || val === null || val === 'undefined' || val === undefined || val === '') {
    return true;
  }

  if (_instanceof(val, Array) || Array.isArray(val)) {
    if (val.length === 0) return true;
  } else if (_instanceof(val, Object) || _typeof(val) === 'object') {
    if (!Object.keys(val).length) return true;
  }

  return false;
}; // 状态码


var ERROR_STATUS_CODE = [0]; // 设置超时

HttpFly.config.timeout = 30000; // 添加请求拦截器

HttpFly.interceptors.request.use( /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(request) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            request.headers['Content-Type'] = 'application/json; charset=utf-8';
            return _context.abrupt("return", request);

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}(), function (error) {
  return Promise.reject(error);
});
// 添加响应拦截器，响应拦截器会在then/catch处理之前执行
HttpFly.interceptors.response.use(function (response) {
  var statusCode = response.data.code;

  if (ERROR_STATUS_CODE.includes(statusCode)) {
    return Promise.reject(response.data);
  }
  return response.data;
}, function (error) {
  return Promise.reject(!isEmpty(error.response) ? error.response.data : error);
});
window.$http = HttpFly;
