/**
 * @name withdraw.js
 * @author chenls
 * @date 2021/2/18 11:10
 * @description: 提现列表脚本
 */

var ApplyWithdrawPopup = httpVueLoader('./static/js/vue-components/ApplyWithdrawPopup.vue?v=' + STATIC_VERSION);

var commissionVue = new Vue({
  el: '#commission',

  components: {
    'footer-nav': httpVueLoader('./static/js/vue-components/FooterNav.vue?v=' + STATIC_VERSION),
    'segment-picker-date': httpVueLoader('./static/js/vue-components/SegmentPickerDate.vue?v=' + STATIC_VERSION),
    'task-item': httpVueLoader('./static/js/vue-components/TaskItem.vue?v=' + STATIC_VERSION),
  },

  data: {
    form: {
      stime: null,
      etime: null,
      search: null,
    },
    timeDate: null,
    commissionList: {data: []},
    commissionTotal: commissionTotal,
    scrollOptions: {
      pullUpLoad: {
        threshold: 50,
        txt: {
          more: '加载更多数据',
          noMore: '无更多数据'
        }
      },
    },
  },

  created: function () {
    this.getCommissionList();
  },

  methods: {
    timeOnSelectPicker: function (selectedDates, selectedVals, selectedTexts) {
      this.form.stime = selectedDates[0].getTime() / 1000;
      this.form.etime = selectedDates[1].getTime() / 1000;
      this.timeDate = selectedTexts[0].join('/') + ' - ' + selectedTexts[1].join('/');
    },

    getCommissionList: function (page) {
      var _this = this;
      var current_page = page || 1;
      var form = {};
      Object.keys(_this.form).forEach(function (val) {
        if (_this.form[val] !== null) {
          form[val] = _this.form[val];
        }
      });

      $http.get('/index.php?s=api/commission/list&page=' + current_page, form)
        .then(function (res) {
          var data = res.data;

          if (data.current_page === 1) {
            _this.commissionList = data;
          } else {
            _this.commissionList = Object.assign(data, {
              data: [].concat(_this.commissionList.data, data.data),
            });
          }
        })
        .catch(function () {

        })
        .then(function () {
          _this.$nextTick(function () {
            _this.$refs.scroll.forceUpdate();
          })
        });
    },

    search: function () {
      this.getCommissionList();
    },

    scrollPullingUp: function () {
      if (this.commissionList.current_page < this.commissionList.last_page) {
        this.getCommissionList(this.commissionList.current_page + 1);
      } else {
        this.$refs.scroll.forceUpdate();
      }
    },

    applyWithdraw: function () {
      var _this = this;

      var popup = _this.$createPopup({}, function (h) {
        return h(ApplyWithdrawPopup, {
          on: {
            close: function () {
              popup.remove();
            },
          },
        });
      }, true);

      popup.show();
    },
  }
});
