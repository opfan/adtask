/**
 * @name withdraw.js
 * @author chenls
 * @date 2021/2/18 11:10
 * @description: 提现列表脚本
 */

var withdrawVue = new Vue({
  el: '#withdraw',

  components: {
    'footer-nav': httpVueLoader('./static/js/vue-components/FooterNav.vue?v=' + STATIC_VERSION),
    'segment-picker-date': httpVueLoader('./static/js/vue-components/SegmentPickerDate.vue?v=' + STATIC_VERSION),
  },

  data: {
    form: {
      stime: null,
      etime: null,
    },
    timeDate: null,
    withdraw_money: withdraw_money,
    money: money,
    withdrawList: {data: []},
    scrollOptions: {
      pullUpLoad: {
        threshold: 50,
        txt: {
          more: '加载更多数据',
          noMore: '无更多数据'
        }
      },
    },
  },

  created: function () {
    this.getWithdrawList();
  },

  mounted () {

  },
  methods: {
    timeOnSelectPicker: function (selectedDates, selectedVals, selectedTexts) {
      this.form.stime = selectedDates[0].getTime() / 1000;
      this.form.etime = selectedDates[1].getTime() / 1000;
      this.timeDate = selectedTexts[0].join('/') + ' - ' + selectedTexts[1].join('/');
    },

    getWithdrawList: function (page) {
      var _this = this;
      var current_page = page || 1;
      var form = {};
      Object.keys(_this.form).forEach(function (val) {
        if (_this.form[val] !== null) {
          form[val] = _this.form[val];
        }
      });

      $http.get('/index.php?s=api/withdraw/list&page=' + current_page, form)
        .then(function (res) {
          var data = res.data;

          if (data.current_page === 1) {
            _this.withdrawList = data;
          } else {
            _this.withdrawList = Object.assign(data, {
              data: [].concat(_this.withdrawList.data, data.data),
            });
          }

        })
        .catch(function () {

        })
        .then(function () {
          _this.$nextTick(function () {
            _this.$refs.scroll.forceUpdate();
          })
        });
    },

    search: function () {
      this.getWithdrawList();
    },

    scrollPullingUp: function () {
      if (this.withdrawList.current_page < this.withdrawList.last_page) {
        this.getWithdrawList(this.withdrawList.current_page + 1);
      } else {
        this.$refs.scroll.forceUpdate();
      }
    },
  },
});
