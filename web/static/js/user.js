/**
 * @name user.js
 * @author chenls
 * @date 2021/2/18 10:13
 * @description: 用户中心脚本
 */

var UserInfoPopup = httpVueLoader('./static/js/vue-components/UserInfoPopup.vue?v=' + STATIC_VERSION);

var userVue = new Vue({
  el: '#user',
  components: {
    'banner-swiper': httpVueLoader('./static/js/vue-components/BannerSwiper.vue?v=' + STATIC_VERSION),
    'footer-nav': httpVueLoader('./static/js/vue-components/FooterNav.vue?v=' + STATIC_VERSION),
    'share-button': httpVueLoader('./static/js/vue-components/ShareButton.vue?v=' + STATIC_VERSION),
  },
  data: {
    userInfo: user,
    adListData: { data: [] },
    loading: false,
    loginUser: login_user,
    scrollOptions: {
      pullUpLoad: {
        threshold: 50,
        txt: {
          more: '加载更多数据',
          noMore: '无更多数据'
        }
      },
    },
    apply_money: apply_money,
    withdraw_money: withdraw_money,
    send_money: send_money,
    child_num: child_num,
  },
  created: function () {
    this.getAdList();
  },
  mounted: function () {

  },
  methods: {
    getAdList: function (page) {
      var _this = this;
      var current_page = page || 1;

      this.loading = true;

      $http.get('/index.php?s=api/user/list&page=' + current_page, !_this.loginUser ? {
        user_id: _this.userInfo.user_id,
      } : {})
        .then(function (res) {
          var data = res.data;

          if (data.current_page === 1) {
            _this.adListData = data;
          } else {
            _this.adListData = Object.assign(data, {
              data: [].concat(_this.adListData.data, data.data),
            });
          }
        })
        .catch(function () {

        })
        .then(function () {
          _this.$nextTick(function () {
            _this.loading = true;
            _this.$refs.scroll.forceUpdate();
          })
        });
    },
    getAddress: function (item) {
      var addres = item.address || [];

      if (addres.length) {
        var data = addres[0];

        return data.address || (data.province + data.city + data.region);
      }
      return null;
    },
    scrollPullingUp: function () {
      if (this.adListData.current_page < this.adListData.last_page) {
        this.getAdList(this.adListData.current_page + 1);
      } else {
        this.$refs.scroll.forceUpdate();
      }
    },
    showUserInfo: function () {
      var _this = this;

      var popup = _this.$createPopup({}, function (h) {
        return h(UserInfoPopup, {
          on: {
            close: function () {
              popup.remove();
            },
          },
        });
      }, true);

      popup.show();
    },

    skip: function (url) {
      window.location.href = url;
    },

    showQrcode: function () {
      var _this = this;

      var qrcodePopup = _this.$createPopup({
        $props: {
          maskClosable: true,
          mask: false,
        },
      }, function (h) {
        return h('div', {
          style: {
            background: 'url(./static/images/qrcode-bg.png)',
            padding: '12%',
            width: '80vw',
            height: '60vh',
            'background-size': 'contain',
            'background-repeat': 'no-repeat',
            'background-position': 'center',
            display: 'flex',
            'align-items': 'center',
            'flex-direction': 'column',
            position: 'relative',
          },
          on: {
            click: function() {
              qrcodePopup.remove();
            },
          },
        }, [h('img', {
          style: {
            width: '100%',
            height: 'auto',
          },
          attrs: {
            src: _this.userInfo.qrcode
          }
        })]);
      }, true).show();
    },
  },
  beforeDestroy: function () {

  }
})
