<?php

namespace app\command;

use app\common\model\finance\Commission;
use think\Config;
use think\console\Command;
use think\console\Input;
use think\console\Output;

defined('COMMISSION') or define('COMMISSION', RUNTIME_PATH . 'COMMISSION' . DS);
class Auto extends Command
{

    /**
     * @desc configure
     *
     */
    protected function configure()
    {
        $this->setName('commission')
         ->setDescription('auto_commission Start');
    }


    /**
     * @param Input $input
     * @param Output $output
     * @return int|void|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function execute(Input $input, Output $output)
    {
        Config::set([
            'log' => [
                'path' => COMMISSION,
            ]
        ]);
        // 输出到日志文件
        $output->writeln("Start");

        $result = (new Commission())->autoCommission();

        $output->writeln($result);
    }
}