<?php

// 应用公共函数库文件

use think\Request;
use think\Log;

/**
 * 打印调试函数
 * @param $content
 * @param $is_die
 */
function pre($content, $is_die = true)
{
    header('Content-type: text/html; charset=utf-8');
    echo '<pre>' . print_r($content, true);
    $is_die && die();
}

/**
 * 驼峰命名转下划线命名
 * @param $str
 * @return string
 */
function toUnderScore($str)
{
    $dstr = preg_replace_callback('/([A-Z]+)/', function ($matchs) {
        return '_' . strtolower($matchs[0]);
    }, $str);
    return trim(preg_replace('/_{2,}/', '_', $dstr), '_');
}

/**
 * 生成密码hash值
 * @param $password
 * @return string
 */
function yoshop_hash($password)
{
    return md5(md5($password) . 'yoshop_salt_SmTRx');
}

/**
 * 获取当前域名及根路径
 * @return string
 */
function base_url()
{
    static $baseUrl = '';
    if (empty($baseUrl)) {
        $request = Request::instance();
        $subDir = str_replace('\\', '/', dirname($request->server('PHP_SELF')));
        $baseUrl = $request->scheme() . '://' . $request->host() . $subDir . ($subDir === '/' ? '' : '/');
    }
    return $baseUrl;
}

/**
 * 写入日志 (废弃)
 * @param string|array $values
 * @param string $dir
 * @return bool|int
 */
//function write_log($values, $dir)
//{
//    if (is_array($values))
//        $values = print_r($values, true);
//    // 日志内容
//    $content = '[' . date('Y-m-d H:i:s') . ']' . PHP_EOL . $values . PHP_EOL . PHP_EOL;
//    try {
//        // 文件路径
//        $filePath = $dir . '/logs/';
//        // 路径不存在则创建
//        !is_dir($filePath) && mkdir($filePath, 0755, true);
//        // 写入文件
//        return file_put_contents($filePath . date('Ymd') . '.log', $content, FILE_APPEND);
//    } catch (\Exception $e) {
//        return false;
//    }
//}

/**
 * 写入日志 (使用tp自带驱动记录到runtime目录中)
 * @param $value
 * @param string $type
 */
function log_write($value, $type = 'yoshop-info')
{
    $msg = is_string($value) ? $value : var_export($value, true);
    Log::record($msg, $type);
}

/** curl请求指定url (get)
 * @param $url
 * @param array $data
 * @param array $config
 * @return bool|string
 */
function curl($url, $data = [],$config = [])
{
    // 处理get数据
    if (!empty($data)) {
        $url = $url . '?' . http_build_query($data);
    }
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_TIMEOUT,$config['time_out'] ?? 60);   //只需要设置一个秒的数量就可以
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}

/**  * curl请求指定url (post)
 * @param $url
 * @param array $data
 * @param array $config
 * @return bool|string
 */
function curlPost($url, $data = [],$config = [])
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT,$config['time_out'] ?? 60);   //只需要设置一个秒的数量就可以
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

if (!function_exists('array_column')) {
    /**
     * array_column 兼容低版本php
     * (PHP < 5.5.0)
     * @param $array
     * @param $columnKey
     * @param null $indexKey
     * @return array
     */
    function array_column($array, $columnKey, $indexKey = null)
    {
        $result = array();
        foreach ($array as $subArray) {
            if (is_null($indexKey) && array_key_exists($columnKey, $subArray)) {
                $result[] = is_object($subArray) ? $subArray->$columnKey : $subArray[$columnKey];
            } elseif (array_key_exists($indexKey, $subArray)) {
                if (is_null($columnKey)) {
                    $index = is_object($subArray) ? $subArray->$indexKey : $subArray[$indexKey];
                    $result[$index] = $subArray;
                } elseif (array_key_exists($columnKey, $subArray)) {
                    $index = is_object($subArray) ? $subArray->$indexKey : $subArray[$indexKey];
                    $result[$index] = is_object($subArray) ? $subArray->$columnKey : $subArray[$columnKey];
                }
            }
        }
        return $result;
    }
}

/**
 * 多维数组合并
 * @param $array1
 * @param $array2
 * @return array
 */
function array_merge_multiple($array1, $array2)
{
    $merge = $array1 + $array2;
    $data = [];
    foreach ($merge as $key => $val) {
        if (
            isset($array1[$key])
            && is_array($array1[$key])
            && isset($array2[$key])
            && is_array($array2[$key])
        ) {
            $data[$key] = array_merge_multiple($array1[$key], $array2[$key]);
        } else {
            $data[$key] = isset($array2[$key]) ? $array2[$key] : $array1[$key];
        }
    }
    return $data;
}

/**
 * 二维数组排序
 * @param $arr
 * @param $keys
 * @param bool $desc
 * @return mixed
 */
function array_sort($arr, $keys, $desc = false)
{
    $key_value = $new_array = array();
    foreach ($arr as $k => $v) {
        $key_value[$k] = $v[$keys];
    }
    if ($desc) {
        arsort($key_value);
    } else {
        asort($key_value);
    }
    reset($key_value);
    foreach ($key_value as $k => $v) {
        $new_array[$k] = $arr[$k];
    }
    return $new_array;
}

/**
 * 数据导出到excel(csv文件)
 * @param $fileName
 * @param array $tileArray
 * @param array $dataArray
 */
function export_excel($fileName, $tileArray = [], $dataArray = [])
{
    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', 0);
    ob_end_clean();
    ob_start();
    header("Content-Type: text/csv");
    header("Content-Disposition:filename=" . $fileName);
    $fp = fopen('php://output', 'w');
    fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));// 转码 防止乱码(比如微信昵称)
    fputcsv($fp, $tileArray);
    $index = 0;
    foreach ($dataArray as $item) {
        if ($index == 1000) {
            $index = 0;
            ob_flush();
            flush();
        }
        $index++;
        fputcsv($fp, $item);
    }
    ob_flush();
    flush();
    ob_end_clean();
}

/**
 * 隐藏敏感字符
 * @param $value
 * @return string
 */
function substr_cut($value)
{
    $strlen = mb_strlen($value, 'utf-8');
    if ($strlen <= 1) return $value;
    $firstStr = mb_substr($value, 0, 1, 'utf-8');
    $lastStr = mb_substr($value, -1, 1, 'utf-8');
    return $strlen == 2 ? $firstStr . str_repeat('*', $strlen - 1) : $firstStr . str_repeat("*", $strlen - 2) . $lastStr;
}

/**
 * 获取当前系统版本号
 * @return mixed|null
 * @throws Exception
 */
function get_version()
{
    static $version = null;
    if ($version) {
        return $version;
    }
    $file = dirname(ROOT_PATH) . '/version.json';
    if (!file_exists($file)) {
        throw new Exception('version.json not found');
    }
    $version = json_decode(file_get_contents($file), true);
    if (!is_array($version)) {
        throw new Exception('version cannot be decoded');
    }
    return $version['version'];
}

/**
 * 获取全局唯一标识符
 * @param bool $trim
 * @return string
 */
function getGuidV4($trim = true)
{
    // Windows
    if (function_exists('com_create_guid') === true) {
        $charid = com_create_guid();
        return $trim == true ? trim($charid, '{}') : $charid;
    }
    // OSX/Linux
    if (function_exists('openssl_random_pseudo_bytes') === true) {
        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    // Fallback (PHP 4.2+)
    mt_srand((double)microtime() * 10000);
    $charid = strtolower(md5(uniqid(rand(), true)));
    $hyphen = chr(45);                  // "-"
    $lbrace = $trim ? "" : chr(123);    // "{"
    $rbrace = $trim ? "" : chr(125);    // "}"
    $guidv4 = $lbrace .
        substr($charid, 0, 8) . $hyphen .
        substr($charid, 8, 4) . $hyphen .
        substr($charid, 12, 4) . $hyphen .
        substr($charid, 16, 4) . $hyphen .
        substr($charid, 20, 12) .
        $rbrace;
    return $guidv4;
}

/**
 * 时间戳转换日期
 * @param $timeStamp
 * @return false|string
 */
function format_time($timeStamp)
{
    return date('Y-m-d H:i:s', $timeStamp);
}

/**
 * 左侧填充0
 * @param $value
 * @param int $padLength
 * @return string
 */
function pad_left($value, $padLength = 2)
{
    return \str_pad($value, $padLength, "0", STR_PAD_LEFT);
}

/**
 * 过滤emoji表情
 * @param $text
 * @return null|string|string[]
 */
function filter_emoji($text)
{
    // 此处的preg_replace用于过滤emoji表情
    // 如需支持emoji表情, 需将mysql的编码改为utf8mb4
    return preg_replace('/[\xf0-\xf7].{3}/', '', $text);
}

/**
 * @desc  isIdCard 验证身份证
 * @param $number
 * @return bool
 */
if(!function_exists('isIdCard')) {

    function isIdCard($number)
    {
        // 检查是否是身份证号
        $number = getIDCard($number);

        // 转化为大写，如出现x
        $number = strtoupper($number);

        if (strlen($number) != 18) {
            return false;
        }
        //加权因子
        $wi    = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        //校验码串
        $ai    = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
        //按顺序循环处理前17位
        $sigma = 0;

        for ($i = 0;$i < 17;$i++) {
            //提取前17位的其中一位，并将变量类型转为实数
            $b = (int) $number{$i};
            //提取相应的加权因子
            $w = $wi[$i];
            //把从身份证号码中提取的一位数字和加权因子相乘，并累加
            $sigma += $b * $w;
        }

        //计算序号
        $snumber = $sigma % 11;

        //按照序号从校验码串中提取相应的字符。
        $check_number = $ai[$snumber];

        if ($number{17} == $check_number) {

            return true;

        } else {

            return false;

        }

    }

}

/**
 * @desc  getIDCard 把15位身份证转换成18位
 * @param string $idCard
 * @return string
 */
if(!function_exists('getIDCard')) {

    function getIDCard($idCard)
    {
        // 若是15位，则转换成18位；否则直接返回ID
        if (15 == strlen ( $idCard )) {

            $W = array (7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2,1 );

            $A = array ("1","0","X","9","8","7","6","5","4","3","2" );

            $s = 0;

            $idCard18 = substr ( $idCard, 0, 6 ) . "19" . substr ( $idCard, 6 );

            $idCard18_len = strlen ( $idCard18 );

            for($i = 0; $i < $idCard18_len; $i ++) {

                $s = $s + substr ( $idCard18, $i, 1 ) * $W [$i];

            }

            $idCard18 .= $A [$s % 11];

            return $idCard18;

        } else {

            return $idCard;

        }

    }

}

/**
 * @desc  cardIdSex 通过身份证获取性别
 * @param $cid
 * @param int $comm
 * @param int $cid 返回格式
 * @return bool|string
 */
if(!function_exists('cardIdSex')) {

    function cardIdSex($cid , $comm = 2)
    {
        //根据身份证号，自动返回性别
        $cid = getIDCard($cid);

        if (!isIdCard($cid)) return false;

        $sexint = (int)substr($cid,16,1);

        if ($comm ==1) {

            return $sexint % 2 === 0 ? '女士' : '先生';

        } elseif($comm ==2) {

            return $sexint % 2 === 0 ? '女' : '男';

        } else {
            return $sexint % 2 === 0 ? '2' : '1';
        }
    }

}

/**
 * 数据脱敏
 * @param $string 字符串
 * @param int $start 开始
 * @param int $length 长度
 * @param string $re 脱敏替代符号
 * @return bool|string
 *
 */
function dataDesensitization($string, $start = 0, $length = 0, $re = '*')
{
    if (empty($string)){
        return false;
    }
    $strArr = array();
    $strLen = mb_strlen($string);
    while ($strLen) {//循环把字符串变为数组
        $strArr[] = mb_substr($string, 0, 1, 'utf8');
        $string   = mb_substr($string, 1, $strLen, 'utf8');
        $strLen   = mb_strlen($string);
    }
    $strLength = count($strArr);
    $begin = $start >= 0 ? $start : ($strLength - abs($start));
    $end   = $last = $strLength - 1;
    if ($length > 0) {
        $end = $begin + $length - 1;
    } elseif ($length < 0) {
        $end -= abs($length);
    }
    for ($i = $begin; $i <= $end; $i++) {
        $strArr[$i] = $re;
    }
    if ($begin >= $end || $begin >= $last || $end > $last) return false;
    return implode('', $strArr);
}

if (!function_exists('message')) {

    /**
     * @param  bool   $state
     * @param  string $message
     * @param  mixed  $code
     * @param  array  $data
     * @return array
     */
    function message(bool $state = true, string $message = '', $code = '', $data = [])
    {

        $array = ['state' => $state, 'msg' => $message, 'time' => time()];

        if (true !== $state && $code > 0) {

            $array['msg'] .= '#' . $code;

        }

        if (!empty($data)) $array['data'] = $data;

        return $array;

    }

}

if (!function_exists('sortArrByManyField')) {

    /**
     * 数字多种排序跟order by一样
     * @desc  sortArrByManyField
     * @return mixed|null
     * @throws Exception
     */
    function sortArrByManyField(){
        $args = func_get_args();
        if(empty($args)){
            return null;
        }
        $arr = array_shift($args);
        if(!is_array($arr)){
            throw new Exception("第一个参数不为数组");
        }
        foreach($args as $key => $field){
            if(is_string($field)){
                $temp = array();
                foreach($arr as $index=> $val){
                    $temp[$index] = $val[$field];
                }
                $args[$key] = $temp;
            }
        }
        $args[] = &$arr;//引用值
        call_user_func_array('array_multisort',$args);
        return array_pop($args);
    }

}


function randomFloat($min = 0, $max = 1) {
    return $min + mt_rand() / mt_getrandmax() * ($max - $min);
}

function array_get($array, $key, $default = null)
{
    if (is_null($key)) {
        return $array;
    }

    if (isset($array[$key])) {
        return $array[$key];
    }

    foreach (explode('.', $key) as $segment) {
        if (! is_array($array) || ! array_key_exists($segment, $array)) {
            return value($default);
        }

        $array = $array[$segment];
    }
    return $array;
}


/** 中奖概率计算
 * @desc  random
 * @param $proArr
 * @return int|string
 */
function get_rand($proArr) {
    $result = '';
    $proArr = array_filter($proArr);
    //概率数组的总概率精度
    $proSum = array_sum($proArr);
    //概率数组循环
    foreach ($proArr as $key => $proCur) {
        $randNum = mt_rand(1, $proSum);
        if ($randNum <= $proCur) {
            $result = $key;
            break;
        } else {
            $proSum -= $proCur;
        }
    }
    unset ($proArr);
    
    return $result;
}

