<?php

namespace app\store\model;

use app\common\library\helper;
use app\common\model\Store as StoreModel;

/**
 * 商城模型
 * Class Store
 * @package app\store\model
 */
class Store extends StoreModel
{

    private $AdModel;

    /* @var User $GoodsModel */
    private $UserModel;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        /* 初始化模型 */

        $this->UserModel = new User;
        $this->AdModel = new Advertising();

    }

    /**
     * 后台首页数据
     * @return array
     * @throws \think\Exception
     */
    public function getHomeData()
    {

        $data = [
            'widget-card' => [
                // 商品总量
                'goods_total' => $this->getAdTotal(),
                // 用户总量
                'user_total' => $this->getUserTotal(),
            ],

        ];


        return $data;
    }


    /**
     * @param null $day
     * @return string
     */
    private function getAdTotal($day = null)
    {
        return number_format($this->AdModel->getAdTotal($day));
    }


    /**
     * 获取用户总量
     * @param null $day
     * @return string
     * @throws \think\Exception
     */
    private function getUserTotal($day = null)
    {
        return number_format($this->UserModel->getUserTotal($day));
    }








}