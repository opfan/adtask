<?php

namespace app\store\model\finance;

use app\common\model\finance\Commission as CommissionModel;
use app\store\model\User;

/**
 * Class Commission
 * @package app\store\model\finance
 */
class Commission extends CommissionModel
{
    
    /**
     * @desc getCount
     * @return float|int
     */
    public function getCount()
    {
        return $this->where('type','in',[3,4])->sum('money');
    }
    /**
     * @desc  getList
     * @param $data
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($data)
    {
        
        // 构建查询规则
        $this->alias('commission')
            ->with(['user','advertising'])
            ->field('commission.*, user.nickName,user.avatarUrl')
            ->join('user', 'user.user_id = commission.user_id')
            ->order(['commission.create_time' => 'desc']);
        $this->where('type','in',[3,4]);
        // 查询条件
        !empty($data['search']) && $this->where('nickName', 'like', "%" . $data['search'] . "%");
        
        if (isset($data['start_time']) && !empty($data['start_time'])) {
            $this->where('commission.create_time', '>=', strtotime($data['start_time']));
        }
        if (isset($data['end_time']) && !empty($data['end_time'])) {
            $this->where('commission.create_time', '<', strtotime($data['end_time']) + 86400);
        }
        
        // 获取列表数据
        return $this->paginate(15, false, [
            'query' => \request()->request()
        ]);
    }
    
    /** 结算
     * @desc  money
     * @param $data
     * @return bool
     * @throws \think\exception\PDOException
     */
    public function money($data)
    {
        if($data['money'] > $this['apply_money']){
            $this->error = '提款金额不能大于申请金额';
            return false;
        }
        if($data['money'] > $this->user['money']){
            $this->error = '提款金额不能大于余额';
            return false;
        }
        
        $this->startTrans();
        try {
            // 更新申请状态
            $this->allowField(true)->save([
                'status'     => 20,
                'money'      => $data['money'] ?? $this['apply_money'],
                'audit_time' => time(),
            ]);
            // 更新佣金
            User::updateMoney($this['user_id'], $this['money']);
            // 事务提交
            $this->commit();
            
            return true;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            $this->rollback();
            
            return false;
        }
    }
    
}