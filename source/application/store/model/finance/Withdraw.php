<?php

namespace app\store\model\finance;

use app\common\model\finance\Withdraw as WithdrawModel;
use app\store\model\User;

/**
 * 分销商提现明细模型
 * Class Withdraw
 * @package app\store\model\dealer
 */
class Withdraw extends WithdrawModel
{
    
    /**
     * 获取器：申请时间
     * @param $value
     * @return false|string
     */
    public function getAuditTimeAttr($value)
    {
        
        return $value > 0 ? date('Y-m-d H:i:s', $value) : 0;
    }
    
    /**
     * @desc  getCount
     * @param int     $state
     * @param string  $field
     * @return float|int
     */
    public function getCount($state = 10,$field = 'money')
    {
        return $this->where(['status'=> $state])->sum($field);
    }
    /**
     * @desc  getList
     * @param $data
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($data)
    {
        // 构建查询规则
        $this->alias('withdraw')
            ->with(['user','image'])
            ->field('withdraw.*, user.nickName')
            ->join('user', 'user.user_id = withdraw.user_id','left')
            ->order(['withdraw.create_time' => 'desc']);
        // 查询条件
        $data['user_id'] > 0 && $this->where('withdraw.user_id', '=', $data['user_id']);
        !empty($data['search']) && $this->where('user.nickName', 'like', "%" . trim($data['search']) . "%");
        $data['status'] > 0 && $this->where('withdraw.status', '=', $data['status']);
    
        if (isset($data['start_time']) && !empty($data['start_time'])) {
            $this->where('withdraw.create_time', '>=', strtotime($data['start_time']));
        }
        if (isset($data['end_time']) && !empty($data['end_time'])) {
            $this->where('withdraw.create_time', '<', strtotime($data['end_time']) + 86400);
        }

        // 获取列表数据
        return $this->paginate(15, false, [
            'query' => \request()->request()
        ]);
    }
    
    /** 结算
     * @desc  money
     * @param $data
     * @return bool
     * @throws \think\exception\PDOException
     */
    public function money($data)
    {
        if($data['money'] > $this['apply_money']){
            $this->error = '提款金额不能大于申请金额';
            return false;
        }
        if($data['money'] > $this->user['money']){
            $this->error = '提款金额不能大于余额';
            return false;
        }
        
        $this->startTrans();
        try {
            // 更新申请状态
            $this->allowField(true)->save([
                'status'     => 20,
                'money'      => $data['money'] ?? $this['apply_money'],
                'audit_time' => time(),
            ]);
            // 更新佣金
            User::updateMoney($this['user_id'], $this['money']);
            // 事务提交
            $this->commit();
            
            return true;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            $this->rollback();
            
            return false;
        }
    }
    
}