<?php

namespace app\store\model;

use app\common\model\AdvertisingAddress as AdvertisingAddressModel;
use app\common\model\Region;
use Exception;
use Lvht\GeoHash;

/**
 * Class AdvertisingAddress
 * @package app\store\model
 */
class AdvertisingAddress extends AdvertisingAddressModel
{

    protected $append = ['point','province','city','region'];

   protected $hidden = ['create_time','update_time'];
    /**
     * @param $ad_id
     * @return array
     */
    public static function getPositionIds($ad_id)
    {
        return (new self)->where('ad_id', '=', $ad_id)->column('position_id');
    }

    /**
     * @param $ad_id
     * @param $params
     * @return array|false
     * @throws Exception
     */
    public function add($ad_id, $params)
    {
        $data = [];
        foreach ($params as $item) {

            $item = $this->createData($item);

            $item = $this->getIdByName($item);

            $data[] = [
                'ad_id' => $ad_id,
                'longitude' => $item['longitude'],
                'latitude' => $item['latitude'],
                'geohash' => $item['geohash'],
                'province_id' => $item['province_id']?? '',
                'city_id' => $item['city_id']??'',
                'region_id' => $item['region_id']?? '',
                'address' => $item['address'] ?? '',
            ];
        }
        return $this->allowField(true)->saveAll($data);
    }

    /**
     * @param $ad_id
     * @param $params
     * @return array|false
     * @throws Exception
     */
    public function edit($ad_id, $params)
    {
        self::deleteAll(['ad_id' => $ad_id]);

        $data = [];
        foreach ($params as $item) {
            $item = $this->createData($item);
            $item = $this->getIdByName($item);
            $data[] = [
                'ad_id' => $ad_id,
                'longitude' => $item['longitude'],
                'latitude' => $item['latitude'],
                'geohash' => $item['geohash'],
                'province_id' => $item['province_id'],
                'city_id' => $item['city_id'],
                'region_id' => $item['region_id'],
                'address' => $item['address'] ?? '',
            ];
        }
        return $this->allowField(true)->saveAll($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    private function getIdByName($data)
    {
        if(!empty($data['province'])){
            $data['province_id'] = Region::getIdByName($data['province'], 1);
            $data['city_id'] = Region::getIdByName($data['city'], 2, $data['province_id']);
            $data['region_id'] = Region::getIdByName($data['region'], 3, $data['city_id']);

        }
        return $data;
    }
    /**
     * 删除记录
     * @param $where
     * @return int
     */
    public static function deleteAll($where)
    {
        return self::destroy($where);
    }

    /**
     * 创建数据
     * @param array $data
     * @return array
     */
    private function createData($data)
    {
        // 格式化坐标信息
        if(!empty( $data['point'])){
            $coordinate = explode(',', $data['point']);
            $data['latitude'] = $coordinate[0];
            $data['longitude'] = $coordinate[1];
        }
        // 生成geohash
        $Geohash = new Geohash;
        $data['geohash'] = $Geohash->encode($data['longitude'], $data['latitude']);
        return $data;
    }

}