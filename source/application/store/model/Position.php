<?php

namespace app\store\model;

use app\common\model\Position as PositionModel;

/**
 * 商品分类模型
 * Class Category
 * @package app\store\model
 */
class Position extends PositionModel
{
    /**
     * 添加新记录
     * @param $data
     * @return false|int
     */
    public function add($data)
    {

        return $this->allowField(true)->save($data);
    }

    /**
     * 编辑记录
     * @param $data
     * @return bool|int
     */
    public function edit($data)
    {
        return $this->allowField(true)->save($data) !== false;
    }

    /**
     * @return int
     */
    public function remove()
    {
        return $this->delete();
    }

    /**
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getAll()
    {
        return (new static())->where('is_delete',0)->select();
    }


}
