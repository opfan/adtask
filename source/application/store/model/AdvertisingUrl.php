<?php

namespace app\store\model;

use app\common\model\AdvertisingUrl as AdvertisingUrlModel;

/**
 * Class AdvertisingUrl
 * @package app\store\model
 */
class AdvertisingUrl extends AdvertisingUrlModel
{
}
