<?php

namespace app\store\model;

use app\common\model\AdvertisingPosition as AdvertisingPositionModel;

/**
 * Class AdvertisingPosition
 * @package app\store\model
 */
class AdvertisingPosition extends AdvertisingPositionModel
{
    /**
     * @param $ad_id
     * @param $positionIds
     * @return array|false
     * @throws \Exception
     */
    public function add($ad_id, $positionIds)
    {
        $data = [];
        foreach ($positionIds as $position_id) {
            $data[] = [
                'ad_id' => $ad_id,
                'position_id' => $position_id,
            ];
        }
        return $this->saveAll($data);
    }

    /**
     * 更新关系记录
     * @param $ad_id
     * @param array $positionIds 新的角色集
     * @return array|false
     * @throws \Exception
     */
    public function edit($ad_id, $positionIds)
    {
        // 已分配的角色集
        $assignPositionIds = self::getPositionIds($ad_id);

        /**
         * 找出删除的角色
         * 假如已有的角色集合是A，界面传递过得角色集合是B
         * 角色集合A当中的某个角色不在角色集合B当中，就应该删除
         * 使用 array_diff() 计算补集
         */
        if ($deleteRoleIds = array_diff($assignPositionIds, $positionIds)) {
            self::deleteAll(['position_id' => $positionIds, 'ad_id' => ['in', $deleteRoleIds]]);
        }

        /**
         * 找出添加的角色
         * 假如已有的角色集合是A，界面传递过得角色集合是B
         * 角色集合B当中的某个角色不在角色集合A当中，就应该添加
         * 使用 array_diff() 计算补集
         */
        $positionIds = array_diff($positionIds, $assignPositionIds);
        $data = [];
        foreach ($positionIds as $positionId) {
            $data[] = [
                'ad_id' => $ad_id,
                'position_id' => $positionId,
            ];
        }
        return $this->saveAll($data);
    }

    /**
     * @param $ad_id
     * @return array
     */
    public static function getPositionIds($ad_id)
    {
        return (new self)->where('ad_id', '=', $ad_id)->column('position_id');
    }

    /**
     * 删除记录
     * @param $where
     * @return int
     */
    public static function deleteAll($where)
    {
        return self::destroy($where);
    }

}