<?php

namespace app\store\model;

use app\common\model\AdvertisingCategory as AdvertisingCategoryModel;

/**
 * Class AdvertisingCategory
 * @package app\store\model
 */
class AdvertisingCategory extends AdvertisingCategoryModel
{
    /**
     * @param $ad_id
     * @param $categoryIds
     * @return array|false
     * @throws \Exception
     */
    public function add($ad_id, $categoryIds)
    {
        $data = [];
        foreach ($categoryIds as $category_id) {
            $data[] = [
                'ad_id' => $ad_id,
                'category_id' => $category_id,
            ];
        }
        return $this->saveAll($data);
    }

    /**
     * 更新关系记录
     * @param $ad_id
     * @param array $categoryIds 新的角色集
     * @return array|false
     * @throws \Exception
     */
    public function edit($ad_id, $categoryIds)
    {
        // 已分配的角色集
        $assignCategoryIds = self::getCategoryIds($ad_id);

        /**
         * 找出删除的角色
         * 假如已有的角色集合是A，界面传递过得角色集合是B
         * 角色集合A当中的某个角色不在角色集合B当中，就应该删除
         * 使用 array_diff() 计算补集
         */
        if ($deleteRoleIds = array_diff($assignCategoryIds, $categoryIds)) {
            self::deleteAll(['category_id' => $categoryIds, 'ad_id' => ['in', $deleteRoleIds]]);
        }

        /**
         * 找出添加的角色
         * 假如已有的角色集合是A，界面传递过得角色集合是B
         * 角色集合B当中的某个角色不在角色集合A当中，就应该添加
         * 使用 array_diff() 计算补集
         */
        $categoryIds = array_diff($categoryIds, $assignCategoryIds);
        $data = [];
        foreach ($categoryIds as $categoryId) {
            $data[] = [
                'ad_id' => $ad_id,
                'category_id' => $categoryId,
            ];
        }
        return $this->saveAll($data);
    }

    /**
     * @param $ad_id
     * @return array
     */
    public static function getCategoryIds($ad_id)
    {
        return (new self)->where('ad_id', '=', $ad_id)->column('category_id');
    }

    /**
     * 删除记录
     * @param $where
     * @return int
     */
    public static function deleteAll($where)
    {
        return self::destroy($where);
    }

}