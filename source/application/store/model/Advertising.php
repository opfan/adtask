<?php

namespace app\store\model;

use app\common\model\Advertising as AdvertisingModel;
use Lvht\GeoHash;

/**
 * Class Advertising
 * @package app\store\model
 */
class Advertising extends AdvertisingModel
{
    /**
     * @param $data
     * @return bool
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function add($data)
    {
        $user = User::get($data['user_id']);
        if(!$user){
            $this->error = '用户不存在';
            return false;
        }
        if(empty($data['urls'])){
            $this->error = '请上传广告图片';
            return false;
        }
        $data['user_id'] = $user['user_id'];
        !empty($data['stime']) && $data['stime'] = strtotime($data['stime']);
        !empty($data['etime']) &&  $data['etime'] = strtotime($data['etime']);
        //默认已支付
        $data['pay_status'] = 20;
        if ($data['ad_type'] == 2) {
            $data['address'] = [];
            $data['address'][] = [
                'longitude' => $user['longitude'],
                'latitude'  => $user['latitude'],
                'province'  => $user['province'],
                'city'      => $user['city'],
                'region'    => $user['region'] ?? '',
                'address'   => $user['province'].$user['city'] ?? '',
            ];
        }
    
        !empty($data['category']) && $data['category'] = array_filter($data['category']);
        
        // 开启事务
        return parent::add($data);
    }

    /**
     * @param $data
     * @return bool|mixed
     * @throws \think\exception\DbException
     */
    public function edit($data)
    {
        $user = User::get(['user_id'=> $data['user_id']]);
        if(!$user){
            $this->error = '用户不存在';
            return false;
        }
        if(empty($data['urls'])){
            $this->error = '请上传广告图片';
            return false;
        }
        if ($data['ad_type'] == 2) {
            $data['address'] = [];
            $data['address'][] = [
                'longitude' => $user['longitude'],
                'latitude'  => $user['latitude'],
                'province'  => $user['province'],
                'city'      => $user['city'],
                'region'    => $user['region'] ?? '',
                'address'   => $user['province'].$user['city'] ?? '',
            ];
        }
        
        !empty($data['category']) && $data['category'] = array_filter($data['category']);
    
        !empty($data['stime']) && $data['stime'] = strtotime($data['stime']);
        !empty($data['etime']) &&  $data['etime'] = strtotime($data['etime']);
        
        return $this->transaction(function () use ($data) {
            // 保存商品
            $this->allowField(true)->save($data);
            // 分类
            !empty($data['category']) &&  (new AdvertisingCategory())->edit($this['ad_id'],$data['category']);
            //职业
            !empty($data['position']) && (new AdvertisingPosition())->edit($this['ad_id'],$data['position']);
            // 广告
            $this->addUrl($data['urls']);
            return true;
        });
    }
    
    /**
     * @desc  getCount
     * @param $data
     * @return int|string
     * @throws \think\Exception
     */
    public function getCount($data)
    {
        $filter = [
            'pay_status' => 20,
            'is_delete'  => 0,
            'status'     => 20,
            'state'      => 10,
            'create_time'  => ['between',$data['time']]
        ];
        
        return $this->where($filter)->count('*');
    }
    /**
     * 获取当前商品总数
     * @param array $where
     * @return int|string
     * @throws \think\Exception
     */
    public function getAdTotal($where = [])
    {
        $filter = [
            'pay_status' => 20,
            'is_delete'  => 0,
        ];
        return $this->where($filter)->where($where)->count();
    }
}
