<style>
    .item-group{
        margin: 0 2rem;
        padding-top: .8rem;
        font-size: 1.34rem;
        color: #656565;
        font-weight: inherit;
        text-align: left;
    }
    .item-group input{
        width: 8em !important;
        display: inline !important;
    }
</style>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">活动设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> 是否开启 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="activity[is_open]" value="1" data-am-ucheck
                                            <?= $values['is_open'] ? 'checked' : '' ?>> 开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="activity[is_open]" value="0" data-am-ucheck
                                            <?= $values['is_open'] ? '' : 'checked' ?>> 关闭
                                    </label>
                                </div>
                            </div>

                            <div class="am-form-inline item-group ">
                                1.用户微信自动登录可以获得<input type="number" class="am-form-field "  name="activity[login_money]"
                                                 value="<?= $values['login_money'] ?? ''?>" >元

                            </div>
                            
                            
                            <div class="am-form-inline item-group ">
                                2.分享给本平台微信朋友，微信朋友成为平台用户后，分享者可以获得<input type="number" class="am-form-field "  name="activity[share_money]"  value="<?= $values['share_money'] ?? ''?>" >元

                            </div>

                            <div class="am-form-inline item-group ">
                                3.获得金额只能用于投放广告，不能提现。每次投广告只能<input type="number" class="am-form-field "  name="activity[rate]" value="<?= $values['rate'] ?? ''?>" >%元。

                            </div>
                            <div class="am-form-inline item-group ">
                                4.每个会员都有自己的二维码，当b扫a的二维码成为会员，b会员完成每个信息转发，a可以获得b每单佣金的<input type="number" class="am-form-field "  name="activity[commission_rate]" value="<?= $values['commission_rate'] ?? ''?>" >%元。

                            </div>

                            <div class="am-form-inline item-group ">
                                5.用户填写职业和行业可以获得<input type="number" class="am-form-field "  name="activity[click]"
                                                     value="<?= $values['click'] ?? ''?>" >曝光量用于投放免费广告

                            </div>
                            
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();

    });
</script>
