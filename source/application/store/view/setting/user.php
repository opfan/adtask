<div class="row-content ">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">添加用户</div>
                            </div>
                            <div class="">
                                <table width="100%" class="am-table am-table-bordered am-table-striped am-text-nowrap ">
                                    <thead>
                                    <tr >
                                        <th style="min-width: 80px">昵称</th>
                                        <th>平台红包</th>
                                        <th>可用佣金</th>
                                        <th>职业</th>
                                        <th>行业</th>
                                        <th>性别</th>
                                        <th style="min-width: 320px;">省份城市县</th>
                                        <th style="min-width: 100px;">详细地址</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (!empty($limit)): for ($i=0;$i < $limit;$i++): ?>
                                        <tr>
<!--                                            昵称-->
                                            <td class="am-text-middle">
                                                <input type="text" class="tpl-form-input" name="user[<?=$i;?>][nickName]"
                                                       value="" required>
                                            </td>
<!--                                            平台红包-->
                                            <td class="am-text-middle">
                                                <input type="text" class="tpl-form-input" name="user[<?=$i;?>][balance]"
                                                       value="" required>
                                            </td>
<!--                                            可用佣金-->
                                            <td class="am-text-middle">
                                                <input type="text" class="tpl-form-input" name="user[<?=$i;?>][money]"
                                                       value="" required>
                                            </td>
<!--                                            职业-->
                                            <td class="am-text-middle">
                                                <select name="user[<?=$i;?>][position_id]"
                                                        data-am-selected="{searchBox: 1, btnSize: 'sm',btnWidth:80,placeholder:'职业', maxHeight: 400}">
                                                    <option value=""></option>
                                                    <?php if (isset($position)): foreach ($position as $first): ?>
                                                        <option value="<?= $first['position_id'] ?>" ><?= $first['name'] ?></option>
                                                    <?php endforeach; endif; ?>
                                                </select>
                                            </td>
<!--                                            行业-->
                                            <td class="am-text-middle">

                                                <select name="user[<?=$i;?>][category_id]"
                                                        data-am-selected="{searchBox: 1,btnWidth:80,btnSize: 'sm',  placeholder: '行业', maxHeight: 400}">
                                                    <option value=""></option>
                                                    <?php if (isset($catgory)): foreach ($catgory as $first): ?>
                                                        <option value="<?= $first['category_id'] ?>">
                                                            <?= $first['name'] ?></option>
                                                        <?php if (isset($first['child'])): foreach ($first['child'] as $two): ?>
                                                            <option value="<?= $two['category_id'] ?>">
                                                                　　<?= $two['name'] ?></option>
                                                        <?php endforeach; endif; ?>
                                                    <?php endforeach; endif; ?>
                                                </select>
                                            </td>
<!--                                            性别-->
                                            <td class="am-text-middle">
                                                <select name="user[<?=$i;?>][gender]"
                                                        data-am-selected="{searchBox: 1, btnSize: 'sm',
                                             placeholder:'请选性别', maxHeight: 400,btnWidth:50}">
                                                    <option value="1">男</option>

                                                        <option value="2" >女</option>
                                                </select>
                                            </td>

<!--                                            省份-->
                                            <td class="am-text-middle" style="width:330px">
                                                <div class="x-region-select" data-region-selected>
                                                    <select name="user[<?=$i;?>][province]"
                                                            data-province
                                                            data-id=""
                                                            data-am-selected="{btnSize: 'sm', btnWidth:100,placeholder: '请选择省份'}">
                                                        <option value=""></option>
                                                    </select>

                                                    <select name="user[<?=$i;?>][city]"
                                                            data-city
                                                            data-id=""
                                                            data-am-selected="{btnSize: 'sm',btnWidth:100, placeholder: '请选择城市'}">
                                                        <option value=""></option>
                                                    </select>
                                                    <select name="user[<?=$i;?>][region]"
                                                            data-region
                                                            data-id=""
                                                            data-am-selected="{btnSize: 'sm', btnWidth:100,placeholder: '请选择区县'}">
                                                        <option value=""></option>
                                                    </select>
                                            </div>
                                            </td>

<!--                                            详细地址-->
                                            <td class="am-text-middle " style="width: 200px">
                                                <input type="text" class="tpl-form-input" name="user[<?=$i;?>][address]"
                                                       value="" required>
                                            </td>


                                        </tr>
                                    <?php endfor;  ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>


                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}
<script src="assets/store/js/select.region.js?v=1.2"></script>

<script>
    $(function () {

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();


    });


</script>
