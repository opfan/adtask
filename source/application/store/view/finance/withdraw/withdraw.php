<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">提现设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">满 </label>
                                <div class="am-u-sm-9">
                                    <div class="am-u-sm-1">
                                        <input type="number" class="am-form-field" name="withdraw[money]"
                                               value="<?= $values['money'] ?? ''?>" required>
                                    </div>
                                    <label class=" am-form-label am-text-left">元才可以提现。</label>

                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> </label>
                                <div class="am-u-sm-9">
                                    <div class="am-u-sm-1">
                                        <input type="number" class="am-form-field" name="withdraw[days]"
                                               value="<?= $values['days']??'' ?>" required>
                                    </div>
                                    <label class="am-form-label am-text-left">天才能提现一次。</label>

                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();

    });
</script>
