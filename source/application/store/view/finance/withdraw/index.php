<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">提现申请</div>
                </div>
                <div class="widget-body am-fr">
                    <!-- 工具栏 -->
                    <div class="page_toolbar am-margin-bottom-xs am-cf">
                        <form class="toolbar-form" action="">
                            <input type="hidden" name="s" value="/<?= $request->pathinfo() ?>">
                            <input type="hidden" name="user_id" value="<?= $request->get('user_id') ?>">
                            <div class="am-u-sm-12 am-u-md-9 am-u-sm-push-3">
                                <div class="am fr">
                                    <div class="am-form-group am-fl">
                                        <select name="status"
                                                data-am-selected="{btnSize: 'sm', placeholder: '审核状态'}">
                                            <option value=""></option>
                                            <option value="-1" <?= $request->get('status') === '-1' ? 'selected' : '' ?>>
                                                全部
                                            </option>
                                            <option value="10" <?= $request->get('status') == '10' ? 'selected' : '' ?>>
                                                待结算
                                            </option>
                                            <option value="20" <?= $request->get('status') == '20' ? 'selected' : '' ?>>
                                                已结算
                                            </option>
                                        </select>
                                    </div>
                                    <div class="am-form-group tpl-form-border-form am-fl">
                                        <input type="text" name="start_time"
                                               autocomplete="off"
                                               class="am-form-field"
                                               value="<?= $request->get('start_time') ?>" placeholder="请选择起始日期"
                                               data-am-datepicker>
                                    </div>
                                    <div class="am-form-group tpl-form-border-form am-fl">
                                        <input type="text" name="end_time"
                                               autocomplete="off"
                                               class="am-form-field"
                                               value="<?= $request->get('end_time') ?>" placeholder="请选择截止日期"
                                               data-am-datepicker>
                                    </div>
                                    
                                    <div class="am-form-group am-fl">
                                        <div class="am-input-group am-input-group-sm tpl-form-border-form">
                                            <input type="text" class="am-form-field" name="search"
                                                   placeholder="请输入昵称"
                                                   value="<?= $request->get('search') ?>">
                                            <div class="am-input-group-btn">
                                                <button class="am-btn am-btn-default am-icon-search"
                                                        type="submit"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12 am-padding-bottom-lg">
                        <span class="am-align-left am-u-sm-3">已结算：<?=$count['used']?></span>
                        <span class="am-align-left am-u-sm-3">待结算：<?=$count['pedding']?></span>
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>用户ID</th>
                                <th>微信昵称</th>
                                <th>手机号</th>
                                <th>申请提现金额</th>
                                <th>打款金额</th>
                                <th class="am-text-center">结算状态</th>
                                <th>申请时间</th>
                                <th>打款时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['user_id'] ?></td>
                                    <td class="am-text-middle">
                                        <p><span><?= $item['user']['nickName'] ?></span></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['user']['phone'] ?: '--' ?>
                                    </td>
                                    <td class="am-text-middle">
                                        <p><span><?= $item['apply_money'] ?></span></p>
                                    </td>
                                    <td class="am-text-middle">
                                        <p><span><?= $item['money'] ?></span></p>
                                    </td>
                                    <td class="am-text-middle am-text-center">
                                        <?php if ($item['status'] == 10) : ?>
                                            <span class="am-badge">待结算</span>
                                        <?php elseif ($item['status'] == 20) : ?>
                                            <span class="am-badge am-badge-secondary">已结算</span>
                                        <?php endif; ?>
                                    </td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle"><?= $item['audit_time'] ?: '--' ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <?php if ($item['status'] == 10) : ?>
                                                <?php if (checkPrivilege('finance.withdraw/money')): ?>
                                                    <a class="j-money tpl-table-black-operation-del"
                                                       data-id="<?= $item['id'] ?>"
                                                       data-src="<?= $item['image']['file_path'] ?? '' ?>"
                                                       data-money="<?= $item['user']['money'] ?? '' ?>"
                                                       href="javascript:void(0);">结算
                                                    </a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="11" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="am-u-lg-12 am-cf">
                            <div class="am-fr"><?= $list->render() ?> </div>
                            <div class="am-fr pagination-total am-margin-right">
                                <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 提现审核 -->
<script id="tpl-dealer-withdraw" type="text/template">
    <div class="am-padding-top-sm">
        <form class="form-dealer-withdraw am-form tpl-form-line-form" method="post"
              action="<?= url('finance.withdraw/money') ?>">
            <input type="hidden" name="id" value="{{ id }}">
           
            <div class="am-form-group">
                <label class="am-u-sm-3 am-form-label">打款金额 </label>
                <div class="am-u-sm-9">
                    <input type="text" class="tpl-form-input" name="money" placeholder="最多可提现{{money}}元"
                           value="">
                </div>
            </div>
            <div class="am-form-wi">
                <label class="am-u-sm-4 am-form-label">收款二维码 </label>
                <div>
                    <a href="{{src}}"
                       title="点击查看大图" target="_blank">
                        <img src="{{src}}"
                             width="100" height="100" alt="商品图片">
                    </a>
                </div>
                
            </div>
        </form>
    </div>
</script>

<script>
    $(function () {

        /**
         * 审核操作
         */
        $('.j-money').click(function () {
            var $this = $(this);
            layer.open({
                type: 1
                , title: '结算'
                , area: '340px'
                , offset: 'auto'
                , anim: 1
                , closeBtn: 1
                , shade: 0.3
                , btn: ['确定', '取消']
                , content: template('tpl-dealer-withdraw', $this.data())
                , success: function (layero) {
                    // 注册radio组件
                    layero.find('input[type=radio]').uCheck();
                }
                , yes: function (index, layero) {
                    // 表单提交
                    layero.find('.form-dealer-withdraw').ajaxSubmit({
                        type: 'post',
                        dataType: 'json',
                        success: function (result) {
                            result.code === 1 ? $.show_success(result.msg, result.url)
                                : $.show_error(result.msg);
                        }
                    });
                    // layer.close(index);
                }
            });
        });

        /**
         * 显示驳回原因
         */
        $('.j-show-reason').click(function () {
            var $this = $(this);
            layer.alert($this.data('reason'), {title: '驳回原因'});
        });

        /**
         * 确认打款
         */
        //$('.j-money').click(function () {
        //    var id = $(this).data('id');
        //    var url = "<?//= url('withdraw/money') ?>//";
        //    layer.confirm('确定已打款吗？', {title: '友情提示'}, function (index) {
        //        $.post(url, {id: id}, function (result) {
        //            result.code === 1 ? $.show_success(result.msg, result.url)
        //                : $.show_error(result.msg);
        //        });
        //        layer.close(index);
        //    });
        //});

        /**
         * 微信付款
         */
        $('.j-wechat-pay').click(function () {
            var id = $(this).data('id');
            var url = "<?= url('apps.dealer.withdraw/wechat_pay') ?>";
            layer.confirm('该操作 将使用微信支付企业付款到零钱功能，确定打款吗？', {title: '友情提示'}, function (index) {
                $.post(url, {id: id}, function (result) {
                    result.code === 1 ? $.show_success(result.msg, result.url)
                        : $.show_error(result.msg);
                });
                layer.close(index);
            });
        });

    });
</script>

