<link rel="stylesheet" href="assets/layui/css/layui.css"  media="all">
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">编辑广告</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="ad_name"
                                           value="<?= $model['ad_name'] ?>" placeholder="请输入名称" required>
                                </div>
                            </div>
                            
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">描述 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="describe"
                                           value="<?= $model['describe'] ?>" placeholder="请输入描述" required>
                                </div>
                            </div>
                            <div class="am-form-group" data-x-switch>
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">广告类型 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ad_type" value="1" <?= $model['ad_type'] == 1 ? 'checked' : '' ?>
                                               data-am-ucheck
                                               data-switch-box="switch-ad_type"
                                               data-switch-item="ad_type__1">
                                        商业广告
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ad_type" value="2"
                                            <?= $model['ad_type'] == 2 ? 'checked' : '' ?>
                                               data-am-ucheck
                                               data-switch-box="switch-ad_type"
                                               data-switch-item="ad_type__2">
                                        公益广告
                                    </label>
                                </div>
                            </div>
                            
                            <div class="am-form-group switch-ad_type ad_type__1 <?= $model['ad_type'] == 1 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">广告分类 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="category[]" multiple
                                            data-am-selected="{searchBox: 1, btnSize: 'sm',
                                             placeholder:'请选择分类', maxHeight: 400}">
                                        <option value="0" <?= empty($cateIds) ? 'selected' : '' ?>>全部</option>
                                        <?php if (isset($category)): foreach ($category as $first): $_category[$first['category_id']] = $first;?>
                                            <option value="<?= $first['category_id'] ?>"  <?= in_array($first['category_id'], $cateIds) ? 'selected' : '' ?>>
                                                <?= $first['name'] ?></option>
                                            <?php if (isset($first['child'])): foreach ($first['child'] as $two): $_category[$two['category_id']] = $two;?>
                                                <option value="<?= $two['category_id'] ?>"  <?= in_array($two['category_id'], $cateIds) ? 'selected' : '' ?>>
                                                    　　<?= $two['name'] ?></option>
                                                <?php if (isset($two['child'])): foreach ($two['child'] as $three): $_category[$three['category_id']] = $three;?>
                                                    <option value="<?= $three['category_id'] ?>" <?= in_array($two['category_id'], $cateIds) ? 'selected' : '' ?>>
                                                        　　　<?= $three['name'] ?></option>
                                                <?php endforeach; endif; ?>
                                            <?php endforeach; endif; ?>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group switch-ad_type ad_type__1 <?= $model['ad_type'] == 1 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">职业 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="position[]" multiple
                                            data-am-selected="{searchBox: 1, btnSize: 'sm',
                                             placeholder:'请选择职业', maxHeight: 400}">
                                        <option value=""></option>
                                        <?php if (isset($position)): foreach ($position as $first): ?>
                                            <option value="<?= $first['position_id'] ?>" <?= in_array($first['position_id'], $positionIds) ? 'selected' : '' ?>><?= $first['name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="am-form-group switch-ad_type ad_type__1 <?= $model['ad_type'] == 1 ? '' : 'hide' ?>" id="address-select">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 投放位置 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div id="coordinate">
                                        <div class="item-content" v-for="(item,index) in address">
                                            <input type="text" style="background: none !important; width: 30%;display: inline-block"
                                                   class="tpl-form-input"
                                                   placeholder="请选择坐标"
                                                   @click="changeIndex(index)"
                                                   required v-model='item.point'>

                                            <div class="am-g am-form-field" style="width: 60%;display: inline-block">
                                                <div class="am-u-sm">省份：{{item.address}}</div>
                                            </div>
                                            <span v-if='address.length != 1' class="am-btn am-btn-secondary am-margin-left-xl" style="padding: 0.2em 1em" @click="address.splice(index,1)">删除</span>
                                        </div>

                                    </div>


                                    <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                        <span class="am-btn am-btn-secondary" id="add_point">添加位置
                                        </span>
                                    </div>
                                    <div class="am-block am-padding-top-xs">
                                        <iframe id="map" src="<?= url('advertising/get_point') ?>"
                                                width="915"
                                                height="610" style="display: block !important;"></iframe>
                                    </div>
                                </div>
                            </div>

                            <div class="am-form-group switch-ad_type ad_type__1 <?= $model['ad_type'] == 1 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">红包 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="1" class="tpl-form-input" name = "money" value="<?= $model['money']?>" placeholder="请输入红包金额" required>
                                </div>
                            </div>

                            <div class="am-form-group switch-ad_type ad_type__1 <?= $model['ad_type'] == 1 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">曝光量 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="1" class="tpl-form-input" name="click"
                                           value="<?= $model['click']?>" placeholder="请输入红包金额" required>
                                </div>
                            </div>

                            <div class="am-form-group switch-ad_type ad_type__1 <?= $model['ad_type'] == 1 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">佣金 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="1" class="tpl-form-input" name="extend[commission][0][money]"
                                           value="<?= $model['extend']['commission'][0]['money']?>" placeholder="请输入佣金一" required>
                                    <input type="text" min="1" class="tpl-form-input" name="extend[commission][0][rate]"
                                           value="<?= $model['extend']['commission'][0]['rate']?>" placeholder="请输入佣金概率一" required>


                                    <input type="number" min="0" class="tpl-form-input" name="extend[commission][1][money]"
                                           value="<?= $model['extend']['commission'][1]['money']?>" placeholder="请输入佣金二" >
                                    <input type="text" min="0" class="tpl-form-input" name="extend[commission][1][rate]"
                                           value="<?= $model['extend']['commission'][1]['rate']?>" placeholder="请输入佣金概率二" >
                                    <input type="number" min="0" class="tpl-form-input" name="extend[commission][2][money]"
                                           value="<?= $model['extend']['commission'][2]['money']?>" placeholder="请输入佣金三" >
                                    <input type="text" min="0" class="tpl-form-input" name="extend[commission][2][rate]"
                                           value="<?= $model['extend']['commission'][2]['rate']?>" placeholder="请输入佣金概率三" >
                                </div>
                            </div>

                            <div class="am-form-group switch-ad_type ad_type__2 <?= $model['ad_type'] == 2 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">时间范围 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="j-startTime am-form-field am-margin-bottom-sm"
                                           name="stime"  value="<?= $model['stime'] > 0 ? date('Y-m-d',$model['stime']) : ''?>" placeholder="请选择开始日期" required>
                                    <input type="text" class="j-endTime am-form-field" name="etime" value="<?= $model['etime'] > 0 ?date('Y-m-d',$model['etime']) : ''?>"
                                           placeholder="请选择结束日期" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">人群性别: </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="sex" value="0" <?= $model['sex'] == 0 ? 'checked' : '' ?> data-am-ucheck>
                                        全部
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="sex" value="1" <?= $model['sex'] == 1 ? 'checked' : '' ?> data-am-ucheck>
                                        男
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="sex" value="2" <?= $model['sex'] == 2 ? 'checked' : '' ?>data-am-ucheck>
                                        女
                                    </label>
                                </div>
                            </div>

                            <div class="am-form-group" data-x-switch>
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">播放类型 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="play_type" value="1" <?= $model['play_type'] == 1 ? 'checked' : '' ?>
                                               data-am-ucheck
                                               data-switch-box="switch-play_type"
                                               data-switch-item="play_type__1">
                                        图片
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="play_type" value="2" <?= $model['play_type'] == 2 ? 'checked' : '' ?>
                                               data-am-ucheck
                                               data-switch-box="switch-play_type"
                                               data-switch-item="play_type__2">
                                        视频
                                    </label>
                                </div>
                            </div>

                            <div class="am-form-group switch-play_type play_type__2 layui-upload <?= $model['play_type'] == 1 ? 'hide' : '' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">视频广告封面图 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class=" upload-file upload-logo am-btn am-btn-secondary am-radius">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
                                                <div class="file-item">
                                                    <a href="<?= $model['thumb_path'] ?>" title="点击查看大图" target="_blank">
                                                        <img src="<?= $model['thumb_path'] ?>">
                                                    </a>
                                                    <input type="hidden" name="thumb_id"
                                                           value="<?= $model['thumb_id'] ?>">
                                                    <i class="iconfont icon-shanchu file-item-delete"></i>
                                                </div>
                                            </div>
                                           
                                            <div class="help-block am-margin-top-sm">
                                                <small>图片格式必须为：png,bmp,jpeg,jpg；不可大于2M；建议使用png格式图片，以保持最佳效果；</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="am-form-group switch-play_type play_type__1 layui-upload <?= $model['play_type'] == 2 ? 'hide' : '' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">图片广告 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button" class="layui-btn layui-btn-normal" id="uploadImage">选择图片</button>
                                        </div>
                                        <div class="help-block am-margin-top-sm">
                                            <small>大小4M以下</small>
                                        </div>
                                    </div>
                                </div>
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label"></label>
                                <div class="layui-upload-list am-u-sm-9 am-u-end">
                                    <table class="layui-table">
                                        <thead>
                                        <tr><th>文件名</th>
                                            <th>大小</th>
                                            <th>广告地址</th>
                                            <th>状态</th>
                                            <th>操作</th>
                                        </tr></thead>
                                        <tbody id="ImageList">
                                        <?php foreach ($model['url'] as $key=>$item): ?>
                                        <tr id="upload-'+ index +'"><td><a href="<?=$item['file_path']?>"><?=$item['file_name']?></a></td><td><?=$item['file_size'].'kb'?></td>
                                            <td><input type="text" class= "adurl" name="urls[<?=$key?>][ad_url]" value="<?=$item['ad_url']?>" ><input type="hidden" class="image" name="urls[<?=$key?>][file_id]" value="<?=$item['file_id']?>" ></td>
                                            <td>成功</td>
                                            <td><button class="layui-btn layui-btn-xs demo-delete layui-hide">重传</button></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!--                                <button type="button" class="layui-btn" id="upload_file_1">开始上传</button>-->

                            </div>

                            <div class="am-form-group switch-play_type play_type__2 layui-upload <?= $model['play_type'] == 1 ? 'hide' : '' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">视频广告 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button" class="layui-btn layui-btn-normal" id="uploadFile">选择视频</button>
                                        </div>
                                    </div>
                                </div>
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label"></label>
                                <div class="layui-upload-list am-u-sm-9 am-u-end">
                                    <table class="layui-table">
                                        <thead>
                                        <tr><th>文件名</th>
                                            <th>大小</th>
                                            <th>广告地址</th>
                                            <th>状态</th>
                                            <th>操作</th>
                                        </tr></thead>
                                        <tbody id="fileList">
                                        <?php foreach ($model['url'] as $key=>$item): ?>
                                            <tr id="upload-'+ index +'"><td><a href="<?=$item['file_path']?>"><?=$item['file_name']?></a></td><td><?=$item['file_size'].'kb'?></td>
                                                <td><input type="text" class= "adurl" name="urls[<?=$key?>][ad_url]" value="<?=$item['ad_url']?>" ><input type="hidden" class="image" name="urls[<?=$key?>][file_id]" value="<?=$item['file_id']?>" ></td>
                                                <td>成功</td>
                                                <td><button class="layui-btn layui-btn-xs demo-delete layui-hide">重传</button></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!--                                <button type="button" class="layui-btn" id="upload_file">开始上传</button>-->
                            </div>


                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设置问题: </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="extend[question][title]"
                                           value="<?= $model['extend']['question']['title'] ?? '' ?>" placeholder="请输入问题" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设置答案：</label>
                                <div class="am-u-sm-9 am-u-end">

                                    <input type="text" class="tpl-form-input" name="extend[question][answer][A]"
                                               value="<?= $model['extend']['question']['answer']['A']??'' ?>" placeholder="请输入答案A" required>
                                    <input type="text" class="tpl-form-input" name="extend[question][answer][B]"
                                           value="<?= $model['extend']['question']['answer']['B']?? '' ?>" placeholder="请输入答案B" required>
                                    
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设置正确答案：</label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="extend[question][right]"
                                           value="<?= $model['extend']['question']['right'] ?? '' ?>" placeholder="请输入答案" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">客户名称：</label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="user_name"
                                           value="<?= $model['user_name'] ?>" placeholder="请输入客户名称" >
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">联系电话：</label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="phone"
                                           value="<?= $model['phone'] ?>" placeholder="请输入联系电话" >
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">用户编号：</label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="user_id"
                                           value="<?= $model['user_id'] ?>" placeholder="用户id" required>
                                </div>
                            </div>

                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="assets/common/js/vue.min.js"></script>
<script src="assets/common/js/ddsort.js"></script>
<script src="assets/store/js/app.js"></script>
<script src="assets/layui/layui.js"></script>
<script>

    var addressData = {address:<?=$model['address'];?>,show:false,active:0};

    /**
     * 位置选择
     */
    var address = new Vue({
        el: '#address-select',
        data:addressData,
        methods:{
            changeIndex(index){
                address._data.active = index;
            }
        }
    });

    // 页面闪烁
    // setTimeout(() => {
    //     $('#address-select').show()
    // }, 100);
    var address_num = <?=$config['address_num']?>;
    $('#add_point').click(function(){
        // if(address._data.address.length == address_num) return
        address._data.address.push({point:'',province:'',city:'',region:'',address:''})
    })

    /**
     * 设置坐标
     */
    function setCoordinate(value) {
        var index = address._data.active;

        address._data.address[index].point= value;
    }

    /**
     * 设置地址
     */
    function setAddress(value) {
        var index = address._data.active;
        address._data.address[index].city = value.city;
        address._data.address[index].region = value.district;
        address._data.address[index].province = value.province;
        address._data.address[index].address = value.address;
    }
</script>

<script>

    layui.use('upload', function() {
        var $ = layui.jquery
            , upload = layui.upload,
            imageUrl = "<?=url('upload/image')?>";
        var imageListView = $('#ImageList')
            ,uploadListIns = upload.render({
            elem: '#uploadImage'
            ,url: imageUrl //改成您自己的上传接口
            ,accept: 'image'
            ,multiple: true
            ,auto: true,
            field:'iFile',
            // bindAction: '#upload_file_1',
            choose: function(obj){
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function(index, file, result){
                    var tr = $(['<tr id="upload-'+ index +'">'
                        ,'<td>'+ file.name +'</td>'
                        ,'<td>'+ (file.size/1024).toFixed(1) +'kb</td>'
                        ,'<td><input type="text" class= "adurl" name="urls['+index+'][ad_url]" value="" ><input type="hidden" class="image" name="urls['+index+'][file_id]" value="" ></td>'
                        ,'<td></td>'
                        ,'<td>'
                        ,'<button class="layui-btn layui-btn-xs demo-delete ">删除</button>'
                        ,'</td>'
                        ,'</tr>'].join(''));

                    //单个重传
                    tr.find('.demo-reload').on('click', function(){
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function(){
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    imageListView.append(tr);
                });
            }
            ,done: function(res, index, upload){
                if(res.code){ //上传成功
                    var tr = imageListView.find('tr#upload-'+ index)
                        ,tds = tr.children();
                    tds.eq(3).html('<span style="color: #5FB878;">上传成功</span>');
                    tds.eq(2).find('.image').val(res.data.file_id);
                    // tds.eq(4).html(''); //清空操作
                    return delete this.files[index]; //删除文件队列已经上传成功的文件
                }
                this.error(index, upload);
            }
            ,error: function(index, upload){
                var tr = imageListView.find('tr#upload-'+ index)
                    ,tds = tr.children();
                tds.eq(3).html('<span style="color: #FF5722;">上传失败</span>');
                // tds.eq(4).find('.demo-delete').removeClass('layui-hide'); //显示重传
            }
        });

        var fileUrl = "<?=url('upload/file')?>";
        var fileListView = $("#fileList")
            ,fileListIns = upload.render({
            elem: '#uploadFile'
            ,url: fileUrl //改成您自己的上传接口
            ,accept: 'file'
            ,multiple: true
            ,auto: true,
            field:'iFile',
            // ,bindAction: '#upload_file',
            choose: function(obj){
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function(index, file, result){
                    var tr = $(['<tr id="upload-'+ index +'">'
                        ,'<td>'+ file.name +'</td>'
                        ,'<td>'+ (file.size/1024).toFixed(1) +'kb</td>'
                        ,'<td><input type="text" class= "adurl" name="urls['+index+'][ad_url]" value="" ><input type="hidden" class="image" name="urls['+index+'][file_id]" value=""></td>'
                        ,'<td></td>'
                        ,'<td>'
                        ,'<button class="layui-btn layui-btn-xs demo-delete ">删除</button>'
                        ,'</td>'
                        ,'</tr>'].join(''));

                    //单个重传
                    tr.find('.demo-reload').on('click', function(){
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function(){
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        fileListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    fileListView.append(tr);
                });
            }
            ,done: function(res, index, upload){
                if(res.code){ //上传成功
                    var tr = fileListView.find('tr#upload-'+ index)
                        ,tds = tr.children();
                    tds.eq(3).html('<span style="color: #5FB878;">上传成功</span>');
                    tds.eq(2).find('.image').val(res.data.file_id);
                    // tds.eq(4).html(''); //清空操作
                    return delete this.files[index]; //删除文件队列已经上传成功的文件
                }
                this.error(index, upload);
            }
            ,error: function(index, upload){
                var tr = fileListView.find('tr#upload-'+ index)
                    ,tds = tr.children();
                tds.eq(3).html('<span style="color: #FF5722;">上传失败</span>');
                // tds.eq(4).find('.demo-delete').removeClass('layui-hide'); //显示重传
            }
        });
    });

</script>



<script>
    /**
     * 时间选择
     */
    $(function () {
        var nowTemp = new Date();
        var nowDay = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0).valueOf();
        var nowMoth = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), 1, 0, 0, 0, 0).valueOf();
        var nowYear = new Date(nowTemp.getFullYear(), 0, 1, 0, 0, 0, 0).valueOf();
        var $startTime = $('.j-startTime');
        var $endTime = $('.j-endTime');

        var checkin = $startTime.datepicker({
            onRender: function (date, viewMode) {
                // 默认 days 视图，与当前日期比较
                var viewDate = nowDay;
                switch (viewMode) {
                    // moths 视图，与当前月份比较
                    case 1:
                        viewDate = nowMoth;
                        break;
                    // years 视图，与当前年份比较
                    case 2:
                        viewDate = nowYear;
                        break;
                }
                return date.valueOf() < viewDate ? 'am-disabled' : '';
            }
        }).on('changeDate.datepicker.amui', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.close();
            $endTime[0].focus();
        }).data('amui.datepicker');

        var checkout = $endTime.datepicker({
            onRender: function (date, viewMode) {
                var inTime = checkin.date;
                var inDay = inTime.valueOf();
                var inMoth = new Date(inTime.getFullYear(), inTime.getMonth(), 1, 0, 0, 0, 0).valueOf();
                var inYear = new Date(inTime.getFullYear(), 0, 1, 0, 0, 0, 0).valueOf();
                // 默认 days 视图，与当前日期比较
                var viewDate = inDay;
                switch (viewMode) {
                    // moths 视图，与当前月份比较
                    case 1:
                        viewDate = inMoth;
                        break;
                    // years 视图，与当前年份比较
                    case 2:
                        viewDate = inYear;
                        break;
                }
                return date.valueOf() <= viewDate ? 'am-disabled' : '';
            }
        }).on('changeDate.datepicker.amui', function (ev) {
            checkout.close();
        }).data('amui.datepicker');
    });
</script>

<script>
    $(function () {

        // swith切换
        var $mySwitch = $('[data-x-switch]');
        $mySwitch.find('[data-switch-item]').click(function () {
            var $mySwitchBox = $('.' + $(this).data('switch-box'));
            $mySwitchBox.hide().filter('.' + $(this).data('switch-item')).show();
        });


        // 选择图片
        $('.upload-logo').selectImages({
            name: 'thumb_id'
        });

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm({
            buildData: function () {
                let addressData = address._data.address;
                return {
                    'address':addressData
                };
            },
        });

    });
</script>
