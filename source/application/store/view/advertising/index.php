<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">广告列表</div>
                </div>
                <div class="widget-body am-fr">
                    <!-- 工具栏 -->
                    <div class="page_toolbar am-margin-bottom-xs am-cf">
                        <form class="toolbar-form" action="">
                            <input type="hidden" name="s" value="/<?= $request->pathinfo() ?>">
                            <div class="am-u-sm-12 am-u-md-2">
                                <div class="am-form-group">
                                    <?php if (checkPrivilege('advertising/add')): ?>
                                        <div class="am-btn-group am-btn-group-xs">
                                            <a class="am-btn am-btn-default am-btn-success"
                                               href="<?= url('advertising/add') ?>">
                                                <span class="am-icon-plus"></span> 新增
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="am-u-sm-12 am-u-md-10">
                                <div class="am fr">
                                    <div class="am-form-group am-fl">
                                        <?php $category_id = $request->get('category_id') ?: null; ?>
                                        <select name="category_id"
                                                data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder: '广告分类', maxHeight: 400}">
                                            <option value=""></option>
                                            <?php if (isset($catgory)): foreach ($catgory as $first): ?>
                                                <option value="<?= $first['category_id'] ?>"
                                                    <?= $category_id == $first['category_id'] ? 'selected' : '' ?>>
                                                    <?= $first['name'] ?></option>
                                                <?php if (isset($first['child'])): foreach ($first['child'] as $two): ?>
                                                    <option value="<?= $two['category_id'] ?>"
                                                        <?= $category_id == $two['category_id'] ? 'selected' : '' ?>>
                                                        　　<?= $two['name'] ?></option>
                                                <?php endforeach; endif; ?>
                                            <?php endforeach; endif; ?>
                                        </select>
                                    </div>
                                    <div class="am-form-group am-fl">
                                        <?php $status = $request->get('status') ?: null; ?>
                                        <select name="status"
                                                data-am-selected="{btnSize: 'sm', placeholder: '审核状态'}">
                                            <option value=""></option>
                                            <option value="10"
                                                <?= $status == 10 ? 'selected' : '' ?>>未审核
                                            </option>
                                            <option value="20"
                                                <?= $status == 20 ? 'selected' : '' ?>>审核通过
                                            </option>
                                            <option value="30"
                                                <?= $status == 30 ? 'selected' : '' ?>>审核失败
                                            </option>
                                        </select>
                                    </div>

                                    <div class="am-form-group am-fl">
                                        <?php $ad_type = $request->get('ad_type') ?: null; ?>
                                        <select name="ad_type"
                                                data-am-selected="{btnSize: 'sm', placeholder: '广告类型'}">
                                            <option value=""></option>
                                            <option value="1"
                                                <?= $ad_type == 1 ? 'selected' : '' ?>>商业
                                            </option>
                                            <option value="2"
                                                <?= $ad_type == 2 ? 'selected' : '' ?>>公益
                                            </option>
                                        </select>
                                    </div>
                                    
                                    <div class="am-form-group am-fl">
                                        <?php $state = $request->get('state') ?: null; ?>
                                        <select name="state"
                                                data-am-selected="{btnSize: 'sm', placeholder: '上下架状态'}">
                                            <option value=""></option>
                                            <option value="10"
                                                <?= $state == 10 ? 'selected' : '' ?>>上架
                                            </option>
                                            <option value="20"
                                                <?= $state == 20 ? 'selected' : '' ?>>下架
                                            </option>
                                        </select>
                                    </div>

                                    <div class="am-form-group am-fl">
                                        <?php $origin = $request->get('origin') ?: null; ?>
                                        <select name="origin"
                                                data-am-selected="{btnSize: 'sm', placeholder: '发布类型'}">
                                            <option value=""></option>
                                            <option value="1"
                                                <?= $origin == 1 ? 'selected' : '' ?>>用户发布
                                            </option>
                                            <option value="2"
                                                <?= $origin == 2 ? 'selected' : '' ?>>系统发布
                                            </option>
                                        </select>
                                    </div>
                                    
                                    <div class="am-form-group am-fl">
                                        <div class="am-input-group am-input-group-sm tpl-form-border-form">
                                            <input type="text" class="am-form-field" name="search"
                                                   placeholder="请输入广告名称"
                                                   value="<?= $request->get('search') ?>">
                                            <div class="am-input-group-btn">
                                                <button class="am-btn am-btn-default am-icon-search"
                                                        type="submit"></button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="am-form-group am-fl">
                                        <a href="<?= url('') ?>" class="am-btn am-btn-default am-btn-sm" style="margin: 0 10px 0 30px;">全部</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>商品名称</th>
                                <th>客户名称</th>
                                <th>手机</th>
                                <th>发布</th>
                                <th>类型</th>
                                <th>审核状态</th>
                                <th>上架状态</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['ad_id'] ?></td>

                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['ad_name'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['user_name'] ?></td>
                                    <td class="am-text-middle"><?= $item['phone'] ?></td>
                                    <td class="am-text-middle"><?= $item['origin'] == 1 ? '用户发布': '后台发布' ?></td>
                                    <td class="am-text-middle">
                                           <span class="x-cur-p"
                                                 data-state="<?= $item['ad_type']['value'] ?>">
                                               <?= $item['ad_type'] == 1? '商业广告' : '公益广告' ?>
                                           </span>
                                    </td>
                                    <td class="am-text-middle">
                                           <span class=" am-badge x-cur-p
                                           am-badge-<?= in_array($item['status']['value'],[ 10,30]) ? 'warning' : 'success' ?>">
                                               <?= $item['status']['text'] ?>
                                           </span>
                                    </td>
                                    <td class="am-text-middle">
                                           <span class="j-state am-badge x-cur-p
                                           am-badge-<?= $item['state']['value'] == 10 ? 'success' : 'warning' ?>"
                                                 data-id="<?= $item['ad_id'] ?>"
                                                 data-state="<?= $item['state']['value'] ?>">
                                               <?= $item['state']['text'] ?>
                                           </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <?php if (checkPrivilege('advertising/view')): ?>
                                                <a href="<?= url('advertising/view',
                                                    ['ad_id' => $item['ad_id']]) ?>">
                                                    <i class="am-icon-pencil"></i> 查看
                                                </a>
                                            <?php endif; ?>
                                            <?php if (checkPrivilege('advertising/edit') && ($item['status']['value'] == 10) && $item['origin'] == 2): ?>
                                                <a href="<?= url('advertising/edit',
                                                    ['ad_id' => $item['ad_id']]) ?>">
                                                    <i class="am-icon-pencil"></i> 编辑
                                                </a>
                                            <?php endif; ?>
                                            <?php if (checkPrivilege('advertising/audit') && $item['status']['value'] == 10): ?>
                                                <a class="j-audit" data-id="<?= $item['ad_id'] ?>"
                                                   href="javascript:void(0);">
                                                    <i class="am-icon-pencil"></i> 审核
                                                </a>
                                            <?php endif; ?>
                                            <?php if (checkPrivilege('advertising/delete')): ?>
                                                <a href="javascript:;" class="item-delete tpl-table-black-operation-del"
                                                   data-id="<?= $item['ad_id'] ?>">
                                                    <i class="am-icon-trash"></i> 删除
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 提现审核 -->
<script id="tpl-dealer-withdraw" type="text/template">
    <div class="am-padding-top-sm">
        <form class="form-dealer-withdraw am-form tpl-form-line-form" method="post"
              action="<?= url('advertising/audit') ?>">
            <input type="hidden" name="ad_id" value="{{ id }}">
            <div class="am-form-group">
                <label class="am-u-sm-3 am-form-label"> 审核状态 </label>
                <div class="am-u-sm-9">
                    <label class="am-radio-inline">
                        <input type="radio" name="status" value="20" data-am-ucheck
                               checked> 审核通过
                    </label>
                    <label class="am-radio-inline">
                        <input type="radio" name="status" value="30" data-am-ucheck> 驳回
                    </label>
                </div>
            </div>
        </form>
    </div>
</script>
<script>
    $(function () {

        /**
         * 审核操作
         */
        $('.j-audit').click(function () {
            var $this = $(this);
            layer.open({
                type: 1
                , title: '审核'
                , area: '340px'
                , offset: 'auto'
                , anim: 1
                , closeBtn: 1
                , shade: 0.3
                , btn: ['确定', '取消']
                , content: template('tpl-dealer-withdraw', $this.data())
                , success: function (layero) {
                    // 注册radio组件
                    layero.find('input[type=radio]').uCheck();
                }
                , yes: function (index, layero) {
                    // 表单提交
                    layero.find('.form-dealer-withdraw').ajaxSubmit({
                        type: 'post',
                        dataType: 'json',
                        success: function (result) {
                            result.code === 1 ? $.show_success(result.msg, result.url)
                                : $.show_error(result.msg);
                        }
                    });
                    layer.close(index);
                }
            });
        });

        $('.j-state').click(function () {
            // 验证权限
            if (!"<?= checkPrivilege('advertising/state')?>") {
                return false;
            }
            var data = $(this).data();
            layer.confirm('确定要' + (parseInt(data.state) === 10 ? '下架' : '上架') + '该广告吗？'
                , {title: '友情提示'}
                , function (index) {
                    $.post("<?= url('advertising/state') ?>"
                        , {
                            ad_id: data.id,
                            state: Number(!(parseInt(data.state) === 10))
                        }
                        , function (result) {
                            result.code === 1 ? $.show_success(result.msg, result.url)
                                : $.show_error(result.msg);
                        });
                    layer.close(index);
                });

        });
        // 删除元素
        var url = "<?= url('advertising/delete') ?>";
        $('.item-delete').delete('ad_id', url);

    });
</script>

