<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">查看广告</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require  ">名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text"  class="tpl-form-input readonly" readonly="readonly" name="ad_name"
                                           value="<?= $model['ad_name'] ?>" placeholder="请输入名称" required>
                                </div>
                            </div>

                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">描述 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="describe"
                                           value="<?= $model['describe'] ?>" placeholder="请输入描述" required>
                                </div>
                            </div>
                            <div class="am-form-group  <?= $model['ad_type'] == 1 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">广告行业 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="category[]" multiple required disabled="disabled"
                                            data-am-selected="{searchBox: 1, btnSize: 'sm',
                                             placeholder:'请选择分类', maxHeight: 400}">
                                        <option value="0" <?= empty($cateIds) ? 'selected' : '' ?>>全部</option>
                                        <?php if (isset($category)): foreach ($category as $first): $_category[$first['category_id']] = $first;?>
                                            <option value="<?= $first['category_id'] ?>"                                                 <?= in_array($first['category_id'], $cateIds) ? 'selected' : '' ?>>
                                                <?= $first['name'] ?></option>
                                            <?php if (isset($first['child'])): foreach ($first['child'] as $two): $_category[$two['category_id']] = $two;?>
                                                <option value="<?= $two['category_id'] ?>"  <?= in_array($two['category_id'], $cateIds) ? 'selected' : '' ?>>
                                                    　　<?= $two['name'] ?></option>
                                                <?php if (isset($two['child'])): foreach ($two['child'] as $three): $_category[$three['category_id']] = $three;?>
                                                    <option value="<?= $three['category_id'] ?>" <?= in_array($two['category_id'], $cateIds) ? 'selected' : '' ?>>
                                                        　　　<?= $three['name'] ?></option>
                                                <?php endforeach; endif; ?>
                                            <?php endforeach; endif; ?>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="am-form-group  <?= $model['ad_type'] == 1 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">职业 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="position[]" multiple required disabled
                                            data-am-selected="{searchBox: 1, btnSize: 'sm',
                                             placeholder:'请选择职业', maxHeight: 400}">
                                        <option value=""></option>
                                        <?php if (isset($position)): foreach ($position as $first): ?>
                                            <option value="<?= $first['position_id'] ?>" <?= in_array($first['position_id'], $positionIds) ? 'selected' : '' ?>><?= $first['name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="am-form-group" data-x-switch>
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">播放类型 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="play_type" value="1" <?= $model['ad_type'] == 1 ? 'checked' : '' ?>
                                               data-am-ucheck
                                               disabled
                                               data-switch-box="switch-play_type"
                                               data-switch-item="play_type__1">
                                        图片
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="play_type" value="2" <?= $model['ad_type'] == 2 ? 'checked' : '' ?>disabled
                                               data-am-ucheck
                                               data-switch-box="switch-play_type"
                                               data-switch-item="play_type__2">
                                        视频
                                    </label>
                                </div>
                            </div>

                            <div class="am-form-group switch-ad_type ad_type__1 <?= $model['ad_type'] == 1 ? '' : 'hide' ?>">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">红包 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" disabled="disabled" min="1" class="tpl-form-input" name = "money" value="<?= $model['money']?>" placeholder="请输入红包金额" required>
                                </div>
                            </div>
                            <div class="am-form-group" data-x-switch>
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">广告类型 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ad_type" disabled="disabled" value="1" <?= $model['ad_type'] == 1 ? 'checked' : '' ?>
                                               data-am-ucheck
                                               data-switch-box="switch-ad_type"
                                               data-switch-item="ad_type__1">
                                        商业广告
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="ad_type" disabled="disabled" value="2"
                                            <?= $model['ad_type'] == 2 ? 'checked' : '' ?>
                                               data-am-ucheck
                                               data-switch-box="switch-ad_type"
                                               data-switch-item="ad_type__2">
                                        公益广告
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group" id="address-select">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 投放位置 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div id="coordinate">
                                        <div class="item-content" v-for="(item,index) in address">
                                            <input type="text" style="background: none !important; width: 30%;display: inline-block"
                                                   class="tpl-form-input"
                                                   placeholder="请选择坐标"
                                                   required v-model='item.point'>

                                            <div class="am-g am-form-field" style="width: 60%;display: inline-block">
                                                <div class="am-u-sm">地址：{{item.address}}</div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="am-form-group switch-ad_type ad_type__1">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">需要曝光量 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" disabled="disabled" min="1" class="tpl-form-input" name="click"
                                           value="<?= $model['click']?>" placeholder="请输入红包金额" required>
                                </div>
                            </div>

                            <div class="am-form-group switch-ad_type ad_type__1">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">平台曝光量 </label>
                                <div class="am-u-sm-9 am-u-end"><input type="number" disabled="disabled" min="1" class="tpl-form-input" name="click"
                                                                       value="<?= $model['user_click']?>" placeholder="请输入红包金额" required>
                                </div>
                            </div>

                            <div class="am-form-group switch-ad_type ad_type__1">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">转发曝光量 </label>
                                <div class="am-u-sm-9 am-u-end"><input type="number" disabled="disabled" min="1" class="tpl-form-input" name="click"
                                                                       value="<?= $model['share_click']?>" placeholder="请输入红包金额" required>
                                </div>
                            </div>

                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">人群性别: </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="sex"  disabled="disabled"  value="0" <?= $model['sex'] == 0 ? 'checked' : '' ?> data-am-ucheck>
                                        全部
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="sex" disabled="disabled"   value="1" <?= $model['sex'] == 1 ? 'checked' : '' ?> data-am-ucheck>
                                        男
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="sex"  disabled="disabled"  value="2" <?= $model['sex'] == 2 ? 'checked' : '' ?>data-am-ucheck>
                                        女
                                    </label>
                                </div>
                            </div>
                            
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">客户名称：</label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text"  disabled="disabled"  class="tpl-form-input" name="user_name"
                                           value="<?= $model['user_name'] ?>" placeholder="请输入客户名称" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">联系电话：</label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" disabled="disabled"   class="tpl-form-input" name="phone"
                                           value="<?= $model['phone'] ?>" placeholder="请输入联系电话" required>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}
<script src="assets/common/js/vue.min.js"></script>
<script src="assets/common/js/ddsort.js"></script>
<script src="assets/store/js/app.js"></script>
<script src="assets/store/js/file.library.js"></script>
<script src="assets/layui/layui.js"></script>
<script>
    var addressData = {address:<?=$model['address'];?>,show:false,active:0};

    /**
     * 位置选择
     */
    var address = new Vue({
        el: '#address-select',
        data:addressData,
        methods:{
            changeIndex(index){
                address._data.active = index;
            }
        }
    });
</script>
<script>
    /**
     * 时间选择
     */
    $(function () {
        var nowTemp = new Date();
        var nowDay = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0).valueOf();
        var nowMoth = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), 1, 0, 0, 0, 0).valueOf();
        var nowYear = new Date(nowTemp.getFullYear(), 0, 1, 0, 0, 0, 0).valueOf();
        var $startTime = $('.j-startTime');
        var $endTime = $('.j-endTime');

        var checkin = $startTime.datepicker({
            onRender: function (date, viewMode) {
                // 默认 days 视图，与当前日期比较
                var viewDate = nowDay;
                switch (viewMode) {
                    // moths 视图，与当前月份比较
                    case 1:
                        viewDate = nowMoth;
                        break;
                    // years 视图，与当前年份比较
                    case 2:
                        viewDate = nowYear;
                        break;
                }
                return date.valueOf() < viewDate ? 'am-disabled' : '';
            }
        }).on('changeDate.datepicker.amui', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.close();
            $endTime[0].focus();
        }).data('amui.datepicker');

        var checkout = $endTime.datepicker({
            onRender: function (date, viewMode) {
                var inTime = checkin.date;
                var inDay = inTime.valueOf();
                var inMoth = new Date(inTime.getFullYear(), inTime.getMonth(), 1, 0, 0, 0, 0).valueOf();
                var inYear = new Date(inTime.getFullYear(), 0, 1, 0, 0, 0, 0).valueOf();
                // 默认 days 视图，与当前日期比较
                var viewDate = inDay;
                switch (viewMode) {
                    // moths 视图，与当前月份比较
                    case 1:
                        viewDate = inMoth;
                        break;
                    // years 视图，与当前年份比较
                    case 2:
                        viewDate = inYear;
                        break;
                }
                return date.valueOf() <= viewDate ? 'am-disabled' : '';
            }
        }).on('changeDate.datepicker.amui', function (ev) {
            checkout.close();
        }).data('amui.datepicker');
    });
</script>

<script>
    $(function () {

        // swith切换
        var $mySwitch = $('[data-x-switch]');
        $mySwitch.find('[data-switch-item]').click(function () {
            var $mySwitchBox = $('.' + $(this).data('switch-box'));
            $mySwitchBox.hide().filter('.' + $(this).data('switch-item')).show();
        });
        // 选择图片
        $('.upload-file').selectImages({
            name: 'images[]'
            , multiple: true
        });

        // 图片列表拖动
        $('.uploader-list').DDSort({
            target: '.file-item',
            delay: 100, // 延时处理，默认为 50 ms，防止手抖点击 A 链接无效
            floatStyle: {
                'border': '1px solid #ccc',
                'background-color': '#fff'
            }
        });


    });
</script>
