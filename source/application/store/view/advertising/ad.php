<style>
    .item-group{
        margin: 0 2rem;
        padding-top: .8rem;
        font-size: 1.34rem;
        color: #656565;
        font-weight: inherit;
        text-align: left;
    }
    .item-group input{
        display: inline !important;
        width: 80px !important;
    }
</style>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">付费投放</div>
                            </div>
                            
                            <div class="am-form-inline item-group ">
                                1.最多可以选择投放<input type="number" class="am-form-field "  name="ad[address_num]"
                                           value="<?= $values['address_num'] ?? ''?>" >个区域（例如a用户要投放付费广告 他可以最多选择3个区域投放）
                                
                            </div>

                            <div class="am-form-inline item-group ">
                                2.区域的范围是根据用户在选择经纬度点为中心向外辐射<input type="number" class="am-form-field "  name="ad[distance]" value="<?= $values['distance'] ?? ''?>" >公里。

                            </div>

                            <div class="am-form-inline item-group ">
                                3.设置投放一个区域每周要<input type="number" class="am-form-field "  name="ad[cost]"  value="<?= $values['cost'] ?? ''?>" >元。

                            </div>

                            <div class="am-form-inline item-group ">
                                4.转发后获得红包<input type="number" class="am-form-field "  name="ad[rate]"  value="<?= $values['rate'] ?? ''?>" >%概率,红包金额最多<input type="number" class="am-form-field "  name="ad[max_money]"  value="<?= $values['max_money'] ?? ''?>" >元,用户付费广告，红包充值最低<input type="number" class="am-form-field "  name="ad[pay]" value="<?= $values['pay'] ?? ''?>" >元，红包被领完后，就没有红包了。

                            </div>

                            <div class="am-form-inline item-group ">
                                5.系统自动抢红包功能,每当用户在这边发布付费成功系统就自动抢到<input type="number" class="am-form-field "  name="ad[money_rate]"  value="<?= $values['money_rate'] ?? ''?>" >%的红包金额，（例如用户发布了100元的转发红包，系统自动抢到30% 其他70%的红包 按照系统设定转发获得红包中奖概率来执行。

                            </div>

                            <div class="am-form-inline item-group ">
                                6.同个微信在<input type="number" class="am-form-field "  name="ad[hour]"  value="<?= $values['hour'] ?? ''?>" >小时内点击只算一次曝光，只有被分享者点击进来，分享者才能获得红包。在<input type="number" class="am-form-field "  name="ad[commission_hour]" style="display: inline;width:50px;" value="<?= $values['commission_hour'] ?? ''?>" >小时内转给同个微信用户只能获得一次佣金（红包）的机会

                            </div>
                            

                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();

    });
</script>
