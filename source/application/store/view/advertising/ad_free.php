<style>
    .item-group{
        margin: 0 2rem;
        padding-top: .8rem;
        font-size: 1.34rem;
        color: #656565;
        font-weight: inherit;
        text-align: left;
    }
    .item-group input{
        display: inline !important;
        width: 80px !important;
    }
</style>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">免费投放</div>
                            </div>

                            <div class="am-form-inline item-group ">
                                1.可以免费投放距离发布者<input type="number" class="am-form-field "  name="ad_free[distance]" value="<?= $values['distance'] ?? ''?>" >公里范围传播。例如A发布了广告了 距离它5公里范围内的用户可以看到这个广告

                            </div>
                            <div class="am-form-inline item-group ">
                                2.同一个微信在<input type="number" class="am-form-field "  name="ad_free[hour]" value="<?= $values['hour'] ?? ''?>" >小时内点击只算一次曝光。

                            </div>

                            <div class="am-form-inline item-group ">
                                3.转发该公益广告，分享广告者可以获得<input type="number" class="am-form-field "  name="ad_free[share_click]" value="<?= $values['share_click'] ?? ''?>" >个广告曝光量，被分享者进入平台才能获得，被分享者也能获得<input type="number" class="am-form-field "  name="ad_free[user_click]"  value="<?= $values['user_click'] ?? ''?>" >个曝光量。转发同个微信用户<input type="number" class="am-form-field "  name="ad_free[click_hour]"  value="<?= $values['click_hour'] ?? ''?>" >小时内只能获得一次曝光量

                            </div>

                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        /**
         * 表单验证提交
         * @type {*}
         */
        $('#my-form').superForm();

    });
</script>
