<?php
/**
 * 后台菜单配置
 *    'home' => [
 *       'name' => '首页',                // 菜单名称
 *       'icon' => 'icon-home',          // 图标 (class)
 *       'index' => 'index/index',         // 链接
 *     ],
 */
return [
    'index' => [
        'name' => '首页',
        'icon' => 'icon-home',
        'index' => 'index/index',
    ],
    'store' => [
        'name' => '管理员',
        'icon' => 'icon-guanliyuan',
        'index' => 'store.user/index',
        'submenu' => [
            [
                'name' => '管理员列表',
                'index' => 'store.user/index',
                'uris' => [
                    'store.user/index',
                    'store.user/add',
                    'store.user/edit',
                    'store.user/delete',
                ],
            ],
            [
                'name' => '角色管理',
                'index' => 'store.role/index',
                'uris' => [
                    'store.role/index',
                    'store.role/add',
                    'store.role/edit',
                    'store.role/delete',
                ],
            ],
        ]
    ],
    'category' => [
        'name' => '广告行业',
        'icon' => 'icon-goods',
        'index' => 'category/index',
        'submenu' => [
            [
                'name' => '分类',
                'index' => 'category/index',
                'uris' => [
                    'category/index',
                    'category/add',
                    'category/edit',
                ],
            ],

        ],
    ],

    'position' => [
        'name' => '职位管理',
        'icon' => 'icon-goods',
        'index' => 'position/index',
        'submenu' => [
            [
                'name' => '职位',
                'index' => 'position/index',
                'uris' => [
                    'position/index',
                    'position/add',
                    'position/edit',
                ],
            ],

        ],
    ],

    'user' => [
        'name' => '用户管理',
        'icon' => 'icon-user',
        'index' => 'user/index',
        'submenu' => [
            [
                'name' => '用户列表',
                'index' => 'user/index',
            ],
//            [
//                'name' => '余额记录',
//                'active' => true,
//                'submenu' => [
//                    [
//                        'name' => '充值记录',
//                        'index' => 'user.recharge/order',
//                    ],
//                    [
//                        'name' => '余额明细',
//                        'index' => 'user.balance/log',
//                    ],
//                ]
//            ],
        ]
    ],

    'advertising' => [
        'name' => '广告管理',
        'icon' => 'icon-user',
        'index' => 'advertising/index',
        'submenu' => [
            [
                'name' => '付费广告设置',
                'index' => 'advertising/setting',
            ],
            [
                'name' => '公益广告设置',
                'index' => 'advertising/free_setting',
            ],
            [
                'name' => '广告列表',
                'index' => 'advertising/index',
            ],

            [
                'name' => '投放行业统计',
                'index' => 'advertising/lists',
            ],

        ]
    ],
    
    

    'finance' => [
        'name' => '财务统计',
        'icon' => 'icon-user',
        'index' => 'finance.withdraw/index',
        'submenu' => [
            [
                'name' => '提现设置',
                'index' => 'finance.withdraw/setting',
            ],
            [
                'name' => '提现列表',
                'index' => 'finance.withdraw/index',
            ],
            [
                'name' => '佣金列表',
                'index' => 'finance.commission/index',
            ],

            [
                'name' => '财务统计列表',
                'index' => 'finance.advertising/index',
            ],
    
        ]
    ],
    
    'setting' => [
        'name' => '设置',
        'icon' => 'icon-setting',
        'index' => 'setting/store',
        'submenu' => [
            [
                'name' => '系统设置',
                'index' => 'setting/store',
            ],

            [
                'name' => '活动设置',
                'index' => 'setting/activity',
            ],
            [
                'name' => '微信公众号设置',
                'index' => 'setting.wxapp/setting',
            ],
            [
                'name' => '用户管理',
                'index' => 'setting/user',
            ],
            [
                'name' => '其他',
                'submenu' => [
                    [
                        'name' => '清理缓存',
                        'index' => 'setting.cache/clear'
                    ]
                ]
            ]
        ],
    ],
];
