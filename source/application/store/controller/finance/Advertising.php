<?php
/**
 * Desc : WidthDraw.php
 * User : kjw
 * Date : 2021/1/26 9:01
 * File : WidthDraw.php
 */

namespace app\store\controller\finance;

use app\store\controller\Controller;
use app\store\model\Advertising as AdvertisingModel;

class Advertising extends Controller
{
    
    /**
     * @desc index
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $model = new AdvertisingModel;
        
        $data = array_merge(['status' => -1,'search' => null,'user_id'=> null,'pay_status' => 20,'pay_type' => 10,'origin' => 1],$this->request->get());
        
        return $this->fetch('index', [
            'list' => $model->getList($data)
        ]);
    }
    
}