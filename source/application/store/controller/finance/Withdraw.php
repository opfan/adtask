<?php
/**
 * Desc : WidthDraw.php
 * User : kjw
 * Date : 2021/1/26 9:01
 * File : WidthDraw.php
 */

namespace app\store\controller\finance;

use app\store\controller\Controller;
use app\store\model\finance\Withdraw as WithdrawModel;

class Withdraw extends Controller
{
    public function setting()
    {
        return $this->updateEvent('withdraw');
    }
    
    /**提现记录列表
     * @desc index
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $model = new WithdrawModel;
        $data = array_merge(['status' => -1,'search' => null,'user_id'=> null],$this->request->get());
        $count = [
            'pedding'=> $model->getCount(10,'apply_money'),
            'used'=> $model->getCount(20),
        ];
        return $this->fetch('index', [
            'list' => $model->getList($data),
            'count'=> $count
        ]);
    }
    
    /** 结算
     * @desc  money
     * @return array|bool
     * @throws \think\exception\DbException
     */
    public function money()
    {
        
        $data = array_merge(['money' => 0,'id'=> null],$this->postData());
        
        $model = WithdrawModel::detail($data['id']);
        
        if ($model->money($data)) {
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError($model->getError() ?: '操作失败');
    }
}