<?php
/**
 * Desc : WidthDraw.php
 * User : kjw
 * Date : 2021/1/26 9:01
 * File : WidthDraw.php
 */

namespace app\store\controller\finance;

use app\store\controller\Controller;

class Commission extends Controller
{
    /**提现记录列表
     * @desc index
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $model = new \app\store\model\finance\Commission();
        $data = array_merge(['search' => null,'user_id'=> null],$this->request->get());
        $count = $model->getCount();
        return $this->fetch('index', [
            'list' => $model->getList($data),
            'count' => $count
        ]);
    }
}