<?php

namespace app\store\controller;

use app\common\model\AdvertisingLog;
use app\store\model\Advertising as AdvertisingModel;
use app\store\model\AdvertisingCategory;
use app\store\model\AdvertisingPosition;
use app\store\model\Category as CategoryModel;
use app\store\model\store\UserRole;
use Lvht\GeoHash;

/**
 * Class Advertising
 * @package app\store\controller
 */
class Advertising extends Controller
{
    /**
     * 付费设置
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function setting()
    {
        return $this->updateEvent('ad');
    }

    /**
     * 付费设置
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function free_setting()
    {
        return $this->updateEvent('ad_free');
    }

    /**
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {

        // 获取全部列表
        $model = new AdvertisingModel;
        $list  = $model->getList(array_merge(['status' => -1], $this->request->param()));
        // 商品分类
        $catgory = CategoryModel::getCacheTree();
//print_r($list->toArray());exit;
        return $this->fetch('index', compact('list', 'catgory'));
    }
    
    /**投放行业统计
     * @desc lists
     * @return mixed
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function lists()
    {
        $model = new AdvertisingModel;
        //统计本日、本周、本月投放的广告
        
        //本日
        $day_start = mktime(0,0,0,date('m'),date('d'),date('Y'));
    
        $day_end = mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
    
        $day_time =  $model->getCount(['time'=> [$day_start,$day_end]]);
        
        //本周
        $sdefaultDate = date("Y-m-d");
// $first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        $first=1;
// 获取当前周的第几天 周日是0 周一到周六是 1 - 6
        $w = date('w', strtotime($sdefaultDate));
        // 获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $week_start=strtotime("$sdefaultDate -".($w ? $w - $first : 6).' days');
// 本周结束日期
        $week_end=$week_start + 7*86400-1;

        $week_time =  $model->getCount(['time'=> [$week_start,$week_end]]);
        //本月
        $mon_start = mktime(0, 0 , 0,date("m"),1,date("Y"));
        
        $mon_end = mktime(23,59,59,date("m"),date("t"),date("Y"));
    
        $month_time =  $model->getCount(['time'=> [$mon_start,$mon_end]]);
        
        $data = array_merge(['status' => 20,'state'=> 10,'search' => null,'user_id'=> null],$this->request->get());
        
        $list =  $model->getList($data);
        
        return $this->fetch('list', compact('list','day_time','week_time','month_time'));
    }

    /**
     * @param $ad_id
     * @return array|bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function audit($ad_id)
    {

        $model = AdvertisingModel::detail($ad_id);
        if ($model->audit($this->postData())) {
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError($model->getError() ?: '操作失败');
    }


    /**
     * @param $ad_id
     * @return array|bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function delete($ad_id)
    {
        // 商品详情
        $model = \app\store\model\Advertising::detail($ad_id);
        if (!$model->setDelete()) {
            return $this->renderError($model->getError() ?: '删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * @return array|bool|mixed
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            // 商品分类
            $category = CategoryModel::getCacheTree();
            $position = \app\store\model\Position::getAll();
            $config = \app\common\model\Setting::getItem('ad');
            return $this->fetch(
                'add',
                compact('category','position','config')
            );
        }
        $model = new AdvertisingModel();
        $data  = $this->postData();

        $data['origin'] = 2;

        if ($model->add($data)) {
            return $this->renderSuccess('添加成功', url('advertising/index'));
        }
        return $this->renderError($model->getError() ?: '添加失败');
    }

    /**
     * @param $ad_id
     * @return array|bool|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit($ad_id)
    {
        // 商品详情
        $model = AdvertisingModel::detail($ad_id);

        if (!$this->request->isAjax()) {
            $category = CategoryModel::getCacheTree();
            $cateIds = AdvertisingCategory::getCategoryIds($ad_id);
            $position = \app\store\model\Position::getAll();
            $positionIds = AdvertisingPosition::getPositionIds($ad_id);
            $config = \app\common\model\Setting::getItem('ad');
            
            return $this->fetch(
                'edit',
                compact('model','category','cateIds','position','positionIds','config')
            );
        }

        // 更新记录
        if ($model->edit($this->postData())) {
            return $this->renderSuccess('更新成功', url('advertising/index'));
        }
        return $this->renderError($model->getError() ?: '更新失败');
    }


    /**
     * @param $ad_id
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function view($ad_id)
    {
        // 商品详情
        $model = AdvertisingModel::detail($ad_id);
        if (!$this->request->isAjax()) {
            $category = CategoryModel::getCacheTree();
            $cateIds = AdvertisingCategory::getCategoryIds($ad_id);
            $position = \app\store\model\Position::getAll();
            $positionIds = AdvertisingPosition::getPositionIds($ad_id);
            return $this->fetch(
                'view',
                compact('model','category','cateIds','position','positionIds')
            );
        }

    }
    
    
    /**
     * @param $ad_id
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function info($ad_id)
    {
        // 商品详情
        $model = AdvertisingModel::detail($ad_id);
        if (!$this->request->isAjax()) {
            $category = CategoryModel::getCacheTree();
            $cateIds = AdvertisingCategory::getCategoryIds($ad_id);
            $position = \app\store\model\Position::getAll();
            $positionIds = AdvertisingPosition::getPositionIds($ad_id);
            return $this->fetch(
                'info',
                compact('model','category','cateIds','position','positionIds')
            );
        }
        
    }
    
    /**
     * @desc getponit
     * @return mixed
     */
    public function get_point()
    {
        $this->view->engine->layout(false);
        return $this->fetch('getpoint');
    }

    /**
     * @param $ad_id
     * @param $state
     * @return array|bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function state($ad_id, $state)
    {
        // 商品详情
        $model = AdvertisingModel::detail($ad_id);
        if (!$model->setStatus($state)) {
            return $this->renderError('操作失败');
        }
        return $this->renderSuccess('操作成功');
    }
    
    /**
     * @desc  ad_log
     * @param $ad_id
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function ad_log($ad_id)
    {
       
        $list = (new AdvertisingLog())->getList($ad_id);
       
        return $this->fetch(
            'ad_log',
            compact('list')
        );
    }
}
