<?php

namespace app\store\controller;

use app\store\model\Position as PositionModel;

/**
 * Class Position
 * @package app\store\controller
 */
class Position extends Controller
{
    /**
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $model = new PositionModel;
        $list = $model->getList([]);
        return $this->fetch('index', compact('list'));
    }

    public function delete($position_id)
    {
        $model = PositionModel::get($position_id);
        if (!$model->remove()) {
            return $this->renderError($model->getError() ?: '删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * @return array|bool|mixed
     */
    public function add()
    {
        $model = new PositionModel();
        if (!$this->request->isAjax()) {

            return $this->fetch('add');
        }

        $postData = $this->postData('position');

        // 新增记录
        if ($model->add($postData)) {
            return $this->renderSuccess('添加成功', url('position/index'));
        }

        return $this->renderError($model->getError() ?: '添加失败');
    }

    /**
     * @param $position_id
     * @return array|bool|mixed
     * @throws \think\exception\DbException
     */
    public function edit($position_id)
    {
        // 模板详情
        $model = PositionModel::get($position_id);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }

        $postData = $this->postData('position');

        // 更新记录
        if ($model->edit($postData)) {
            return $this->renderSuccess('更新成功', url('position/index'));
        }
        return $this->renderError($model->getError() ?: '更新失败');
    }

}
