<?php

namespace app\store\controller;

use app\store\model\Category as CategoryModel;
use app\store\model\Printer as PrinterModel;
use app\store\model\Setting as SettingModel;
use app\common\library\sms\Driver as SmsDriver;

/**
 * 系统设置
 * Class Setting
 * @package app\store\controller
 */
class Setting extends Controller
{
    /**
     * 设置
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function store()
    {
        return $this->updateEvent('store');
    }
    
    /**
     * 微信配置
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function wx()
    {
        return $this->updateEvent('wx');
    }

    /**
     * 微信配置
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function activity()
    {
        return $this->updateEvent('activity');
    }


    public function user()
    {
        if (!$this->request->isAjax()) {
            // 商品分类
            $catgory = CategoryModel::getCacheTree();
            // 商品分类
            $position = \app\store\model\Position::getAll();
            $limit = 15;
            return $this->fetch('user',compact('limit','position','catgory'));
        }

        $data = $this->postData('user');

        $model = new \app\store\model\User();

        foreach ($data as &$item){

            if(empty($item['nickName'])){
                return $this->renderError('昵称不能为空');
            }

            $item['avatarUrl'] = $model->makeAvatar();

            //获取省份信息
            $all = \app\common\model\Region::getCacheTree();
            $province = $all[$item['province']]['name']?? '';
            //获取城市
            $city = $all[$item['province']]['city'][$item['city']]['name']??'';

            $region = $all[$item['province']]['city'][$item['city']]['region'][$item['region']]['name']?? '';

            $item['province'] = $province;
            $item['city'] = $city;
            $item['region'] = $region;
            $item['country'] = '中国';
        }


        $result = $model->allowField(true)->saveAll($data);

        if($result){
            return $this->renderSuccess('添加成功');
        }
        return $this->renderError('添加失败');

    }
    

}
