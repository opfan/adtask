<?php

namespace app\api\controller;

use app\api\model\Advertising;
use app\api\model\Category;
use app\api\model\Position;
use app\api\model\User as UserModel;
use app\api\model\Withdraw as WithDrawModel;
use app\common\exception\BaseException;
use app\common\model\Setting;
use think\Exception;
use think\exception\DbException;
use function request;

/**
 * 用户管理
 * Class User
 * @package app\api
 */
class User extends Controller
{
    
    public function index()
    {
        
        config('default_return_type', 'html');
        
        $user_id = $this->request->get('user_id', $this->user['user_id']);
        
        //表示是否当前登录用户
        $login_user = ($user_id == $this->user['user_id']) ? 1 : 0;
        
        $user = UserModel::get($user_id);
        
        if ($user && $user['category_id']) {
            $category         = Category::get($user['category_id']);
            $user['category'] = $category['name'] ?? '';
        }
        !$user && $user = '{}';
        //提现申请
        $apply_money = WithDrawModel::getUserDetail(['user_id' => $this->user['user_id'], 'status' => 10], 'apply_money');
        //已提现
        $withdraw_money = WithDrawModel::getUserDetail(['user_id' => $this->user['user_id'], 'status' => 20]);
        //已发红包
        $send_money = (new Advertising())->where(['user_id' => $this->user['user_id'], 'origin' => 1, 'pay_status' => 20])->sum('money');
        //分享的下线成员
        $child_num = UserModel::childCount($this->user['user_id']);
        
        return $this->fetch('', compact('user', 'apply_money', 'withdraw_money', 'send_money', 'login_user', 'child_num'));
        
    }
    
    /**用户最新发布
     * @return array
     * @throws DbException
     */
    public function list()
    {
        
        $user_id = $this->request->get('user_id', $this->user['user_id']);
        
        $model = new Advertising();
        
        $filter = [
            'user_id'    => $user_id,
            'pay_status' => 20,
            'state'      => 10,
            'status'     => 20,
            'is_delete'  => 0,
        ];
        
        $param = array_merge([
            'listRows' => 4,       // 每页数量
        ], $this->request->get());
        
        // 执行查询
        $list = $model
            ->field('ad_id,ad_name,thumb_id,origin,play_type,ad_type,user_id,user_click,share_click')
            ->with([
                'url.file', 'file', 'address' => function ($query) {
                    
                    $query->field('id,ad_id,address');
                },
            ])
            ->where($filter)
            ->order('ad_id desc')
            ->paginate($param['listRows'], false, [
                'query' => request()->request(),
            ]);
        
        return $this->renderSuccess($list);
    }
    
    /**
     * 当前用户详情
     * @return array
     * @throws BaseException
     * @throws DbException
     */
    public function detail()
    {
        
        // 当前用户信息
        $userInfo = $this->getUser();
        
        return $this->renderSuccess(compact('userInfo'));
    }
    
    /**
     * @desc update
     * @return array
     * @throws Exception
     * @throws DbException
     */
    public function update()
    {
        
        $data = array_merge([
            'longitude'   => null,
            'latitude'    => null,
            'user_id'     => $this->user['user_id'],
            'position_id' => 0,
            'category_id' => 0,
            'auth'        => 1,
        ], $this->request->param());
        
        $model = new UserModel();
        
        if ($model->updateUser($data) !== false) {
            return $this->renderSuccess();
        }
        
        return $this->renderError($model->getError() ?: '更新失败');
    }
    
    /**
     * @return array
     * @throws DbException
     * @throws Exception
     */
    public function update_address()
    {
        
        $data = array_merge([
            'longitude' => null,
            'latitude'  => null,
            'user_id'   => $this->user['user_id'],
        ], $this->request->param());
        
        $model = new UserModel();
        
        if ($model->updateAddress($data) !== false) {
            return $this->renderSuccess();
        }
        
        return $this->renderError($model->getError() ?: '更新失败');
    }
    
    /**编辑完善资料
     * @return array
     * @throws DbException
     */
    public function change()
    {
        
        $data = array_merge([
            'user_id'     => $this->user['user_id'],
            'position_id' => 0,
            'category_id' => 0,
        ], $this->request->param());
        
        $user = UserModel::get($data['user_id']);
        
        if (!Category::get($data['category_id'])) {
            return $this->renderError('行业不存在');
        }
        if (!Position::get($data['position_id'])) {
            return $this->renderError('职位不存在');
        }
        //判断是否填写了职业和行业，赠送曝光量
        if ((!$user['category_id'] || !$user['position_id']) && !empty($data['category_id']) && !empty($data['position_id'])) {
            $config = Setting::getItem('activity');
            if ($config['is_open']) {
                $data['click'] = $user['click'] + $config['click'];
            }
        }
        
        if ($user->save($data) !== false) {
        
            return redirect($this->request->domain().'/index.php?s=api/user/index');
            
        }
        
        return $this->renderError('更新失败');
    }
    
    /**商业广告
     * @return mixed
     */
    public function business()
    {
        
        return $this->fetch('business');
    }
    
    /**已发红包
     * @return mixed
     */
    public function ad()
    {
        
        //来源 1已发红包 2商业广告投放
        $origin = $this->request->get('origin', 1);
        //已发红包
        $money = (new Advertising())->where(['user_id' => $this->user['user_id'], 'origin' => 1, 'pay_status' => 20])->sum('money');
        
        return $this->fetch('hb-send', compact('origin', 'money'));
    }
    
    /** 已发红包、商业广告列表
     * @return array
     * @throws DbException
     * @throws Exception
     */
    public function ad_list()
    {
        
        $data            = $this->request->get();
        $data['user_id'] = $this->user['user_id'];
        
        $list = (new Advertising())->user_list($data);
        
        return $this->renderSuccess($list);
    }
    
    /**
     * @return mixed
     * @throws Exception
     */
    public function child_index()
    {
        
        config('default_return_type', 'html');
        
        $child_num = UserModel::childCount($this->user['user_id']);
        
        return $this->fetch('child', compact('child_num'));
    }
    
    /**
     * @return array
     * @throws DbException
     */
    public function child_list()
    {
        
        $list = (new UserModel())->getChildList(['user_id' => $this->user['user_id']]);
        
        return $this->renderSuccess($list);
    }
    

    
}
