<?php


namespace app\api\controller;

use app\api\model\Advertising as Model;
class Task extends Controller
{
    
    /**
     * @desc index
     * @return mixed
     * @throws \think\Exception
     */
    public function index()
    {
        //总佣金
        $total = $this->getUser()['money'];
        
        return $this->fetch('',compact('total'));
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function list()
    {
        $data = $this->request->get();
        
        $data['user_id'] = $this->user['user_id'];
        
        $list = (new Model())->taskList($data);
        
        return $this->renderSuccess($list);
        
    }
}