<?php


namespace app\api\controller;

use app\api\model\Withdraw as Model;

class Withdraw extends Controller
{
    /**
     * @return mixed
     * @throws \app\common\exception\BaseException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $withdraw_money = Model::getUserDetail(['user_id'=> $this->user['user_id'],'status'=> 20]);
        $user = $this->getUser();
        $money = $user['money'];

        return $this->fetch('',compact('money','withdraw_money'));
    }

    /**
     * @return array
     * @throws \think\exception\DbException
     */
    public function list()
    {
        $data = $this->request->get();
        $data['user_id'] = $this->user['user_id'];

        $list = (new Model())->list($data);

        return $this->renderSuccess($list);

    }

    /**
     * @return array
     * @throws \think\exception\DbException
     */
    public function apply()
    {
        $data = array_merge(
            [
                'user_id'=> $this->user['user_id'],
                'file_id' => 0,
                'apply_money'=> 0,
            ],$this->postData()
        );
        $model = new Model();

        if($model->add($data)){
            return $this->renderSuccess();
        }

        return $this->renderError($model->getError()?:'申请失败');
    }
}