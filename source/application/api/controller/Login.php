<?php
/**
 * Desc : Login.php
 * User : kjw
 * Date : 2021/2/1 10:46
 * File : Login.php
 */

namespace app\api\controller;

use app\api\model\Wxapp;
use EasyWeChat\Factory;
use think\Cache;
use think\Log;

class Login extends \think\Controller
{
    
    /**
     * @desc oauthcallback
     * @throws \app\common\exception\BaseException
     * @throws \think\Exception
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function oauthcallback()
    {
        
        $config = Wxapp::getConfig();
       
        $data     = $this->request->get();
        
        $userInfo = Cache::get($data['code']);

        if (empty($userInfo)) {
            $app   = Factory::officialAccount($config);
            $oauth = $app->oauth;

            // 获取 OAuth 授权结果用户信息
            $user     = $oauth->user()->toArray();
            $userInfo = $user['original'];
            Cache::set($data['code'], $userInfo, 7200);
        }

        //源地址
        $targetUrl = session('target_url') ?: url('api/index/index');
        //解析源地址参数
        $url = parse_url($targetUrl);

        !empty($url['query']) && parse_str($url['query'],$data);

        //登录、注册
        $userModel = new \app\api\model\User();
        
        $result = $userModel->login(array_merge($data,$userInfo));
        
        if($result['is_new']){
            $targetUrl = $targetUrl.(!empty($url['query']) ? '&' : '?').'register=1';
        }
        
        $targetUrl = $result['is_new'] ?  (url('api/index/register').'&redirect='.urlencode($targetUrl)) :$targetUrl;

        header('location:'. $targetUrl); 
    }
    
}