<?php


namespace app\api\controller;

use app\api\model\Commission as Model;
class Commission extends Controller
{
    /**
     * @return mixed
     * @throws \think\Exception
     */
    public function index()
    {
        //总佣金
        $total = $this->getUser()['money'];

        return $this->fetch('',compact('total'));
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function list()
    {
        $data = $this->request->get();
        $data['user_id'] = $this->user['user_id'];
        $list = (new Model)->getList($data);

        return $this->renderSuccess($list);
    }
}