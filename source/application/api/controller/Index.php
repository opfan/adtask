<?php
/**
 * Desc : Index.php
 * User : kjw
 * Date : 2021/2/1 10:54
 * File : Index.php
 */

namespace app\api\controller;

use app\api\model\Advertising;
use app\api\model\Category;
use app\api\model\Position;
use app\api\model\User;
use app\api\model\Wxapp;
use app\common\model\AdvertisingLog;
use app\common\model\finance\Commission;
use app\common\model\Setting;
use app\common\service\Qrcode;
use app\common\service\WxTemplateMessage;
use EasyWeChat\Factory;
use think\Cache;

class Index extends Controller
{

    /**
     * @return mixed
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function index()
    {

        config('default_return_type', 'html');

        $data = $this->request->get();
        $data['user_id'] = $this->user['user_id'];
        //是否注册
        $register = $data['register'] ?? 0;
        //分享点击获取佣金
        $commission  = new \app\api\model\Commission();

        $commission->calculate($data);

        $user = $this->user;
        
        $money = Setting::getItem('activity')['login_money']??0;
        
        //缓存是否新用户
        if($register && !Cache::has('register_'.$this->user['user_id'])){
            Cache::set('register_'.$this->user['user_id'],1);
        }else{
            $register = 0;
        }
        return $this->fetch('index', compact('user','register','money'));
    }

    /**
     * @desc register
     * @return mixed
     */
    public function register()
    {
        $redirect = $this->request->get('redirect',config('host_url'));
//        $redirect = urlencode($redirect);

        return $this->fetch('register',compact('redirect'));
    }


    /**
     * @desc list
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function list()
    {

        $data = array_merge($this->request->get(), [
            'user_id' => $this->user['user_id'],
        ]);

        $model = new Advertising();

        $list = $model->getList($data);

        if($list->isEmpty()){
            $list = $model->getList(array_merge($data,['is_recommend'=> true]));
        }
        
        return $this->renderSuccess($list);
    }

    /**
     * @desc detail
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function detail()
    {
        $data = array_merge($this->request->get(), [
            'user_id' => $this->user['user_id'],
        ]);

        $model = new Advertising();

        $detail = $model::detail($data);

        return $this->renderSuccess($detail);
    }

    /**
     * @return array
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function add()
    {
        $data = array_merge($this->request->param(), [
            'user_id' => $this->user['user_id'],
        ]);

        $model = new Advertising();

        $money = $model->checkOut($data);
        if($money === false){
            return $this->renderError($model->getError() ?? '获取失败');
        }

        if($this->request->isGet()){

            return $this->renderSuccess(compact('money'));
        }

        if ($model->add(array_merge($data,['user'=>$this->user->toarray()]))) {
            if(($data['ad_type'] == 1) && ($data['pay_type']==10)){
                $wx = $model->wxPay($model,$this->user);
                if(!$wx){
                    return $this->renderError('微信支付失败');
                }
            }
            //唤起支付
            return $this->renderSuccess(['ad_no' => $model->ad_no, 'pay_info' => $wx ?? []]);
        }
        return $this->renderError($model->getError() ?? '添加失败');
    }


    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function nearby()
    {
        $user = $this->user;

        $model = new \app\api\model\User();

        if ($list = $model->getNearby($user)) {
            return $this->renderSuccess($list);
        }
        return $this->renderError('失败');
    }

    /**行业
     * @return array
     */
    public function category()
    {
        $list = Category::getCacheTree();

        return $this->renderSuccess(array_values($list));
    }

    /**职位
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function position()
    {
        $list = (new Position())->select();

        return $this->renderSuccess($list);
    }
    
    /** 获取题目
     * @desc question
     * @return array
     * @throws \think\exception\DbException
     */
    public function question()
    {
        
        $ad_id = $this->request->get('ad_id');
        
        $ad = Advertising::get($ad_id);
        
        if(!$ad || empty($ad['extend']['question'])){
            return $this->renderError('获取失败');
        }
     
        return $this->renderSuccess($ad['extend']['question']);
    }

    /**
     * @return array
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyWeChat\Kernel\Exceptions\RuntimeException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \think\exception\DbException
     */
    public function wx_config()
    {

        $apis = $this->request->get('api','onMenuShareAppMessage');

        $url = $this->request->get('url','');

        $wxconfig = Wxapp::getConfig();

        $app = Factory::officialAccount($wxconfig);

        $referer = $this->request->header('Referer');

        //设置调用接口页面的url
        ($url || $referer) &&  $app->jssdk->setUrl($url?:$referer);

        $apis = explode(',',$apis);

        $data = $app->jssdk->buildConfig($apis, $debug = false, $beta = false, $json = true);

        return $this->renderSuccess($data);
    }
    
    /**
     * @desc add_config
     * @return array
     * @throws \think\exception\DbException
     */
    public function add_config()
    {
        $config = Setting::getItem('ad');
    
        $user = User::detail($this->user['user_id']);
        //可用余额
        $userBalance = $user['useBalance'];
        
        return $this->renderSuccess(array_merge($config,['user_click'=>$this->user['click'],'balance'=> $userBalance]));
    }

    /**
     * @return array
     */
    public function pay_money()
    {
        $data = $this->request->get();

        $model = new Advertising();

        $money = $model->getPayMoney($data);

        return $this->renderSuccess($money);
    }

    /**
     * @return array
     * @throws \think\exception\DbException
     */
    public function ad_log()
    {
        $ad_id = $this->request->get('ad_id');

        $list = (new AdvertisingLog())->getList($ad_id);

        return $this->renderSuccess($list);
    }


    public function test()
    {

//        $model = new \app\api\model\Commission();
//        $ad = Advertising::get(30);
//        $data = [
//            'user_id'=> $this->user['user_id'],
//            'referee_id' => 734,
//            'ad_id' => 30
//        ];
//
//        $result = (new Commission())->autoCommission();
//
//        print_r($result);
    }

}
