<?php

namespace app\api\controller;

use app\api\model\Advertising;
use app\api\model\Wxapp;
use EasyWeChat\Factory;
use think\Log;

/**
 * 支付成功异步通知接口
 * Class Notify
 * @package app\api\controller
 */
class Notify
{
    /** 支付成功异步通知
     * @throws \EasyWeChat\Kernel\Exceptions\Exception
     * @throws \think\exception\DbException
     */
    public function notify()
    {
        
        $config = Wxapp::wxPay();

        $app = Factory::payment($config);
      
        $response = $app->handlePaidNotify(function ($message, $fail) {
            Log::write('接收到微信支付'.json_encode($message,JSON_UNESCAPED_UNICODE));
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $ad = Advertising::get(['ad_no'=>$message['out_trade_no']]);

            if (!$ad || $ad->pay_status == 20) { // 如果订单不存在 或者 订单已经支付过了
                return true; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }

            //建议在这里调用微信的【订单查询】接口查一下该笔订单的情况，确认是已经支付

            if ($message['return_code'] === 'SUCCESS') { // return_code 表示通信状态，不代表支付状态
                // 用户是否支付成功
                if (array_get($message, 'result_code') === 'SUCCESS') {
                    // 保存订单
                    $ad->payNotify($ad,$message);
                    
                } elseif (array_get($message, 'result_code') === 'FAIL') {
                    // 用户支付失败

                }
            } else {
                return $fail('通信失败，请稍后再通知我');
            }
            
            return true; // 返回处理完成
        });

        $response->send();
    }
}
