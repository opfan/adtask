<?php

namespace app\api\controller;

use app\api\model\User as UserModel;
use app\api\model\Wxapp;
use app\common\exception\BaseException;
use app\common\model\Setting;
use EasyWeChat\Factory;
use think\Request;
use think\Session;

/**
 * API控制器基类
 * Class BaseController
 * @package app\store\controller
 */
class Controller extends \think\Controller
{
    const JSON_SUCCESS_STATUS = 1;
    const JSON_ERROR_STATUS = 0;
    
    protected $user;

    /**
     * @desc _initialize
     */
    public function _initialize()
    {
        parent::_initialize();
    
        //校验是否登录
        $this->checkLogin();
    
        // 全局layout
        $this->layout();

        //绑定推荐人
        $this->bindReferee();

        //校验是否填完注册信息
//        $this->checkUserInfo();
    }

    private function checkUserInfo()
    {
        $allowAuth = [
            'index/wx_config',
            'index/register',
            'index/position',
            'index/category',
            'login/oauthcallback',
            'user/update',
            'user/update_address',
        ];
        $action = $this->request->action();
        $controller = toUnderScore($this->request->controller());

        if(!in_array($controller.'/'.$action,$allowAuth) && (!$this->user['category_id'] || !$this->user['position_id'])){

            $url = config('host_url');
            $this->redirect($url.'index.php?s=api/index/register');
            exit;
        }
    }
    /**
     * @throws BaseException
     * @throws \think\exception\DbException
     */
    private function bindReferee()
    {
        $invite_id = $this->request->get('invite_id');

        if(empty($invite_id)){
            return ;
        }

        $user = $this->getUser();
        //已经绑定不能变更
        if($user['referee_id']){
            return ;
        }

        $invite = \app\api\model\User::get($invite_id);

        if(!$invite){
            return ;
        }

        $user->save(['referee_id'=> $invite_id,'referee_time'=>time()]);


    }
    /**
     * 全局layout模板输出
     * @throws \think\exception\DbException
     * @throws \Exception
     */
    private function layout()
    {
        // 验证当前请求是否在白名单
        
            // 输出到view
            $this->assign([
                'base_url' => base_url(),                      // 当前域名
                'setting' => Setting::getAll() ?: null,        // 当前商城设置
                'request' => Request::instance(),              // Request对象
                'version' => get_version(),                    // 系统版本号
                'user'    => $this->user,                       //用户信息
                'user_id' => $this->user['user_id'],            //当前用户id
            ]);
        
    }
    /**
     * @desc checkLogin
     * @throws \app\common\exception\BaseException
     * @throws \think\exception\DbException
     */
    private function checkLogin()
    {
        //未登录跳转登录
        if(!$user = $this->getUser(false)){
            $config =  Wxapp::getConfig();

            $app = Factory::officialAccount($config);
            $oauth = $app->oauth;
            
            $redirectUrl = $oauth->redirect()->send();

            session('target_url',$this->request->domain().$this->request->url());
            
            header("Location: {$redirectUrl}");
        }
        
        $this->user = $user;
        
    }
    
    /**
     * 获取当前用户信息
     * @param bool $is_force
     * @return UserModel|bool|null
     * @throws BaseException
     * @throws \think\exception\DbException
     */
    protected function getUser($is_force = true)
    {
      
        if (!$user = UserModel::getUser()) {
            $is_force && $this->throwError('没有找到用户信息', -1);
            return false;
        }
        return $user;
    }

    /**
     * 输出错误信息
     * @param int $code
     * @param $msg
     * @throws BaseException
     */
    protected function throwError($msg, $code = 0)
    {
        throw new BaseException(['code' => $code, 'msg' => $msg]);
    }

    /**
     * 返回封装后的 API 数据到客户端
     * @param int $code
     * @param string $msg
     * @param array $data
     * @return array
     */
    protected function renderJson($code = self::JSON_SUCCESS_STATUS, $msg = '', $data = [])
    {
        return json(compact('code', 'msg', 'data'));
    }

    /**
     * 返回操作成功json
     * @param array $data
     * @param string|array $msg
     * @return array
     */
    protected function renderSuccess($data = [], $msg = 'success')
    {
        return $this->renderJson(self::JSON_SUCCESS_STATUS, $msg, $data);
    }

    /**
     * 返回操作失败json
     * @param string $msg
     * @param array $data
     * @return array
     */
    protected function renderError($msg = 'error', $data = [])
    {
        return $this->renderJson(self::JSON_ERROR_STATUS, $msg, $data);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postData($key = null)
    {
        return $this->request->post(is_null($key) ? '' : $key . '/a');
    }

}
