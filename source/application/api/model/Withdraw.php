<?php


namespace app\api\model;


use app\common\model\Setting;

class Withdraw extends \app\common\model\finance\Withdraw
{
    /**
     * @param $param
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function list($param)
    {
        $filter['user_id'] = $param['user_id'];

        if(!empty($param['stime'])){
            $filter['create_time'] = ['>',$param['stime']];
        }

        if(!empty($param['etime'])){
            $filter['create_time'] = ['<',$param['etime']];
        }

        $list = $this
            ->where($filter)
            ->order('id desc')
            ->paginate($param['listRows']?? 10, false, [
                'query' => request()->request()
            ]);

        return $list;
    }

    /**
     * @param $data
     * @return bool|false|int
     * @throws \think\exception\DbException
     */
    public function add($data)
    {
        if(empty((int)$data['apply_money']) || !is_numeric($data['apply_money'])){
            $this->error = '请输入金额';
            return false;
        }

        if($data['apply_money'] < 0){
            $this->error = '金额错误';
            return false;
        }
        $config = Setting::getItem('withdraw');
        //满多少才能提现
        if($config['money'] > $data['apply_money']){
            $this->error = '满'.$config['money'].'才能提现';
            return false;
        }
    
        $user = User::get($data['user_id']);
    
        if($user['money'] < $data['apply_money']){
            $this->error = '可提现金额不足';
            return false;
        }
        
        
        $withdraw = $this->where(['user_id'=> $data['user_id']])->order('id DESC')->find();
        //满几天才能提现
        if($withdraw){
            if((strtotime($withdraw['create_time']) + $config['days']*86400) > time()){
                $this->error = '满'.$config['days'].'天才能提现一次1';
                return false;
            }
        }else{
            $commission = (new Commission())->where(['user_id'=> $data['user_id']])->order('id ASC')->find();
           
            if(strtotime($commission['create_time']) + $config['days']*86400 > time()){
                $this->error = '满'.$config['days'].'天才能提现一次';
                return false;
            }
        }
        
        
     

        if(empty($data['file_id'])){
            $this->error = '请上传二维码';
            return false;
        }

        $insert = [
            'apply_money' => $data['apply_money'],
            'image_id' => $data['file_id'],
            'status' => 10,
            'user_id' => $data['user_id']
        ];

        return $this->save($insert);
    }
}