<?php

namespace app\api\model;

use app\common\model\AdvertisingAddress as AdvertisingAddressModel;

/**
 * Class AdvertisingAddress
 * @package app\api\model
 */
class AdvertisingAddress extends AdvertisingAddressModel
{
    protected $append = [
        'province','city','region'
    ];

}
