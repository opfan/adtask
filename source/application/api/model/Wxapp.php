<?php

namespace app\api\model;

use app\common\model\Wxapp as WxappModel;

/**
 * 微信小程序模型
 * Class Wxapp
 * @package app\api\model
 */
class Wxapp extends WxappModel
{
    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [
        'wxapp_id',
        'app_name',
        'app_id',
        'app_secret',
        'mchid',
        'apikey',
        'create_time',
        'update_time'
    ];

    /**
     * @return array|bool
     * @throws \think\exception\DbException
     */
    public static function getConfig()
    {
        $wx = self::detail();

        return [
            'app_id' => $wx['app_id'],
            'secret' => $wx['app_secret'],
            'token' => 'AmllgssdSDl2gg',
            'aes_key' => 'bU8u1ZfAlly1AYgg8EFOAEx8kf7OLMAGgAkLkUsMYXf',
            'response_type' => 'array',
            'oauth' => [
                'scopes' => ['snsapi_userinfo'],
                'callback' => 'index.php?s=api/login/oauthCallback',
            ],
            'log' => [
                'level' => 'debug',
                'permission' => 0777,
                'file' => LOG_PATH . 'easywechat.log',
            ]
        ];
    }


    /**\
     * @throws \think\exception\DbException
     */
    public static function wxPay()
    {
        $wx = self::detail();

        $config = [
            // 前面的appid什么的也得保留哦
            'app_id'             => $wx['app_id'],
            'mch_id'             => $wx['mchid'],
            'key'                => $wx['apikey'],
            'cert_path'          => ROOT_PATH.'application/apiclient_cert.pem', // XXX: 绝对路径！！！！
            'key_path'           => ROOT_PATH.'application/apiclient_key.pem',      // XXX: 绝对路径！！！！
            'notify_url'         => config('host_url').'notice.php', // 你也可以在下单时单独设置来想覆盖它
            // 'device_info'     => '013467007045764',
            // 'sub_app_id'      => '',
            // 'sub_merchant_id' => '',
            // ...
        ];

        return $config;
    }
}
