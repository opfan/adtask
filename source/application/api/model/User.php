<?php

namespace app\api\model;

use app\common\exception\BaseException;
use app\common\model\Setting;
use app\common\model\User as UserModel;
use app\common\service\Qrcode;

/**
 * 用户模型类
 * Class User
 * @package app\api\model
 */
class User extends UserModel
{

    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden
        = [
            'open_id',
            'is_delete',
            'wxapp_id',
            'create_time',
            'update_time',
        ];

    /**
     * @desc getUser
     * @return \app\api\model\User|bool|null
     * @throws \think\exception\DbException
     */
    public static function getUser()
    {

        $session = session('wechat_user');

        if (!$session) {
            return false;
        }

        return self::detail(['open_id' => $session['openid']]);
    }

    /**
     * @desc  login
     * @param $post
     * @return array
     * @throws \app\common\exception\BaseException
     * @throws \think\Exception
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function login($post)
    {
        $user = self::detail(['open_id' => $post['openid']]);
        //是否是新用户
        $is_new = $user ? false :true;
        // 邀请人id
        $refereeId = isset($post['invite_id']) ? $post['invite_id'] : 0;

        $data = [
            'open_id'    => $post['openid'],
            'nickName'   => $post['nickname'],
            'province'   => $post['province'],
            'city'       => $post['city'],
            'country'    => $post['country'],
            'avatarUrl'  => $post['headimgurl'],
            'gender'     => (int)$post['sex'],
        ];
        //活动配置
        $config = Setting::getItem('activity');

        //活动是否开启
        $activity_is_open = $config['is_open'];

        $model = $user ?: $this;

        //绑定推荐人关系
        if($is_new || empty($post['referee_id'])){
            $data['referee_id'] = $refereeId;
            $data['referee_ime'] = time();
            $is_new && $activity_is_open && $data['balance'] = $config['login_money'];
        }

        $this->startTrans();
        
        try {

            // 保存/更新用户记录
            if ($model->allowField(true)->save($data) === false) {
                throw new BaseException(['msg' => '用户注册失败']);
            }

            //系统赠送红包金额
            if($is_new){

                if($activity_is_open){
                    //注册赠送佣金
                    //记录佣金
                    (new Commission())->add(array_merge($data,[
                        'user_id' => $model->user_id,
                        'type'  => 1,
                        'money' => $config['login_money'],
                    ]));

                    //分享人赠送
                    $invite_id = $post['invite_id'] ?? ($post['referee_id']?? 0);
                    if($invite_id){
                        $invite = self::get($invite_id);
                        if($invite){
                            //记录佣金
                            (new Commission())->add(array_merge($data,[
                                'user_id' => $invite_id,
                                'type'  => 2,
                                'money' => $config['share_money'],
                            ]));
                            $invite->setInc('balance',$config['share_money']);
                        }
                    }
                }
            }


            
            $this->commit();
            
        } catch (\Exception $e) {

            $this->rollback();
            throw new BaseException(['msg' => $e->getMessage()]);
        }

        session('wechat_user', $post);

        return [
            'user_id' => $model['user_id'],
            'is_new' => $is_new,
        ];

    }


    /**
     * @param $user
     * @return array|bool|false
     * @throws \Exception
     */
    private function addVirtualUser($user)
    {

        $config = Setting::getItem('store');
        
        if($config['virtual_num'] <= 0){
            return true;
        }
        
        $tmp = [
            'is_virtual' => 1,
            'referee_id' => $user['user_id'],
            'province'   => $user['province'],
            'city'       => $user['city'],
            'country'    => $user['country'],
            'position_id' => 0,
            'category_id' => 0,
            'nickName'   => '',
            'avatarUrl'  => ''
        ];

        $range = $this->getRange($user['longitude'], $user['latitude'], 10 * 1000);

        for ($i = 0; $i < $config['virtual_num']; $i++) {

            $insert[] = array_merge($tmp, [
                'longitude' => sprintf('%.6f',randomFloat($range['minLon'],$range['maxLon'])),
                'latitude'  => sprintf('%.6f',randomFloat($range['minLat'], $range['maxLat'])),
                'gender'    => rand(1, 2),
                'avatarUrl'    => $this->makeAvatar(),
                'nickName'   => $this->makeNickName()
            ]);
        }

        return self::saveAll($insert);

    }

    /**
     * @desc  getRange
     * @param        $lng
     * @param        $lat
     * @param float  $distance
     * @return array
     */
    public static function getRange($lng, $lat, $distance=0.5)
    {
        $distance = $distance/1000;

        $EARTH_RADIUS = 6371;//地球半径，平均半径为6371km

        $dlng = 2 * asin(sin($distance / (2 * $EARTH_RADIUS) / cos(deg2rad($lat))));
        $dlng = rad2deg($dlng);
        $dlat = $distance / $EARTH_RADIUS;
        $dlat = rad2deg($dlat);

        $range =  [
            'minLat' => sprintf('%.6f',$lat - $dlat),
            'maxLat' => sprintf('%.6f',$lat + $dlat),
            'minLon' => sprintf('%.6f',$lng -$dlng),
            'maxLon' => sprintf('%.6f',$lng + $dlng),
        ];

        return $range;
    }

    /**
     * 计算两点之间的距离
     * @param $lng1 经度1
     * @param $lat1 纬度1
     * @param $lng2 经度2
     * @param $lat2 纬度2
     * @param int $unit m，km
     * @param int $decimal 位数
     * @return float
     */
    public static function  getDistance($lng1, $lat1, $lng2, $lat2, $unit = 2, $decimal = 2)
    {

        $EARTH_RADIUS = 6370.996; // 地球半径系数
        $PI           = 3.1415926535898;

        $radLat1 = $lat1 * $PI / 180.0;
        $radLat2 = $lat2 * $PI / 180.0;

        $radLng1 = $lng1 * $PI / 180.0;
        $radLng2 = $lng2 * $PI / 180.0;

        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;

        $distance = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
        $distance = $distance * $EARTH_RADIUS * 1000;

        if ($unit === 2) {
            $distance /= 1000;
        }

        return round($distance, $decimal);
    }

    /**
     * @desc  GetRange
     * @param float  $lat     坐标纬度
     * @param float  $lon     坐标经度
     * @param float  $raidus  半径(m)
     * @return array
     */
//    public function getRange($lon, $lat, $raidus)
//    {
//
//        $pi = 3.1415926535898;//圆周率
//        //计算纬度
//        $degree    = (24901 * 1609) / 360.0;
//        $dpmLat    = 1 / $degree;
//        $radiusLat = $dpmLat * $raidus;
//        $minLat    = $lat - $radiusLat; //得到最小纬度
//        $maxLat    = $lat + $radiusLat; //得到最大纬度
//        //计算经度
//        $mpdLng    = $degree * cos($lat * ($pi / 180));
//        $dpmLng    = 1 / $mpdLng;
//        $radiusLng = $dpmLng * $raidus;
//        $minLng    = $lon - $radiusLng; //得到最小经度
//        $maxLng    = $lon + $radiusLng; //得到最大经度
//
//        //范围
//        $range = [
//            'minLat' => sprintf('%.6f',$minLat),
//            'maxLat' => sprintf('%.6f',$maxLat),
//            'minLon' => sprintf('%.6f',$minLng),
//            'maxLon' => sprintf('%.6f',$maxLng),
//        ];
//
//        return $range;
//    }

    /**
     * 微信配置
     * @desc  wxConfig
     * @return Wxapp|mixed|null
     * @throws BaseException
     * @throws \think\exception\DbException
     */
    private function wxConfig()
    {

        // 获取当前小程序信息
        $wxConfig = Wxapp::getWxappCache();
        // 验证appid和appsecret是否填写
        if (empty($wxConfig['app_id']) || empty($wxConfig['app_secret'])) {
            throw new BaseException(['msg' => '请到 [后台-小程序设置] 填写appid 和 appsecret']);
        }

        return $wxConfig;

    }

    /**
     * @desc makeNickName
     */
    private function makeNickName()
    {

        $a  = "赵 钱 孙 李 周 吴 郑 王 冯 陈 楮 卫 蒋 沈 韩 杨 朱 秦 尤 许 何 吕 施 张 孔 曹 严 华 金 魏 陶 姜 戚 谢 邹 喻 柏 水 窦 章 云 苏 潘 葛 奚 范 彭 郎 鲁 韦 昌 马 苗 凤 花 方 俞 任 袁 柳 酆 鲍 史 唐 费 廉 岑 薛 雷 贺 倪 汤 滕 殷 罗 毕 郝 邬 安 常 乐 于 时 傅 皮 卞 齐 康 伍 余 元 卜 顾 孟 平 黄 和 穆 萧 尹 姚 邵 湛 汪 祁 毛 禹 狄 米 贝 明 臧 计 伏 成 戴 谈 宋 茅 庞 熊 纪 舒 屈 项 祝 董 梁 杜 阮 蓝 闽 席 季 麻 强 贾 路 娄 危 江 童 颜 郭 梅 盛 林 刁 锺 徐 丘 骆 高 夏 蔡 田 樊 胡 凌 霍 虞 万 支 柯 昝 管 卢 莫 经 房 裘 缪 干 解 应 宗 丁 宣 贲 邓 郁 单 杭 洪 包 诸 左 石 崔 吉 钮 龚 程 嵇 邢 滑 裴 陆 荣 翁 荀 羊 於 惠 甄 麹 家 封 芮 羿 储 靳 汲 邴 糜 松 井 段 富 巫 乌 焦 巴 弓 牧 隗 山 谷 车 侯 宓 蓬 全 郗 班 仰 秋 仲 伊 宫 宁 仇 栾 暴 甘 斜 厉 戎 祖 武 符 刘 景 詹 束 龙 叶 幸 司 韶 郜 黎 蓟 薄 印 宿 白 怀 蒲 邰 从 鄂 索 咸 籍 赖 卓 蔺 屠 蒙 池 乔 阴 郁 胥 能 苍 双 闻 莘 党 翟 谭 贡 劳 逄 姬 申 扶 堵 冉 宰 郦 雍 郤 璩 桑 桂 濮 牛 寿 通 边 扈 燕 冀 郏 浦 尚 农 温 别 庄 晏 柴 瞿 阎 充 慕 连 茹 习 宦 艾 鱼 容 向 古 易 慎 戈 廖 庾 终 暨 居 衡 步 都 耿 满 弘 匡 国 文 寇 广 禄 阙 东 欧 殳 沃 利 蔚 越 夔 隆 师 巩 厍 聂 晁 勾 敖 融 冷 訾 辛 阚 那 简 饶 空 曾 毋 沙 乜 养 鞠 须 丰 巢 关 蒯 相 查 后 荆 红 游 竺 权 逑 盖 益 桓 公 万俟 司马 上官 欧阳 夏侯 诸葛 闻人 东方 赫连 皇甫 尉迟 公羊 澹台 公冶 宗政 濮阳 淳于 单于 太叔 申屠 公孙 仲孙 轩辕 令狐 锺离 宇文 长孙 慕容 鲜于 闾丘 司徒 司空 丌官 司寇 仉 督 子车 颛孙 端木 巫马 公西 漆雕 乐正 壤驷 公良 拓拔 夹谷 宰父 谷梁 晋 楚 阎 法 汝 鄢 涂 钦 段干 百里 东郭 南门 呼延 归 海 羊舌 微生 岳 帅 缑 亢 况 后 有 琴 梁丘 左丘 东门 西门 商 牟 佘 佴 伯 赏 南宫 墨 哈 谯 笪 年 爱 阳 佟 第五 言 福";
        $aa = explode(' ', $a);

        $b  = '彬 轩 含 蒲 乒 虚 行 亭 仑 蓝 影 韬 函 克 盛 衡 芝 晗 昊 诗 琦 至 涵 伦 时 映 志 菱 纶 士 永 致 嘉 旷 示 咏 智 安 轮 世 勇 中 昂 律 业 友 忠 敖 齐 轼 桓 林 言 群 书 有 宣 颁 略 伟 骢 州 清 宏 充 佑 洲 庭 马 濮 丹 乐 邦 迈 卫 平 乾 榜 宸 蔚 旲 东 宝 昴 树 材 纪 保 茂 泓 棋 竹 葆 浩 魏 妤 铸 劻 玫 晔 渝 壮 羚 阳 文 瑜 卓 掣 奎 船 与 萱 豹 梅 汶 旭 濯 驾 和 航 宇 孜 邶 望 武 羽 崊 霆 美 希 雨 淑 冰 蒙 才 凰 腾 备 密 溪 泰 子 辈 冕 帅 语 茜 蓓 淼 曦 玉 梓 弼 民 奇 禾 综 碧 洋 霞 连 祖 厚 晨 先 昱 选 昪 旻 虹 朔 济 彪 淏 贤 儋 冬 龄 馗 娴 钰 栋 飙 传 舷 御 端 澜 然 磊 裕 段 挺 名 春 誉 天 飚 明 灏 堂 碫 莱 鸣 双 渊 琳 坚 茗 一 元 倩 宾 村 宪 辉 铎 妍 铭 献 彭 思 策 谋 祥 序 伯 骞 牧 翔 启 恩 建 慕 向 沅 发 汗 穆 骁 溓 帆 健 恒 洪 媛 汉 键 威 晓 源 冀 勒 成 笑 远 弘 龙 仁 蕾 棠 凡 江 魁 伊 德 方 城 铿 顺 月 飞 萍 皓 朴 悦 学 骄 楠 啸 绪 强 鲛 妮 勰 跃 霖 劼 宁 兵 越 芬 杰 弩 淳 起 丰 洁 攀 心 云 风 柴 旁 昕 会 沣 婕 薇 欣 良 泊 同 沛 新 芸 川 悍 佩 依 颇 封 金 松 鸿 耘 峰 岩 日 竦 韵 勋 辰 朋 沂 坤 骥 晴 岚 怡 泽 锋 津 荣 信 增 澔 锦 容 立 波 乔 瑾 鹏 宜 登 凤 进 铖 达 承 豪 晋 榕 华 展 福 菁 韦 以 章 俯 彤 融 来 彰 恬 景 力 亿 涛 辅 炎 茹 义 梁 迅 璟 儒 瀚 浦 富 禅 采 艺 基 澉 颔 襦 星 钊 刚 庆 锐 议 昭 博 珑 斌 亦 照 纲 敬 瑞 佚 哲 合 靖 澎 励 喆 佳 驹 睿 易 绮 钢 聚 垒 奕 真 苓 万 尧 益 臻 阔 颜 若 淇 焘 聪 涓 飒 骅 沧 罡 娟 弛 朗 帝 高 军 森 兴 缜 歌 钧 砂 大 畅 弓 筠 山 谊 亮 功 丞 河 逸 稹 巩 全 善 意 舱 固 俊 超 溢 振 钦 隆 频 毅 朕 冠 翰 候 利 谦 部 彦 为 茵 震 谱 韩 劭 英 理 廷 昌 绍 琪 滔 家 骏 社 雄 镇 凌 珺 升 崇 征 光 竣 生 鹰 正 广 凯 圣 迎 诤 晷 铠 驰 寒 政 贵 康 胜 桦 琛 国 泉 晟 盈 殿 海 科 礼 代 之 卿 诚 耀 滢 吉 鑫 谚 亨 瀛 舜 延 可 维 逸';
        $bb = explode(' ', $b);

        $c  = '菡 娆 炫 源 卉 娘 蕊 娜 纤 蔓 凡 怡 蒙 嫔 敏 花 叶 琰 汇 妃 莲 娥 娴 雪 露 素 菁 然 青 艳 薰 苑 莺 晶 岚 卿 香 艺 亚 滟 璟 娉 爽 霄 美 瑗 惠 婧 霭 风 水 影 月 蓓 靖 平 纨 嵘 呤 柳 蓝 眉 评 聪 丹 咏 秋 银 茗 丝 宛 晓 悠 曼 明 静 苹 菀 晴 诗 玥 紫 宁 舒 囡 心 俞 楚 漫 璧 梅 娈 芯 可 炎 玟 林 屏 忆 音 姹 妙 慧 蓉 嫦 若 代 华 瑶 念 英 莓 婉 蝶 淼 悦 火 姗 莉 盈 欣 如 馥 姬 馡 依 谷 翎 瑜 娅 珆 燕 菱 琳 伶 羽 越 姞 菊 萍 琬 荣 梓 枫 冽 娟 芮 薇 炅 思 云 佳 君 艾 烟 瑞 含 芸 玲 茵 叆 好 歆 嫣 韵 嘉 筠 琪 媚 媱 珴 昭 冰 珂 纯 宜 琼 妮 芳 妆 姳 琦 冉 蕾 海 涵 白 咛 姲 荭 馨 淞 怀 珊 伊 棋 文 星 雨 贞 丽 凝 绮 仪 楠 语 珍 姜 奴 芊 梦 秀 玉 草 菲 女 茜 纹 旭 渺 姝 娇 桃 霜 雯 絮 育 涟 姣 偀 彤 妩 萱 采 偲 妹 凤 倩 南 亿 钰 璐 洁 盼 嫱 姯 滢 彨 颜 玫 婵 柔 希 妶 倪 茹 芬 清 红 翠 旋 煜 真 碧 赫 慕 曦 雁 瑷 芝 姑 婷 漪 宝 桂 竹 欢 姿 澜 彩 冷 听 画 枝 婕 淑 芙 禧 波 雅 芷 姐 沛 巧 霖 萌 晗 荔 莎 兰 怜 寻 黛 毓 珠 春 俪 晨 莹 容 妍 寒 锦 佩 芹 娣 灵 园 烁 倰 瑛 琴 情 漩 媛 环 霏 芃 湾 贻 璇 荷 嫂 檀 融 勤 霞 颖 安 幻 瑾 飘 爱';
        $cc = explode(' ', $c);

        $a_num = rand(0, 503);
        $b_num = rand(0, 559);
        $c_num = rand(0, 122);
        $type  = rand(2, 3);

        if ($type == 0) {
            $username = $aa[$a_num] . $bb[$b_num] . $cc[$c_num];
        } else if ($type == 1) {
            $username = $aa[$a_num] . $cc[$c_num];
        } else {
            $username = $aa[$a_num] . $bb[$b_num];
        }

        return $username;
    }



    /**
     * @param $user
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getNearby($user)
    {
        //附近的人
        $distance = 10*1000;

        $range = User::getRange($user['longitude'],$user['latitude'],$distance);

        $list = $this->field('user_id,avatarUrl,nickName,longitude,latitude')->where(['longitude'=>['between',[$range['minLon'],$range['maxLon']],'latitude'=>['between',[$range['minLat'],$range['maxLat']]]]])->where('user_id','neq',$user['user_id'])->limit(30)->select();

        return $list;
    }

    /**
     * @param $data
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function updateUser($data)
    {
        $user = self::get($data['user_id']);

        $location = ['longitude' => $user['longitude'],'latitude'=> $user['latitude']];

//        if(empty($data['category_id']) && $data['auth']){
//            $this->error = '请选择广告行业';
//            return false;
//        }
//        if(empty($data['position_id'])  && $data['auth']){
//            $this->error = '请选择职位';
//            return false;
//        }


        if(!empty($data['position_id'])){
            $position = Position::get($data['position_id']);
        }
    
        if(!empty($data['category_id'])){
            $category = Category::get($data['category_id']);
        }
        
        //判断是否填写了职业和行业，赠送曝光量
        if((empty($user['category_id']) || empty($user['position_id'])) && !empty($data['category_id']) && !empty($data['position_id'])){
            $config = Setting::getItem('activity');
            if($config['is_open']){
                $data['click'] = $config['click'];
            }
        }

        $update = [
            'position_id'=> $position['position_id'] ?? 0,
            'category_id'=> $category['category_id'] ?? 0,
            'click'=> $data['click'] ?? 0
        ];

        if((empty($user['latitude']) || empty($user['longitude'])) && !empty($data['longitude']) && !empty($data['latitude'])){
            $update['longitude'] = sprintf('%.6f',$data['longitude']);
            $update['latitude'] = sprintf('%.6f',$data['latitude']);
        }

        $result = $user->save($update);

        if($result === false){

            return false;
        }

        if(empty($update['longitude']) || empty($update['latitude'])){

            return true;
        }

        if (!empty($location['longitude']) && !empty($location['latitude']) && $location['longitude'] > 0 && $location['latitude'] > 0) {

            return true;

        }

        $this->addVirtualUser(array_merge($user->toArray(),$data));

        return true;
    }

    /**
     * @param $param
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getChildList($param)
    {
        // 商品列表获取条件
        $param = array_merge([
            'listRows'    => 8,       // 每页数量
        ], $param);

        // 筛选条件
        $filter = [
            'referee_id' => $param['user_id'],
            'is_virtual' => 0,
        ];

        // 执行查询
        $list = $this
            ->field('user_id,nickName,referee_time,avatarUrl')
            ->where($filter)
            ->order('referee_time desc')
//            ->fetchSql(true)->select();
            ->paginate($param['listRows'], false, [
                'query' => \request()->request(),
            ]);



        return $list;
    }


    /**
     * @param $userId
     * @return int|string
     * @throws \think\Exception
     */
    public static function childCount($userId)
    {
        return self::where(['referee_id'=> $userId ,'is_virtual'=> 0])->count();
    }

    /**
     * @param $data
     * @return array|bool|false
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function updateAddress($data)
    {

        $user = self::get($data['user_id']);

        if(!empty($user['latitude']) && !empty($user['longitude']) && $user['longitude'] > 0 && $user['latitude'] > 0){
            return true;
        }
        if(empty($data['latitude']) && empty($data['longitude'])){
            return true;
        }
        $update['longitude'] = sprintf('%.6f',$data['longitude']);
        $update['latitude'] = sprintf('%.6f',$data['latitude']);

        $user->save($update);

        return $this->addVirtualUser(array_merge($user->toArray(),$data));
    }

}