<?php

namespace app\api\model;

use app\common\model\Advertising as AdvertisingModel;
use app\common\model\AdvertisingLog;
use app\common\model\Setting;
use EasyWeChat\Factory;
use think\Cache;
use think\Db;

/**
 * Class Advertising
 * @package app\api\model
 */
class Advertising extends AdvertisingModel
{
    
    /**
     * 投放时间
     */
    Const TIME = [1, 2, 3];
    
    /**
     * @param         $param
     * @param string  $field
     * @return AdvertisingModel
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function detail($param, $field = '*')
    {
        
        $detail = parent::detail($param['ad_id'], 'ad_id,ad_name,play_type,sex,ad_type,click,user_click,money,user_money,share_click,thumb_id');

        return $detail;
    }
    
    /** 增加曝光量
     * @param $advertising
     * @param $param
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function addClick($advertising, $param)
    {
        $configMap = [1 => 'ad', 2 => 'ad_free'];

        $config = Setting::getItem($configMap[$advertising['ad_type']]);

        if (!$this->checkClick($advertising['ad_id'], $param['user_id'], $config['hour'])) {
            return;
        }

        $field = !empty($param['referee_id']) ? 'share_click' : 'user_click';

        $ad = self::get($advertising['ad_id']);

        $ad->setIncClick($field);
        
        $this->addLog($advertising, $param);

    }
    
    /**
     * @desc  checkClick
     * @param $ad_id
     * @param $user_id
     * @param $hour
     * @return bool
     */
    public function checkClick($ad_id, $user_id, $hour)
    {

        $key = 'click_user_id_' .$user_id . '_ad_id_' . $ad_id;

        if($hour==0){
            Cache::rm($key);
            return true;
        }


        if (Cache::has($key)) {
            return false;
        }

        return Cache::set($key, 1, $hour * 3600);
    }
    
    /** 曝光量记录
     * @desc  addLog
     * @param $advertising
     * @param $param
     * @return \app\common\model\AdvertisingLog
     */
    public function addLog($advertising, $param)
    {

        return AdvertisingLog::add($advertising['ad_id'], $param['user_id'], $param['referee_id'] ?? 0);
    }
    
    /*** 判断是否结束
     * @param $ad
     * @return bool
     */
    public static function checkIsEnd($ad)
    {
        
        if ($ad['origin'] == 1) {
            //商业广告 红包领完并且时间也到了才结束
            if ($ad['ad_type'] == 1) {
                if ($ad['money'] == 0 && $ad['etime'] < time()) {
                    return true;
                }
            } else {
                //公益广告点击量到达就结束
                if ($ad['click'] <= ($ad['user_click']+$ad['share_click'])) {
                    return true;
                }
            }
        } else {

            if ($ad['ad_type'] == 2) {
                //后台发布的公益广告 结束时间到达
                if ($ad['etime']+86400 < time()) {
                    return true;
                }
            } else {

                //商业广告红包领完就下架
                if (($ad['money'] == $ad['user_money']) && ($ad['click'] < ($ad['user_click'] + $ad['share_click']))) {
                    return true;
                }
            }
            
        }
        
        return false;
    }
    
    /**
     * @desc  getList
     * @param $param
     * @return \think\Paginator
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function getList($param)
    {
        
        // 商品列表获取条件
        $param = array_merge([
            'category_id' => 0,     // 分类id
            'position_id' => 0,     // 职业id
            'state'       => -1,          // 上下架
            'status'      => -1,          // 上下架
            'search'      => '',         // 搜索关键词
            'listRows'    => 4,       // 每页数量
            'origin'      => 2,       //默认后台发布
            'page'        => 1,
            'is_recommend'=> false, //是否推荐列表
        ], $param);
        
        $userModel = new User();
        $user      = $userModel::get($param['user_id']);
        
        $ad_config      = Setting::getItem('ad');
        $ad_free_config = Setting::getItem('ad_free');
        
        if (!$param['is_recommend'] && !empty($user['longitude']) && $user['latitude']) {
            //广告范围
            $param['range']      = $userModel->getRange($user['longitude'], $user['latitude'], $ad_config['distance'] * 1000);
            $param['free_range'] = $userModel->getRange($user['longitude'], $user['latitude'], $ad_free_config['distance'] * 1000);
        }
        
       
            //性别
        $param['sex'] = $user['gender'];
        
        // 筛选条件
        $filter = [
            'ad.is_delete' => 0,
            'ad.state'     => 10,
            'ad.status'    => 20,
            'ad.pay_status'   => 20,
            'ad.origin'    => $param['origin'],
        ];
        
        !empty($param['search']) && $filter['ad.ad_name|ad.user_name'] = ['like', '%' . trim($param['search']) . '%'];
        
        
        if (isset($param['start_time']) && !empty($param['start_time'])) {
            $this->where('ad.create_time', '>=', strtotime($param['start_time']));
        }
        if (isset($param['end_time']) && !empty($param['end_time'])) {
            $this->where('ad.create_time', '<', strtotime($param['end_time']) + 86400);
        }
        
        //商业广告范围
        if ( !$param['is_recommend'] && !empty($param['range'])) {
            $this->where("IF (`ad_type` = 1,(`aa`.`longitude` between " . $param['range']['minLon'] . " AND " . $param['range']['maxLon'] . ") AND `aa`.`latitude` between " . $param['range']['minLat'] . " AND " . $param['range']['maxLat'] . ",1=1)");
        }
        
        //公益广告范围
        if (!$param['is_recommend'] && !empty($param['free_range'])) {
            $this->where("IF (`ad_type` = 2,(`aa`.`longitude` between " . $param['free_range']['minLon'] . " AND " . $param['free_range']['maxLon'] . ") AND `aa`.`latitude` between " . $param['free_range']['minLat'] . " AND " . $param['free_range']['maxLat'] . ",1=1)");
        }
        
        if($param['origin'] == 2){
            //公益广告判断时间
            $this->where("IF (`ad_type` = 2,`ad`.`stime` < " . time() . " AND  `ad`.`etime`+86400 >" . time() . ",1=1)");
    
            //商业广告判断佣金是否用完了
            $this->where("IF (`ad_type` = 1,`ad`.`money` > `ad`.`user_money` OR `ad`.`click` > `ad`.`user_click`+`ad`.`share_click`,1=1)");
        }else{
            //公益广告判断点击量
            $this->where("IF (`ad_type` = 2,`ad`.`click` > `ad`.`user_click`+`ad`.`share_click`,1=1)");
            
            //商业广告判断红包是否用完或者结束时间
            $this->where("IF (`ad_type` = 1,`ad`.`money` > `ad`.`user_money` OR `ad`.`etime` > ".time().",1=1)");
            
        }
        
        
        //职业
        if (!$param['is_recommend'] && !empty($user['position_id'])) {
//            $this->where("IF (`origin` = 2,`p`.`position_id` = " .$user['position_id'] . ",1=1)");
    
            $this->where("IF (ISNULL(`p`.`position_id`) ,1=1,`p`.`position_id` = " .$user['position_id'] . ")");
        }
        
        //判断性别
        if (!$param['is_recommend'] && !empty($user->getData('gender'))) {
            
            $this->where("IF (`sex` != 0,`ad`.`sex` = " . $user->getData('gender') . ",1=1)");
        }
     
        //行业
        if (!$param['is_recommend'] && !empty($user['category_id'])) {
           
            $this->where("IF (ISNULL(`c`.`category_id`) ,1=1,`c`.`category_id` = " .$user['category_id'] . ")");
        }
    
        $orderBy = 'ad.ad_id desc';
        
        //判断是否有id
        if(!empty($param['ad_id']) && $param['page'] == 1){
            
            $this->whereOr('ad.ad_id',$param['ad_id']);
           
            $orderBy =  Db::raw('field(ad.ad_id,'.$param['ad_id'].') DESC,ad.ad_id DESC');
        }
        
        // 执行查询
        $list = $this->alias('ad')
            ->field('ad.ad_id,ad.money,ad.ad_type,ad.origin,ad.ad_name,ad.play_type,ad.ad_type,ad.click,ad.user_click,ad.share_click,u.avatarUrl,ad.thumb_id,ad.describe')
            ->with(['url.file', 'address','file'])
            ->join('advertising_position p', 'p.ad_id = ad.ad_id', 'left')
            ->join('advertising_category c', 'c.ad_id = ad.ad_id', 'left')
            ->join('advertising_address aa', 'aa.ad_id = ad.ad_id', 'left')
            ->join('user u', 'ad.user_id = u.user_id', 'left')
            ->where($filter)
            ->order($orderBy)
            ->group('ad.ad_id')
//            ->fetchSql(true)->select();
            ->paginate($param['listRows'], false, [
                'query' => \request()->request(),
            ]);

        //曝光量+1
        if (!$list->isEmpty()) {
            foreach ($list as $item) {
                $this->addClick($item, $param);
                $item['share_click'] = AdvertisingLog::getUserShare($param['user_id'],$item['ad_id']);
            }
        }
        
        return $list; // TODO: Change the autogenerated stub
    }
    
    /**
     * @desc  add
     * @param $data
     * @return bool|void
     * @throws \think\exception\PDOException
     */
    public function add($data)
    {
        
        !empty($data['category_id']) && $data['category'] = explode(',', $data['category_id']);
        $data['file_id']  = explode(',', $data['file_id']);
        
        if ($data['ad_type'] == 2) {
            $data['address'][] = [
                'longitude' => $data['user']['longitude'],
                'latitude'  => $data['user']['latitude'],
                'province'  => $data['user']['province'],
                'city'      => $data['user']['city'],
                'region'    => $data['user']['region'] ?? '',
                'address'   => $data['user']['province'].$data['user']['city'].$data['user']['region'].$data['user']['address'] ?? '',
            ];
            //免费广告，点击量抵扣
            $data['pay_type'] = 30;
        }
        
        foreach ($data['file_id'] as $item) {
            $data['urls'][]['file_id'] = $item;
        }
        $data['user_name'] = $data['user']['nickName'];
        $data['phone']     = $data['user']['phone'];
        $data['origin']    = 1;
        $data['status']  = 20;

        if ($data['pay_type'] == 20 || $data['ad_type'] == 2) {
            $data['pay_status'] = 20;
            $data['pay_time']   = time();
        }
        
        $map           = [1 => 1, 2 => 7, 3 => 30];
        $data['stime'] = time();
        
        if ($data['ad_type'] == 1) {
            //投放时间
            $data['etime'] = time() + $map[$data['time']] * 86400;
        }
        
        return parent::add($data);
    }
    
    /**
     * @desc  checkOut
     * @param $data
     * @return bool|int|string
     * @throws \think\exception\DbException
     */
    public function checkOut($data)
    {
        
        if (!in_array($data['ad_type'], self::AD_TYPE)) {
            $this->error = '广告类型错误';
            
            return false;
        }
        
        $config = Setting::getItem($this->configMpa[$data['ad_type']]);
        
        if (empty($data['ad_name'])) {
            $this->error = '请输入广告名称';
            
            return false;
        }
        
        if (empty($data['file_id'])) {
            $this->error = '请上传图片';
            
            return false;
        }
        
        $data['file_id'] = explode(',', $data['file_id']);
        
        if (count($data['file_id']) > 9) {
            $this->error = '最多只能上传9张图片';
            
            return false;
        }
        
        
        $user = User::detail($data['user_id']);
        //可用余额
        $userBalance = $user['useBalance'];
        
        if ($data['ad_type'] == 2) {
            
            if (empty($data['click'])) {
                $this->error = '请输入曝光量';
                
                return false;
            }
            
            if ($data['click'] > $user['click']) {
                $this->error = '可用曝光量不足';
                
                return false;
            }
            
            return 0;
        } else {
    
//            if (empty($data['category_id'])) {
//                $this->error = '请选择投放行业';
//
//                return false;
//            }
            
            if (!in_array($data['time'], self::TIME)) {
                $this->error = '请选择发布时间';
                
                return false;
            }
            
            if (empty($data['address'])) {
                $this->error = '请选择发布地址';
                
                return false;
            }
            
            //投放限制
            if (count($data['address']) > $config['address_num']) {
                $this->error = '最多只能投放' . $config['address_num'] . '个';
                
                return false;
            }
            
            if (!isset($data['sex']) || !in_array($data['sex'], [0, 1, 2])) {
                $this->error = '请选择投放人群';
                
                return false;
            }
            
            if (!in_array($data['pay_type'], self::PAY_TYPE)) {
                $this->error = '请选择支付方式';
                
                return false;
            }
            
            if (empty($data['money'])) {
                $this->error = '请输入金额';
                
                return false;
            }
            
            $money = $this->getPay($data, $config);
            
            if ($data['money'] < $money) {
                $this->error = '输入的金额错误';
                
                return false;
            }

            //余额支付
            if (($data['pay_type'] == 20) && ($userBalance < $data['money'])) {
                $this->error = '可用余额不足';
                
                return false;
            }

            return $data['money'];
        }
        
    }
    
    /**
     * @param      $data
     * @param      $config
     * @param int  $count
     * @return float
     */
    private function getPay($data, $config, $count = 1)
    {
        
        $dayCost = $config['cost'] / 7;
        
        $dayMap = [1 => 1, 2 => 7, 3 => 30];
        
        return round($dayMap[$data['time']] * $dayCost * $count, 2);
    }
    
    /*** 获取付费广告金额
     * @param $data
     * @return float
     */
    public function getPayMoney($data)
    {
        
        $config = Setting::getItem('ad');
        $money = $this->getPay($data, $config, $data['num']);
        $min_money = $config['pay'] > $money ? $config['pay'] : $money;
        
        return compact('money','min_money');
    }
    
    /**
     * @param $data
     * @param $user
     * @return array|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \think\exception\DbException
     */
    public function wxPay($data, $user)
    {
        
        $config = Wxapp::wxPay();
        
        $app = Factory::payment($config);
        
        $result = $app->order->unify([
            'body'         => '付费广告充值',
            'out_trade_no' => $data['ad_no'],
            'total_fee'    => $data['money'] * 100,
            //            'spbill_create_ip' => '123.12.12.123', // 可选，如不传该参数，SDK 将会自动获取相应 IP 地址
            //            'notify_url' => 'https://pay.weixin.qq.com/wxpay/pay.action', // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            'trade_type'   => 'JSAPI', // 请对应换成你的支付方式对应的值类型
            'openid'       => $user['open_id'],
        ]);
        
        if ($result['return_code'] !== 'SUCCESS') {
            return false;
        }
// $result:
//{
//    "return_code": "SUCCESS",
//    "return_msg": "OK",
//    "appid": "wx2421b1c4390ec4sb",
//    "mch_id": "10000100",
//    "nonce_str": "IITRi8Iabbblz1J",
//    "openid": "oUpF8uMuAJO_M2pxb1Q9zNjWeSs6o",
//    "sign": "7921E432F65EB8ED0CE9755F0E86D72F2",
//    "result_code": "SUCCESS",
//    "prepay_id": "wx201411102639507cbf6ffd8b0779950874",
//    "trade_type": "JSAPI"
//}
        $jssdk = $app->jssdk;
        //微信内置浏览器
        $json = $jssdk->bridgeConfig($result['prepay_id']); // 返回 json 字符串，如果想返回数组，传第二个参数 false
        //jssdk
//        $json = $jssdk->sdkConfig($result['prepay_id']); // 返回数组


        return $json;
    }
    
    /**
     * @desc  payNotify
     * @param $advertising
     * @param $data
     * @return mixed
     */
    public function payNotify($advertising,$data)
    {
        
        $update = [
            'pay_status'     => 20,
            'transaction_id' => $data['transaction_id'],
            'pay_time'       => time(),
        ];
        
        return $advertising->save($update);
    }

    /** 我的任务
     * @param $param
     * @return \think\Paginator
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function taskList($param)
    {
        
        // 商品列表获取条件
        $param = array_merge([
            'category_id' => 0,     // 分类id
            'position_id' => 0,     // 职业id
            'state'       => -1,          // 上下架
            'status'      => -1,          // 上下架
            'search'      => '',         // 搜索关键词
            'listRows'    => 5,       // 每页数量
            'type'        => 1,      //筛选类型 1 全部 2 已参加 3未参加 4 已结束
        ], $param);
        
        $userModel = new User();
        
        $user      = $userModel::get($param['user_id']);
        
        $ad_config      = Setting::getItem('ad');
        $ad_free_config = Setting::getItem('ad_free');
        
        if (!empty($user['longitude']) && $user['latitude']) {
            //广告范围
            $param['range']      = $userModel->getRange($user['longitude'], $user['latitude'], $ad_config['distance'] * 1000);
            $param['free_range'] = $userModel->getRange($user['longitude'], $user['latitude'], $ad_free_config['distance'] * 1000);
        }
        
        //性别
        $param['sex'] = $user['gender'];
        
        // 筛选条件
        $filter = [
            'ad.is_delete' => 0,
            'ad.state'     => 10,
            'ad.status'    => 20,
            'ad.pay_status'   => 20,

        ];
        
        !empty($param['search']) && $filter['ad.ad_name'] = ['like', '%' . trim($param['search']) . '%'];
        
        
        //职业
        if (!empty($user['position_id'])) {
            $this->where("IF (ISNULL(`p`.`position_id`) ,1=1,`p`.`position_id` = " .$user['position_id'] . ")");

//            $filter['p.position_id'] = $user['position_id'];
        }

        
        //判断性别
        if (!empty($user->getData('gender'))) {
            
            $this->where("IF (`sex` != 0,`ad`.`sex` = " . $user->getData('gender') . ",1=1)");
        }
    
        //行业
        if (!empty($user['category_id'])) {
        
            $this->where("IF (ISNULL(`c`.`category_id`) ,1=1,`c`.`category_id` = " .$user['category_id'] . ")");
        }
        
        //已结束
        if($param['type'] == 4){
            //后台发布的
            $this->where("IF(`ad`.`origin` = 2,IF(`ad`.`ad_type` =1,`ad`.`money`=`ad`.`user_money` AND `ad`.`click` < `ad`.`user_click`+`ad`.`share_click` ,`ad`.`etime`+86400 < ".time()."),1=1)");
            
//            //前台发布的
            $this->where("IF(`ad`.`origin` = 1,IF(`ad`.`ad_type` =1,`ad`.`money`=`ad`.`user_money` AND `ad`.`etime` <".time().",`ad`.`click` < `ad`.`user_click` + `ad`.`share_click`),1=1)");
        }
        elseif($param['type'] == 3){
            //未参与
            //获取参加的ad_id
            $adIds = (new AdvertisingLog())->where(['share_id'=> $param['user_id']])->group('ad_id')->column('ad_id');
            $filter['ad.ad_id'] = ['not in',$adIds];
            
        }elseif ($param['type'] == 2){
            //已参与
            $filter['al.share_id'] = $param['user_id'];
        }
        
        
        // 执行查询
        $list = $this->alias('ad')
            ->field('ad.ad_id,ad.stime,ad.origin,ad.etime,ad.user_money,ad.money,ad.ad_name,ad.play_type,ad.ad_type,ad.click,ad.user_click,ad.share_click,ad.create_time,al.log_id,ad.thumb_id,ad.describe')
            ->with(['url.file','file'])
            ->join('advertising_position p', 'p.ad_id = ad.ad_id', 'left')
            ->join('advertising_category c', 'c.ad_id = ad.ad_id', 'left')
            ->join('advertising_address aa', 'aa.ad_id = ad.ad_id', 'left')
            ->join('user u', 'ad.user_id = u.user_id', 'left')
            ->join('advertising_log al', 'al.ad_id = ad.ad_id', 'right')
            ->where($filter)
            ->order('ad.ad_id desc')
            ->group('ad.ad_id')
//            ->fetchSql(true)->select();
            ->paginate($param['listRows'], false, [
                'query' => \request()->request(),
            ]);

        if(!$list->isEmpty()){
            foreach ($list as &$item){
                $item['share_time'] = AdvertisingLog::getUserShare($param['user_id'],$item['ad_id']);
                $item['rest_click'] = (($item['user_click']+ $item['share_click']) > $item['click']) ? 0 : $item['click'] - ($item['user_click']+ $item['share_click']);
                $item['is_end'] = Advertising::checkIsEnd($item);
            }
        }
        return $list;
    }

    /**商业广告、已发红包
     * @param $param
     * @return \think\Paginator
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function user_list($param)
    {
        // 商品列表获取条件
        $param = array_merge([
            'search'      => '',         // 搜索关键词
            'listRows'    => 5,       // 每页数量
            'origin'      => 1,      // 1 前台 已发红包 2 后台商业广告
            'type'         => 1,    // 1 全部 2进行中 2结束
        ], $param);

        // 筛选条件
        $filter = [
            'ad.is_delete' => 0,
            'ad.state'     => 10,
            'ad.status'    => 20,
            'ad.pay_status'   => 20,
            'ad.user_id'   => $param['user_id'],
            'ad.origin'   => $param['origin'],
        ];

        !empty($param['search']) && $filter['ad.ad_name'] = ['like', '%' . trim($param['search']) . '%'];

        if($param['origin'] ==2){
            //已结束
            if($param['type'] == 3){
                $this->where("IF(`ad`.`ad_type` =1,`ad`.`money`=`ad`.`user_money` AND `ad`.`click` < `ad`.`user_click`+`ad`.`share_click`,`ad`.`etime`+86400 < ".time().")");

            }elseif($param['type'] == 2){
                //进行中
                $this->where("IF(`ad`.`ad_type` =1,`ad`.`money`>`ad`.`user_money` OR `ad`.`click` > `ad`.`user_click`+`ad`.`share_click`,`ad`.`etime`+86400 > ".time().")");
            }
        }
        // 执行查询
        $list = $this->alias('ad')
            ->field('ad.ad_id,ad.stime,sex,ad.origin,ad.etime,ad.user_money,ad.money,ad.ad_name,ad.play_type,ad.ad_type,ad.click,ad.user_click,ad.share_click,ad.create_time,ad.thumb_id,ad.describe')
            ->with(['url.file','file'])
            ->join('advertising_category c', 'c.ad_id = ad.ad_id', 'left')
            ->join('user u', 'ad.user_id = u.user_id', 'left')
            ->where($filter)
            ->order('ad.ad_id desc')
            ->group('ad.ad_id')
//            ->fetchSql(true)->select();
            ->paginate($param['listRows'], false, [
                'query' => \request()->request(),
            ]);

        if(!$list->isEmpty()){
            foreach ($list as &$item){
                
                $category = AdvertisingCategory::all(['ad_id'=> $item['ad_id']]);

                foreach ($category as $value){
                    $industry[]= $value->category['name'];
                }
                $item['industry'] = $industry ?? [];
//
                $item['is_end'] = Advertising::checkIsEnd($item);
            }
        }
        return $list;
    }




}
