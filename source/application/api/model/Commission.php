<?php


namespace app\api\model;


use app\common\model\AdvertisingLog;
use app\common\model\Setting;
use think\Cache;

class Commission extends \app\common\model\finance\Commission
{
    
    /**
     * @desc  getList
     * @param $param
     * @return \think\Paginator
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function getList($param)
    {
        $filter = [];

        // 商品列表获取条件
        $param = array_merge([
            'stime'  => 0,
            'etime'   => 0,
            'user_id' => 0,
            'search' => '',         // 搜索关键词
            'listRows' => 4,       // 每页数量
        ], $param);
        //开始时间
        if(!empty($param['stime'])){
            $filter['c.create_time'][] = ['>=',$param['stime']];
        }
        //结束时间
        if(!empty($param['etime'])){
            $filter['c.create_time'][] = ['<',$param['etime']+86400,'AND'];
        }

        if(!empty($param['search'])){
            $filter['ad.ad_name'] = $param['search'];
        }

        if($param['user_id']){
            $filter['c.user_id'] = $param['user_id'];
        }

        //商业广告的获取到的佣金
        $filter['type'] = ['in',[3,4]];
//        print_r($filter);exit;
        // 执行查询
        $list = $this->alias('c')
            ->field('c.*,u.user_id,u.nickName')
            ->join('advertising ad','ad.ad_id=c.ad_id')
            ->join('user u','c.parent_id=u.user_id','left')
            ->with(['advertising'=>function($query){$query->field('ad_id,ad_name,etime,ad_type,click,user_click,share_click,money,user_money,play_type,click,origin,create_time,thumb_id')->with(['url.file','file']);}])
            ->where($filter)
            ->order('c.id desc')
            ->paginate($param['listRows'], false, [
                'query' => request()->request()
            ]);

        if(!$list->isEmpty()){
            foreach ($list as $item){

                $count  = AdvertisingLog::where(['ad_id'=> $item['ad_id'],'share_id'=> $param['user_id']])->count('log_id');
                //已转发
                $item['count'] = $count;
                
                //是否结束
                $item['is_end'] = Advertising::checkIsEnd($item['advertising']);
//                if($item['advertising']['origin'] == 2){
//                    $item['rest_click'] = $item['advertising']['click'] - $item['advertising']['user_click'];
//                }
            }
        }
        return $list;
    }
    
    /**
     * @desc  total
     * @param $user_id
     * @return float|int
     */
    public static function total($user_id)
    {
        return $total  = self::where(['user_id'=> $user_id,'type'=> ['in',[3,4]]])->sum('money');
    }
    
    /** 处理分享事件
     * @desc  calculate
     * @param $data
     * @return bool|void
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function calculate($data)
    {
        $referee_id = $data['referee_id'] ?? 0;
        
        $ad_id = $data['ad_id'] ?? 0;
        
        $user_id = $data['user_id'];

        if(!$referee_id ||!$ad_id || ($referee_id == $user_id)){
            return ;
        }

        $referee = User::get($referee_id);

        if(!$referee){
            return ;
        }

        $ad = Advertising::get($ad_id);
        
        if(!$ad){
            return ;
        }

        //增加曝光量
        (new Advertising())->addClick($ad,$data);

        //商业广告
        if($ad['ad_type'] == 1){
            $this->commission($ad,$data);
        }else{
            //公益广告赠送点击量
            $this->click($ad,$data);
        }


        return true;
        
    }

    /**
     * @param $ad
     * @param $data
     * @return bool
     * @throws \think\exception\PDOException
     */
    public function click($ad,$data){
        $cacheKey = 'click_referee_id_'.$data['referee_id'].'_user_id_'.$data['user_id'];

        if(!Cache::has($cacheKey)){
            //付费广告配置
            $config = Setting::getItem('ad_free');
//            print_r($config);exit;
            try{
                $this->startTrans();
                $user = User::get($data['user_id']);
                $user->setIncClick($config['user_click']);
                $referee = User::get($data['referee_id']);
                $referee->setIncClick($config['share_click']);
                $this->commit();
            }catch (\Exception $e){
                $this->rollback();
                return false;
            }

            Cache::set($cacheKey,1,$config['click_hour']*3600);

        }
    }



}