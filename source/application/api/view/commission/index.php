  <div id="commission" class="wrap-flex commision" v-cloak>
    <cube-scroll
      ref="scroll"
      :data="commissionList.data"
      :options="scrollOptions"
      class="wrap-flex-content wrap-nav-60"
      @pulling-up="scrollPullingUp"
    >
      <div class="main">
        <div class="input-group search-top">
          <input type="text" v-model="form.search" class="form-control" placeholder="输入名称进行搜索">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button" @click="search">
              <i class="glyphicon glyphicon-search"></i>
            </button>
          </span>
        </div>

        <segment-picker-date
          :value="timeDate"
          @on-select="timeOnSelectPicker"
          @on-search="search"
        ></segment-picker-date>

        <div class="task-commision flex-between">
          <span>总计：您的佣金为<span class="text-danger bold">{{commissionTotal}}元</span></span>

          <div>
            <button type="button" class="btn btn-primary" @click="applyWithdraw">申请提现</button>
            <a href="./index.php?s=/api/withdraw/index" class="btn btn-primary">提现记录</a>
          </div>
        </div>

        <div class="task-list">
          <task-item
            v-for="item in commissionList.data"
            :key="item.id"
            :data="item"
          ></task-item>
        </div>
      </div>
    </cube-scroll>
    <!-- 菜单栏 -->
    <footer-nav :active-index="3"></footer-nav>
  </div>

{{include file="layouts/_template/script"/}}

<script>
var commissionTotal = <?=$total?>;
var user = <?=$user?>;

</script>
<script type="text/javascript" src="static/js/commission.js?v=<?=$version?>"></script>
