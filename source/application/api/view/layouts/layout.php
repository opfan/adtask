<!DOCTYPE html>
<html lang="zh-CN">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title><?= $setting['store']['values']['name'] ?></title>
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
  <meta name="apple-mobile-web-app-title" content="<?= $setting['store']['values']['name'] ?>" />
  <link rel="stylesheet" href="static/css/cube.min.css">
  <link rel="stylesheet" href="static/css/bootstrap.min.css">
  <link rel="stylesheet" href="static/css/swiper5.min.css">
  <link rel="stylesheet" href="static/css/common.css?v=<?= $version ?>">
  <link rel="stylesheet" href="static/css/video-js.min.css">

  <script src="static/js/vue.min.js"></script>
  <!-- 开发环境版本，包含了有帮助的命令行警告 -->
  <!-- <script src="static/js/vue.js"></script> -->
  <script src="static/js/cube.min.js?v=<?= $version ?>"></script>
  <script src="static/js/httpVueLoader.js"></script>

  <script>
    BASE_URL = '<?= isset($base_url) ? $base_url : '' ?>';
    STORE_URL = '<?= isset($store_url) ? $store_url : '' ?>';
    USER_ID = '<?= isset($user_id) ? $user_id : '' ?>';
    STATIC_VERSION = '<?= isset($version) ? $version : '' ?>'
    USER_INFO = <?= isset($user) ? $user : '' ?>;
  </script>
</head>

<body data-type="">
  {__CONTENT__}
</body>

</html>
