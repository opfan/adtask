<script type="text/javascript" src="//res.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
<!-- 动态polyfill -->
<script type="text/javascript" src="static/js/polyfill-js/polyfill.min.js?features=Array.from%2CArray.isArray%2CArray.prototype.every%2CArray.prototype.find%2CArray.prototype.filter%2CArray.prototype.forEach%2CArray.prototype.includes%2CArray.prototype.indexOf%2CArray.prototype.keys%2CArray.prototype.map%2CArray.prototype.reduce%2CArray.prototype.values%2CArray.prototype.sort%2CArray.prototype.lastIndexOf%2CArray.prototype.findIndex%2CArray.prototype.some%2CNumber.isNaN%2CNumber.parseFloat%2CNumber.parseInt%2CObject.assign%2CObject.create%2CObject.defineProperties%2CObject.keys%2CObject.values%2CPromise%2CPromise.prototype.finally%2CString.prototype.includes%2CString.prototype.trim%2CString.prototype.trimEnd%2CString.prototype.trimStart%2Catob%2CBlob%2CHTMLCanvasElement.prototype.toBlob"></script>
<!-- <script type="text/javascript" src="static/js/polyfill.min.js"></script> -->
<script type="text/javascript" src="static/js/polyfill-js/index.js?v=<?=$version?>"></script>
<script type="text/javascript" src="static/js/fly.min.js"></script>
<script type="text/javascript" src="static/js/http.js"></script>
<script type="text/javascript" src="static/js/compressor.min.js"></script>
<script type="text/javascript" src="static/js/video.min.js"></script>
<script type="text/javascript" src="static/js/swiper5.min.js"></script>
<script type="text/javascript" src="static/js/emitter.js?v=<?=$version?>"></script>
<script type="text/javascript" src="static/js/common.js?v=<?=$version?>"></script>
