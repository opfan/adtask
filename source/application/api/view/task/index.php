<div id="business" class="wrap-flex" v-cloak>
  <cube-scroll class="wrap-flex-content wrap-nav-60" ref="scroll" :data="dataList.data" :options="scrollOptions" @pulling-up="scrollPullingUp">
    <div class="main">
      <div class="input-group search-top">
        <input type="text" v-model="form.search" class="form-control" placeholder="输入名称进行搜索">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button" @click="search">
            <i class="glyphicon glyphicon-search"></i>
          </button>
        </span>
      </div>

      <div class="tab tab-task flex-between">
        <button type="button" class="btn btn-default" :class="{'btn-primary': form.type === 1}" @click="search(1)">全部</button>
        <button type="button" class="btn btn-default" :class="{'btn-primary': form.type === 2}" @click="search(2)">已转发</button>
        <button type="button" class="btn btn-default" :class="{'btn-primary': form.type === 3}" @click="search(3)">未转发</button>
        <button type="button" class="btn btn-default" :class="{'btn-primary': form.type === 4}" @click="search(4)">已结束</button>
      </div>

      <div class="task-commision">
        <span>总计：您的佣金为<span class="text-danger bold">{{commissionTotal}}元</span></span>
      </div>

      <div class="task-list">
        <task-item v-for="item in dataList.data" :key="item.ad_id" :data="item">
          <template>
            <p class="task-item-info" v-if="item.click">
              <span class="info-label">需要曝光:</span>
              <span class="info-value text-danger">{{item.click}}次</span>
            </p>
            <p class="task-item-info">
              <span>
                <span class="info-label">你转发曝光:</span>
                <span class="info-value text-danger">{{item.share_time}}次</span>
              </span>
            </p>
            <p class="task-item-info" v-if="item.click">
              <span>
                <span class="info-label">现在还差曝光:</span>
                <span class="info-value text-danger">{{item.rest_click}}次</span>
              </span>
            </p>
<!--            <p class="task-item-info">-->
<!--              <span class="info-label">此次你获得的红包:</span>-->
<!--              <span class="info-value text-danger">{{item.user_money}}次</span>-->
<!--            </p>-->
            <p class="task-item-info">
              <span class="info-label">发布时间：{{item.create_time.split(' ')[0]}}</span>
            </p>
          </template>
          <template v-slot:right-top v-if="!item.is_end">
            <div class="btn-status">
              <share-button :info="item">
                <button type="button" class="btn btn-xs btn-status" :class="item.share_time
                ? ['btn-jxyel', 'joined']
                : ['btn-yel', 'unjoin']">{{ item.share_time ? '继续转发' : '转发' }}</button>
              </share-button>
            </div>
          </template>
        </task-item>
      </div>
    </div>
  </cube-scroll>
  <footer-nav :active-index="2"></footer-nav>
</div>
{{include file="layouts/_template/script"/}}

<script>
  // 总佣金
  var commissionTotal = <?= $total ?>;
  var user = <?=$user?>;
</script>

<script type="text/javascript" src="static/js/task.js?v=<?=$version?>"></script>
