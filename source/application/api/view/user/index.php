<div id="user" class="wrap-flex" v-cloak>
  <cube-scroll ref="scroll" :data="adListData.data" :options="scrollOptions" class="wrap-flex-content wrap-nav-60" @pulling-up="scrollPullingUp">
    <div class="center-top">
      <div class="media media-center">
        <div class="media-left">
          <img class="media-object center-avatar" :src="userInfo.avatarUrl" alt="">
        </div>
        <div class="media-body">
          <p class="user-name">{{userInfo.nickName}}</p>
          <p class="user-opts">
            <template v-if="loginUser">
              <a href="javascript:void(0)" class="user-edit" @click="showUserInfo">编辑/查看资料</a>
              <p class="user-balance text-warning">奖励红包(只能用于余额抵扣)：{{userInfo.balance}}</p>
            </template>
            <template v-else>
              <span class="user-edit">行业：{{userInfo.category}}</span>
            </template>
          </p>
        </div>
        <div v-if="loginUser" class="media-right">
          <img style="width: 60px; height: 60px;" :src="userInfo.qrcode" @click="showQrcode">
        </div>
      </div>
    </div>

    <div class="center-wallet" v-if="loginUser">
      <div class="flex">
        <div class="center-wallet-item" @click="skip('/index.php?s=/api/commission/index')">
          <p class="value-wallet">{{userInfo.money}}元</p>
          <p class="label-wallet">我的佣金</p>
        </div>
        <div class="center-wallet-item">
          <p class="value-wallet">{{userInfo.click}}次</p>
          <p class="label-wallet">可用曝光量</p>
        </div>
        <div class="center-wallet-item" @click="skip('/index.php?s=api/user/ad')">
          <p class="value-wallet">{{send_money}}元</p>
          <p class="label-wallet">已发红包</p>
        </div>
      </div>
      <div class="flex" style="margin-top: 10px;" v-if="loginUser">
        <div class="center-wallet-item" @click="skip('/index.php?s=/api/withdraw/index')">
            <p class="value-wallet">{{apply_money}}元</p>
            <p class="label-wallet">提现申请</p>
        </div>

        <div class="center-wallet-item" @click="skip('/index.php?s=/api/withdraw/index')">
            <p class="value-wallet">{{withdraw_money}}元</p>
            <p class="label-wallet">已提现</p>
        </div>

        <div class="center-wallet-item" @click="skip('/index.php?s=/api/user/child_index')">
            <p class="value-wallet">{{child_num}}人</p>
            <p class="label-wallet">我分享的下线人数</p>
        </div>
      </div>
    </div>
    <div class="center-task">
      <div class="center-head flex-between">
        <span class="center-head-title">最近发布信息</span>

        <a v-if="loginUser" href="/index.php?s=api/user/business" class="btn btn-primary btn-sm">查看商业广告投放信息</a>
      </div>

      <div class="ct-list">
        <div v-for="item in adListData.data" class="ct-item">
          <banner-swiper
            :uid="item.ad_id"
            :data="item"
            disabled-play
            image-preview
            :options="{
              init: true,
              autoplay: false,
            }" class="ct-item-pict">
            <template v-slot:right-bottom>
              <div class="banner-swiper__right-bottom">
                已曝光 {{item.share_click + item.user_click}} 次
              </div>
            </template>
          </banner-swiper>

          <div class="ct-item-info flex-between">
            <div class="ct-info-user flex">
              <img :src="userInfo.avatarUrl" alt="">

              <div class="ct-info-box flex-column">
                <p class="name">{{item.ad_name}}</p>
                <p class="p2">地址：{{getAddress(item)}}</p>
              </div>
            </div>

            <share-button :info="item"></share-button>
          </div>
        </div>
      </div>
    </div>
  </cube-scroll>

  <!-- 菜单栏 -->
  <footer-nav :active-index="4"></footer-nav>
</div>

<!-- 菜单栏 -->
{{include file="layouts/_template/script"/}}

<script>
  //标识是否当前登录用户 true 表示当前为登录用户页面 false表示其他人
  var login_user = <?= $login_user ?>;
  //用户信息 余额user['balance']，佣金 user['money']
  //var userInfo = <?//= $user ?>//;
  //提现申请
  var apply_money = <?= $apply_money ?>
  //已提现
  var withdraw_money = <?= $withdraw_money ?>
  //已发红包
  var send_money = <?= $send_money ?>
  //下线人数
  var child_num = <?= $child_num ?>

  var user = <?= $user ?>;

</script>
<script type="text/javascript" src="static/js/user.js?v=<?= $version ?>"></script>
