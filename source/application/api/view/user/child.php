  <div id="user-child" class="wrap-flex commision" v-cloak>
    <cube-scroll ref="scroll" :data="userChild.data" :options="scrollOptions" class="wrap-flex-content wrap-nav-60 main" @pulling-up="scrollPullingUp">
      <div class="task-commision flex-between">
        <span>
          您的线下成员：<span class="text-danger bold">{{child_num}}个</span>
        </span>
      </div>

      <div class="wd-list">
        <div v-for="list in userChild.data" :key="list.id" class="well wd-item user-child-item">
          <div class="flex-center user-child-item__info">
            <img :src="list.avatarUrl" :alt="list.nickName">
            <span>{{list.nickName}}</span>
          </div>
          <p>时间：{{list.referee_time}}</p>
        </div>
      </div>
    </cube-scroll>
    <!-- 菜单栏 -->
    <footer-nav :active-index="4"></footer-nav>
  </div>

  {{include file="layouts/_template/script"/}}

  <script>
    // 下线人数
    var child_num = <?= $child_num ?>;
  </script>

  <script type="text/javascript" src="static/js/user-child.js?v=<?= $version ?>"></script>
