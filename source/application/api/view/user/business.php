 <div id="business" class="wrap-flex" v-cloak>
   <cube-scroll ref="scroll" :data="dataList.data" :options="scrollOptions" class="wrap-flex-content wrap-nav-60" @pulling-up="scrollPullingUp">
     <div class="main">
       <div class="input-group search-top">
         <input v-model="form.search" type="text" class="form-control" placeholder="输入名称进行搜索">
         <span class="input-group-btn">
           <button class="btn btn-default" type="button" @click="search">
             <i class="glyphicon glyphicon-search"></i>
           </button>
         </span>
       </div>

       <div class="tab tab-task tab-business flex">
         <button type="button" class="btn btn-default" :class="{'btn-primary': form.type === 1}" @click="search(1)">全部</button>
         <button type="button" class="btn btn-default" :class="{'btn-primary': form.type === 2}" @click="search(2)">进行中</button>
         <button type="button" class="btn btn-default" :class="{'btn-primary': form.type === 3}" @click="search(3)">已结束</button>
       </div>

       <div class="task-list business-list">
         <task-item v-for="item in dataList.data" :key="item.ad_id" :data="item">
           <template>
             <p class="task-item-info" v-if="item.click">
              <span class="info-label">需要曝光:</span>
              <span class="info-value text-danger">{{item.click}}次</span>
             </p>
             <p class="task-item-info">
               <span>
                 <span class="info-label">平台内曝光:</span>
                 <span class="info-value text-danger">{{item.share_click}}次</span>
               </span>
             </p>
             <p class="task-item-info">
               <span>
                 <span class="info-label">用户转发:</span>
                 <span class="info-value text-danger">{{item.user_click}}次</span>
               </span>
             </p>
             <p class="task-item-info spec">
               <span class="info-label">投放人群:</span><br>
               <span class="sec-sm">
                 <span class="info-label">性别：</span>
                 <span class="info-value text-danger">{{ item.sex | sex}}</span>
               </span>
               <span class="sec-sm" v-if="item.industry !==undefined && item.industry.length > 0">
                 <span class="info-label">行业：</span>
                 <span class="info-value text-danger">{{item.industry.join()}}</span>
               </span>
             </p>
             <p class="task-item-info">
               <span class="info-label">发布时间：{{item.create_time.split(' ')[0]}}</span>
             </p>
           </template>
           <template v-slot:right-bottom>
            <button type="button" class="btn btn-success btn-sm btn-action" @click="showAdItem(item)">查看数据</button>
           </template>
         </task-item>
       </div>
     </div>
   </cube-scroll>
 </div>
 {{include file="layouts/_template/script"/}}

 <script type="text/javascript" src="static/js/business.js?v=<?=$version?>"></script>
