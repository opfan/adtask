<div id="hbsend" class="wrap-flex" v-cloak>
  <cube-scroll ref="scroll" :data="dataList.data" :options="scrollOptions" class="wrap-flex-content wrap-nav-60" @pulling-up="scrollPullingUp">
    <div class="main">
      <div class="input-group search-top">
        <input type="text" v-model="form.search" class="form-control" placeholder="输入名称进行搜索">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button" @click="search">
            <i class="glyphicon glyphicon-search"></i>
          </button>
        </span>
      </div>

      <segment-picker-date :value="timeDate" @on-select="timeOnSelectPicker" @on-search="search"></segment-picker-date>

      <div class="task-commision flex-between">
        <span>总计：您已投放<span class="text-danger bold">{{money_send}}元</span></span>
      </div>

      <div class="task-list">
        <task-item v-for="item in dataList.data" :key="item.ad_id" :show-name="false" :data="item">
          <template>
            <p class="task-item-info text-overflow">
              <span class="info-value title">{{item.ad_name}}</span>
            </p>
            <p class="task-item-info flex-between">
              <span>
                <span class="info-label">曝光:</span>
                <span class="info-value text-danger">{{item.user_click}}次</span>
              </span>

              <span>
                <span class="info-label">转发曝光:</span>
                <span class="info-value text-danger">{{item.share_click}}次</span>
              </span>
            </p>

            <p class="task-item-info">
              <span class="info-label">时间：{{item.create_time.split(' ')[0]}}</span>
            </p>

            <p class="task-item-info">
              <span class="info-label">投放红包：</span>
              <span class="info-value text-danger">{{item.money}}元</span>
            </p>
          </template>
          <template v-slot:right-bottom>
            <button type="button" class="btn btn-success btn-sm btn-action" @click="showAdItem(item)">查看数据</button>
          </template>
        </task-item>
      </div>
    </div>
  </cube-scroll>

  <!-- 菜单栏 -->
  <footer-nav :active-index="4"></footer-nav>
</div>

{{include file="layouts/_template/script"/}}

<script>
  var money_send = <?= $money ?>;
</script>
<script type="text/javascript" src="static/js/hb-send.js?v=<?=$version?>"></script>
