<div id="register" class="wrap" v-cloak>
  <div class="register flex-column">
    <!-- <h1>欢迎光临</h1> -->
    <h3 style="width: 120%;text-indent:2em;">尊敬的伙伴:</h3>
    <p class="p-title" style="width: 95%;text-indent:2em;">使用洛客外快可以<b class="b1">免费发广告</b>，还可以<br><b class="b2">做任务赚钱</b>，一个免费发广告/赚外快的平台。</p>
    <!-- <button type="button" class="btn btn-success btn-login" onClick="location.href='index.html'">微信登录</button>
        <div class="checkbox login-opts">
            <label class="l1">
                请阅读遵守使用规则
            </label>
        </div> -->

    <div class="register-box">
      <div class="register-item flex">
        <label for="job">从事行业：</label>
        <div class="register-switch">
          <div class="make" @click="switchCategory"></div>
          <cube-input :value="categorySwithText" readonly placeholder="选择从事行业"></cube-input>
        </div>
        <span class="tips text-danger">选填</span>
      </div>

      <div class="register-item flex">
        <label for="job">职位：</label>
        <div class="register-switch">
          <div class="make" @click="switchPosition"></div>
          <cube-input :value="positionSwithData.name" readonly placeholder="选择职位"></cube-input>
        </div>
        <span class="tips text-danger">选填</span>
      </div>
      <div class="checkbox login-opts" style="text-align: center;">
        <a class="l1" style="font-size: 12px;" href="">请阅读遵守使用规则（进入平台默认同意）</a>
      </div>
      <?php if (!empty($setting['activity']['values']['is_open'])) : ?>
        <p class="text-danger"><b>温馨提示</b>：您填写从事行业、职位可获得<?= $setting['activity']['values']['click'] ?>个广告曝光量</p>

      <?php endif; ?>
    </div>

    <cube-button @click="submit">提交信息</cube-button>
  </div>
</div>
{{include file="layouts/_template/script"/}}
<script>
  var redirect = "<?php echo htmlspecialchars_decode($redirect) ?>";
</script>
<script type="text/javascript" src="static/js/register.js?v=<?= $version ?>"></script>
