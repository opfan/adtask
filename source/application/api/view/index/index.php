<div id="home" class="wrap-flex" v-cloak>
  <!-- 首页内容 -->
  <div id="home-swiper" class="wrap-flex-content swiper-container home-container wrap-nav-60">
    <div class="swiper-wrapper">
      <div class="swiper-slide home-banner">
        <banner-swiper ref="adBannerRef" :uid="currentBannerAdId" :stop-status="bannerSwiperStop" :active-index="activeIndex" :wrap-content-h="wrapContentH" :data="bannerAdData.data" class="banner-swiper" @transition-end="transitionEnd">
        </banner-swiper>
        <banner-footer :info="currentBannerAdData"></banner-footer>
      </div>

      <!-- map克隆对象，固定底部 -->
      <nearby-map class="swiper-slide" style="height: auto;" @update="updateSwiper"></nearby-map>

      <div v-for="(item, key) in indexAdList" :key="key + 1" class="swiper-slide home-swiper-slide">
        <div class="ct-item">
          <template v-if="!key">
            <div class="index-head flex-between">
              <!-- <select id="location" class="form-control">
                <option value="厦门">厦门</option>
                <option value="漳州">漳州</option>
                <option value="泉州">泉州</option>
              </select> -->

              <a href="javascript:void(0)" class="text-tips"><b>如何赚得更多钱</b></a>
            </div>
          </template>
          <banner-swiper ref="adListRef" :uid="item.ad_id" :stop-status="bannerSwiperStop" :active-index="activeIndex" image-preview :wrapContentH="wrapContentH" :data="item" class="ct-item-pict">
            <template v-slot:right-bottom>
              <div class="banner-swiper__right-bottom">
                已曝光 {{item.share_click + item.user_click}} 次
              </div>
            </template>
          </banner-swiper>

          <div class="ct-item-info flex-between">
            <div class="ct-info-user flex">
              <img :src="item.avatarUrl" alt="">

              <div class="ct-info-box flex-column">
                <p class="name">{{item.ad_name}}</p>
                <p class="p2">地址：{{getAddress(item)}}</p>
              </div>
            </div>

            <share-button :info="item"></share-button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--关注微信公众号弹窗样式 -->
  <div v-show="!mapBox" style="background: rgba(0, 0, 0, 0.6);
    color: #fff;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 2;
    padding: 5px 10px;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;">
    为了您方便更快速做任务，赚钱，<br />请点击关注微信公众号
    <button style="background:#FF9900; color:#FFF;border-radius:5px;border:none; min-width: 50px;margin-left: auto;"><a href="">关注</a></button>
  </div>

  <!-- 菜单栏 -->
  <footer-nav></footer-nav>
</div>

<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=OnoveryOGtjRvGjOET1Zsk20"></script>
<!-- 菜单栏 -->
{{include file="layouts/_template/script"/}}
<script>
  //用户信息
  var user = <?= $user ?>;

  //是否新用户注册
  var register = <?= $register ?>;
  //弹窗提示的金额
  var money = <?= $money ?>;
</script>
<script type="text/javascript" src="static/js/home.js?v=<?= $version ?>"></script>
