  <div id="withdraw" class="wrap-flex commision" v-cloak>
  <cube-scroll
      ref="scroll"
      :data="withdrawList.data"
      :options="scrollOptions"
      class="wrap-flex-content wrap-nav-60 main"
      @pulling-up="scrollPullingUp"
    >
      <segment-picker-date
        :value="timeDate"
        @on-select="timeOnSelectPicker"
        @on-search="search"
      ></segment-picker-date>

      <div class="task-commision flex-between">
        <span>
          提现总计：您已经提现<span class="text-danger bold">{{withdraw_money}}元</span>
          <span>&nbsp;&nbsp;&nbsp;&nbsp;账户余额：<span class="text-danger bold">{{money}}元</span></span>
        </span>
      </div>

      <div class="wd-list">
        <div
          v-for="list in withdrawList.data"
          :key="list.id"
          class="well wd-item"
         >
          <p>提现：<span class="text-danger bold">{{list.apply_money}}元</span></p>
          <p>时间：{{list.create_time}}</p>
        </div>
      </div>
    </cube-scroll>
    <!-- 菜单栏 -->
    <footer-nav :active-index="3"></footer-nav>
  </div>
  {{include file="layouts/_template/script"/}}

  <script>
    //已提现
    var withdraw_money = <?=$withdraw_money?>;
    //余额
    var money = <?=$money?>;

    var user = <?=$user?>;
  </script>

  <script type="text/javascript" src="static/js/withdraw.js?v=<?= $version ?>"></script>
