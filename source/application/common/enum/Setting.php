<?php

namespace app\common\enum;

/**
 * 商城设置枚举类
 * Class Setting
 * @package app\common\enum
 */
class Setting extends EnumBasics
{
    // 商城设置
    const STORE = 'store';

    // 交易设置
    const TRADE = 'trade';

    // 付费广告设置
    const AD = 'ad';
    // 积分设置
    const AD_FREE = 'ad_free';

    // 提现设置
    const WITHDRAW = 'withdraw';

    // 活动设置
    const ACTIVITY = 'activity';
    /**
     * 获取订单类型值
     * @return array
     */
    public static function data()
    {
        return [
            self::STORE => [
                'value' => self::STORE,
                'describe' => '商城设置',
            ],
            self::TRADE => [
                'value' => self::TRADE,
                'describe' => '交易设置',
            ],

            self::ACTIVITY => [
                'value' => self::ACTIVITY,
                'describe' => '活动设置',
            ],

            self::AD => [
                'value' => self::AD,
                'describe' => '商业广告设置',
            ],
            self::AD_FREE => [
                'value' => self::AD_FREE,
                'describe' => '公益广告设置',
            ],
            self::WITHDRAW => [
                'value' => self::WITHDRAW,
                'describe' => '提现设置',
            ],
        ];
    }

}