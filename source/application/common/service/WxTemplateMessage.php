<?php


namespace app\common\service;


use app\api\model\Wxapp;
use EasyWeChat\Factory;
use think\Log;

class WxTemplateMessage extends Basics
{
    /**
     * @param $data
     * @return bool
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \think\exception\DbException
     */
    public static function notice($data)
    {

        $templeId = 'sR1AQyTknCsgA2O1_U1E5p5hYTW9gKzt2Yzhdu_SfWM';

        $config = Wxapp::getConfig();

        $app   = Factory::officialAccount($config);

        $result = $app->template_message->send([
            'touser' => $data['open_id'],
            'template_id' => $templeId,
            'url' => config('host_url').'index.php?s=api/commission/index',
            'data' => [
                'first' => [
                    'value' => '您有一笔收款已到账，请查收！',
                    'color' => '#173177'
                ],
                'keyword1' => [
                    'value' => '洛克外快',
                    'color' => '#173177'
                ],
                'keyword2' => [
                    'value' => $data['money'],
                    'color' => '#173177'
                ],
                'keyword3' => [
                    'value' => '洛克外快账号',
                    'color' => '#173177'
                ],
                'keyword4' => [
                    'value' => date('y-m-d H:i:s'),
                    'color' => '#173177'
                ],
                'remark' => [
                    'value' => '若有疑问请联系客服',
                    'color' => '#173177'
                ]
            ],
        ]);
        $msg = '========推送消息通知========='.PHP_EOL.'推送用户:'.$data['user_id'].'金额：'.$data['money'].PHP_EOL.'推送结果'.json_encode($result,JSON_UNESCAPED_UNICODE);

        Log::write($msg);

       return true;
    }
}