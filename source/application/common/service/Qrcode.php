<?php
/**
 * Desc : Qrcode.php
 * User : kjw
 * Date : 2021/2/19 9:42
 * File : Qrcode.php
 */

namespace app\common\service;

class Qrcode
{
    
    /**
     * @desc  code
     * @param $text
     * @param $user_id
     * @return string
     */
    public function code($text,$user_id){
       
        $pathname = WEB_PATH . 'uploads/qrcode/';
        
        if(!is_dir($pathname)) { //若目录不存在则创建之
            mkdir($pathname,0777,true);
        }
        
        $errorCorrectionLevel = 'H';  //容错级别
        $matrixPointSize = 10;      //生成图片大小
        //生成二维码图片
        $abc = $user_id.'.png';
        $filename = $pathname.$abc;
        \PHPQRCode\QRcode::png($text,$filename , $errorCorrectionLevel, $matrixPointSize, 2);
        
        $logo = WEB_PATH . 'logo.png'; //准备好的logo图片
        
        $QR = $filename;      //已经生成的原始二维码图
        
        if (file_exists($logo)) {
            $QR = imagecreatefromstring(file_get_contents($QR));    //目标图象连接资源。
            $logo = imagecreatefromstring(file_get_contents($logo));  //源图象连接资源。
            $QR_width = imagesx($QR);      //二维码图片宽度
            $QR_height = imagesy($QR);     //二维码图片高度
            $logo_width = imagesx($logo);    //logo图片宽度
            $logo_height = imagesy($logo);   //logo图片高度
            $logo_qr_width = $QR_width / 4;   //组合之后logo的宽度(占二维码的1/5)
            $scale = $logo_width/$logo_qr_width;  //logo的宽度缩放比(本身宽度/组合后的宽度)
            $logo_qr_height = $logo_height/$scale; //组合之后logo的高度
            $from_width = ($QR_width - $logo_qr_width) / 2;  //组合之后logo左上角所在坐标点
            //重新组合图片并调整大小
            /*
             * imagecopyresampled() 将一幅图像(源图象)中的一块正方形区域拷贝到另一个图像中
             */
            imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width,$logo_qr_height, $logo_width, $logo_height);
        }
        //输出图片
        imagepng($QR,$filename);
        
        return 'uploads/qrcode/'.$abc;
    }
}