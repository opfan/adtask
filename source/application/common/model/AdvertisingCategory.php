<?php

namespace app\common\model;

/**
 * 商家用户角色模型
 * Class UserRole
 * @package app\store\model\store
 */
class AdvertisingCategory extends BaseModel
{
    protected $name = 'advertising_category';
    protected $updateTime = false;
    
    public function category()
    {
        return $this->belongsTo('Category','category_id','category_id')
            ->bind(['name']);
    }
}