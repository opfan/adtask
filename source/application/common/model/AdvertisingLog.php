<?php
namespace app\common\model;


use app\api\model\Commission;

/**
 * Class Record
 * @package app\api\model
 */
class AdvertisingLog extends BaseModel
{
    protected $name = 'advertising_log';
    
    /**
     * @desc  getList
     * @param $ad_id
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($ad_id)
    {
        
        // 执行查询
        $list = $this->alias('al')
            ->field('u.nickName,u.avatarUrl,al.user_id,u.phone,count(u.user_id) as count')
            ->join('user u', 'al.user_id = u.user_id', 'left')
            ->where('al.ad_id',$ad_id)
            ->order('al.user_id desc')
            ->group('al.user_id')
            ->paginate(10, false, [
                'query' => \request()->request(),
            ]);

        if(!$list->isEmpty()){

            foreach ($list as &$user){
                
                $user['commission'] = (new Commission())->where(['type'=>3,'user_id'=> $user['user_id'],'ad_id'=> $ad_id])->sum('money');

            }
        }
        return $list;
    }
    
    /**
     * @desc  add
     * @param      $ad_id
     * @param      $user_id
     * @param int  $share_id
     * @return \app\common\model\AdvertisingLog
     */
    public static function add($ad_id, $user_id,$share_id = 0)
    {

        return self::create(['user_id' => $user_id,'ad_id' => $ad_id,'share_id'=> $share_id]);
    }

    /**
     * @param $user_id
     * @param $ad_id
     * @return int|string
     * @throws \think\Exception
     */
    public static function getUserShare($user_id,$ad_id)
    {
        return self::where('share_id',$user_id)->where('ad_id',$ad_id)->count();
    }
    

}
