<?php

namespace app\common\model;

/**
 * Class AdvertisingImage
 * @package app\common\model
 */
class AdvertisingUrl extends BaseModel
{
    protected $name = 'advertising_url';
    protected $updateTime = false;

    /**
     * 关联文件库
     * @return \think\model\relation\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('UploadFile', 'file_id', 'file_id')
            ->bind(['file_path', 'file_name', 'file_url','file_size']);
    }

}
