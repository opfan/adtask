<?php

namespace app\common\model\finance;

use app\common\model\Advertising;
use app\common\model\AdvertisingLog;
use app\common\model\BaseModel;
use app\common\model\Setting;
use app\common\model\User;
use app\common\service\WxTemplateMessage;
use think\Cache;
use think\Db;
use think\Log;

/**
 * Class Commission
 * @package app\common\model\finance
 */
class Commission extends BaseModel
{
    protected $name = 'Commission';


    /**
     * @return \think\model\relation\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('app\\common\\model\\User');
    }
    
    /**
     * @desc advertising
     * @return \think\model\relation\HasOne
     */
    public function advertising()
    {
        return $this->hasOne('app\\common\\model\\Advertising','ad_id','ad_id');
    }

    /**
     * @param $id
     * @return Commission|null
     * @throws \think\exception\DbException
     */
    public static function detail($id)
    {
        return self::get($id);
    }
    
    /**自动抢红包
     * @desc autoCommission
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function autoCommission()
    {
        //获取商业广告
        $adModel = new Advertising();

        $filter = [
            'virtual_money' => ['>',0],
            'pay_status'    => 20,
            'ad_type'       => 1,
            'origin'        => 1,
        ];

        $list = $adModel->where($filter)->select();

        if($list->isEmpty()){
           return true;
        }
        
        foreach ($list as $item){

            $userList = (new User())->where(['is_virtual'=> 1])->orderRaw('rand()')->limit(3)->select();
            
            foreach ($userList as $user){
                
                $ad = Advertising::get($item['ad_id']);
                $this->commission($ad,['referee_id'=> $user['user_id'],'ad_id'=> $item['ad_id']]);
                
            }
        }
    }

    /**
     * @param $data
     * @return false|int
     */
    public function add($data)
    {
        $insert = [
            'user_id' => $data['user_id'],
            'type'    => $data['type'] ?? 3,
            'ad_id'   => $data['ad_id'] ?? 0,
            'money'   => $data['money'],
            'parent_id'   => $data['parent_id'] ?? 0,
        ];

        return $this->allowField(true)->save($insert);
    }

    /**佣金、抽佣计算
     * @param $ad
     * @param $data
     * @return bool|int|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \think\exception\DbException
     */
    public function commission($ad,$data)
    {
        
        $cacheKey = 'commission_referee_id_'.$data['referee_id'].'_ad_id_'.($ad['ad_id']?? 0).'_user_id_'.($data['user_id']?? 0);

        if(!Cache::has($cacheKey)){

            //用户
            $user = User::get($data['referee_id']);
            //0 真实可领取 1 虚拟用户领取
            $map = [0 => 'real_money',1=> 'virtual_money'];
           
            //红包已被领取完
            if($ad[$map[$user['is_virtual']]] ==0){
                return false;
            }
            
            //付费广告配置
            $config = Setting::getItem('ad');
            
            //后台发布的广告
            if($ad['origin'] == 2){
                $rateConfig = $ad['extend']['commission'];
                
                if(empty($rateConfig)){
                    return false;
                }
                
                $total_rate = array_sum(array_column($rateConfig,'rate'));
                
                if( $total_rate< 100){
                    array_push($rateConfig,['money'=> 0,'rate'=> 100-$total_rate]);
                }
                
                $rateConfig = array_column($rateConfig,'rate','money');
                //根据概率获取金额
                $money = get_rand($rateConfig);
                
            }else{
                
                $key = mt_rand(1,$config['max_money']);
                //前台发布的广告
                $rateConfig = [ $key => $config['rate'],0 => 100 -$config['rate']];
                
                $money = get_rand($rateConfig);
            }
            
            if($money == 0){
                return false;
            }
            
            //可用余额小于赠送佣金
            if($money > $ad[$map[$user['is_virtual']]]){
                $money = $ad[$map[$user['is_virtual']]];
            }

            try{
                Db::startTrans();
                
                //用户已领取的金额增加
                $ad = \app\api\model\Advertising::get($data['ad_id']);
                
                $ad->setInc('user_money',$money);
                
                $ad->setDec($map[$user['is_virtual']],$money);

                //推荐人佣金
                $activity = Setting::getItem('activity');
                
                if($activity['is_open'] && !$user['is_virtual']){
                    if($user['referee_id']){
                        $refereeMoney = bcmul($activity['commission_rate']/100,$money,2);
                        $money = bcsub($money,$refereeMoney,2);
                        $referee = User::get($user['referee_id']);
                        //抽佣
                        $referee->setIncMoney($refereeMoney);
                        //记录到佣金表里
                        $this->add(array_merge($data,[
                            'user_id' => $user['referee_id'],
                            'type'  => 4,
                            'money' => $refereeMoney,
                            'parent_id' => $user['user_id'],
                        ]));
                        //推送消息通知
                        WxTemplateMessage::notice(['open_id'=> $referee['open_id'],'money'=> $refereeMoney,'user_id'=> $user['referee_id']]);
                    }
                    
                }

                //用户佣金增加
                $user->setIncMoney($money);
                //记录到佣金表里
                $result = (new static())->add(array_merge($data,[
                    'user_id' => $data['referee_id'],
                    'type'  => 3,
                    'money' => $money,
                    'share_id' => $data['user_id']?? 0,
                ]));
                if(!$user['is_virtual']){
                    //推送消息通知
                    WxTemplateMessage::notice(['open_id'=> $user['open_id'],'money'=> $money,'user_id'=> $user['user_id']]);
                }

                //自动抢红包，添加曝光量
                $user['is_virtual'] && AdvertisingLog::add($ad['ad_id'],$user['user_id']);

                Cache::set($cacheKey,1,$config['commission_hour']*3600);
                
                Db::commit();
            }catch (\Exception $e){
                Log::write('佣金记录失败,用户:'.($user['user_id']??'').'失败信息：'.$e->getMessage().'文件:'.$e->getFile().'行号：'.$e->getLine());
                Db::rollback();
                return false;
            }
            
            return $money;
        }

        return 0;
        
    }
    
    
    
    
    public function test()
    {
    
//        $restMoney = Advertising::where('ad_id',$item['ad_id'])->value('virtual_money');
//
//        $money = rand(1,$maxMoney);
//
//        if($money > $restMoney){
//            $money = $restMoney;
//        }
//
//        //红包领完跳出循环
//        if($restMoney <= 0){
//            break;
//        }
//        //广告红包减少
//        $item->setDec('virtual_money',$money);
//        //广告用户已领取增加
//        $item->setInc('user_money',$money);
//        //用户红包增加
//        $user->setIncMoney($money);
//        //添加曝光量
//        AdvertisingLog::add($item['ad_id'],$user['user_id']);
//
//        //添加佣金记录
//        (new static())->add([
//            'user_id' => $user['user_id'],
//            'type'    => 3,
//            'ad_id'   => $item['ad_id'],
//            'money'   => $money,
//        ]);
    }
}