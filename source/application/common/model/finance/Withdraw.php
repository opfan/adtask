<?php

namespace app\common\model\finance;

use app\common\model\BaseModel;
use app\common\model\User;

/**
 * 分销商提现明细模型
 * Class Apply
 * @package app\common\model\dealer
 */
class Withdraw extends BaseModel
{
    protected $name = 'withdraw';



    /**
     * 申请状态
     * @var array
     */
    public $applyStatus = [
        10 => '待结算',
        20 => '已结算',
    ];

    /**
     * 关联分销商用户表
     * @return \think\model\relation\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('app\\common\\model\\User');
    }
    
    /**
     * 关联文件库
     * @return \think\model\relation\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('app\\common\\model\\UploadFile', 'image_id', 'file_id')
            ->bind(['file_path', 'file_name', 'file_url']);
    }
    
    /**
     * @desc  detail
     * @param $id
     * @return \app\common\model\finance\Withdraw|null
     * @throws \think\exception\DbException
     */
    public static function detail($id)
    {
        return self::get($id);
    }

    /**
     * @param $where
     * @param string $field
     * @return float|int
     */
    public static function getUserDetail($where,$field = 'money')
    {
        //待结算、已结算
        return self::where($where)->sum($field);
    }
}