<?php

namespace app\common\model;
use app\common\service\Qrcode;

/**
 * 用户模型类
 * Class User
 * @package app\common\model
 */
class User extends BaseModel
{
    protected $name = 'user';

    // 性别
    private $gender = ['未知', '男', '女'];
    
    
    public function category()
    {
        return $this->hasOne('category','category_id','category_id');
        
    }
    
    public function position()
    {
        return $this->hasOne('position','position_id','position_id');
        
    }
    
    public static function init()
    {
        
        parent::init();
        // 监听订单处理事件
        $static = new static;
//        Hook::listen('order', $static);
        
        self::beforeInsert(function ($order) {
        
        });
        
        self::afterInsert(function ($order) {
            
            $data = $order->data;
           
            //注册生成二维码
            $url = config('host_url').'?invite_id='.$data['user_id'];
            
            $qrcode = (new Qrcode())->code($url,$data['user_id']);
            $user = self::get($data['user_id']);
            $user->save(['qrcode'=> $qrcode]);
        });
        
        self::beforeUpdate(function ($user) {
//            print_r($user);
            $data    = $user->data;
            $oldData = $user->origin;

            if(!empty($data['longitude']) && !empty($data['latitude']) && (($oldData['longitude'] != $data['longitude'])  || ($oldData['latitude'] != $data['latitude']))&& $data['longitude'] >0 && $data['latitude'] >0 ){
               $insert =  (new static())->getLocation($data);
               if(!empty($insert)) {
                   $user->country = $insert['country'];
                   $user->province = $insert['province'];
                   $user->city = $insert['city'];
                   $user->region = $insert['region'];
                   $user->address = $insert['address'];
               }
            }
        });
        
        self::afterUpdate(function ($order) {
            
            $data = $order->data;
            
            $oldData = $order->origin;
            
           
            
        });
    }

    /**
     * @param $data
     * @return array
     */
    public function getLocation($data)
    {
        $url  = "https://apis.map.qq.com/ws/geocoder/v1/?location=".$data['latitude'].','.$data['longitude']."&key=S3EBZ-3GGKX-EYQ4P-TOWIK-PZXTJ-ZABNI";

        $result = curl($url);
        $result = json_decode($result,'true');
//        print_r($result);exit;
        if(isset($result['status']) && $result['status'] == 0 && !empty($result['result'])){
            $address = $result['result']['address_component']?? [];
            if(!empty($address)){
                $insert['country'] = $address['nation']??'';
                $insert['province'] = $address['province']??'';
                $insert['city'] = $address['city']?? '';
                $insert['region'] = $address['district']?? '';
                $insert['address'] = ($address['street']??'');
            }

        }

        return $insert ?? [];
    }
    /**
     * @desc  getQrcodeAttr
     * @param $value
     * @param $data
     * @return string
     * @throws \think\exception\DbException
     */
    public function getQrcodeAttr($value,$data)
    {
        if(!is_file(WEB_PATH.$value)){
            //注册生成二维码
            $url = config('host_url').'?invite_id='.$data['user_id'];
            $qrcode = (new Qrcode())->code($url,$data['user_id']);
            $user = self::get($data['user_id']);
            
            $user->save(['qrcode'=> $qrcode]);
        }
        
        return config('host_url').$value;
    }
    /**
     * @param $value
     * @param $data
     * @return string
     */
    public function getUseBalanceAttr($value,$data)
    {
        $config = Setting::getItem('activity');
        $balance = bcmul($data['balance'],$config['rate']/100,2);
        return bcadd($balance,$data['money'],2);
    }
    /**
     * 显示性别
     * @param $value
     * @return mixed
     */
    public function getGenderAttr($value)
    {
        return $this->gender[$value];
    }
    
    /**
     * @desc  getRefereeTimeAttr
     * @param $value
     * @return false|int|string
     */
    public function getRefereeTimeAttr($value)
    {
        return $value> 0 ? date('Y-m-d H:i:s') : 0;
    }
    
    /**
     * @desc getAvatarUrlAttr
     * @param $value
     * @param $data
     * @return string
     */
    public function getAvatarUrlAttr($value,$data)
    {
        
        if(empty($data['avatarUrl'])){
            
            return config('host_url').'avatar/1.png';
            
        }
       
        return !empty(strstr($data['avatarUrl'],'qlogo.cn')) ? $data['avatarUrl'] : config('host_url').$data['avatarUrl'];
        
    }
    /**
     * 获取用户信息
     * @param $where
     * @param $with
     * @return null|static
     * @throws \think\exception\DbException
     */
    public static function detail($where, $with = [])
    {
        $filter = ['is_delete' => 0];
        if (is_array($where)) {
            $filter = array_merge($filter, $where);
        } else {
            $filter['user_id'] = (int)$where;
        }
        return static::get($filter, $with);
    }
    

    /**
     * 累积用户总消费金额
     * @param $money
     * @return int|true
     * @throws \think\Exception
     */
    public function setIncPayMoney($money)
    {
        return $this->setInc('pay_money', $money);
    }

    /**
     * 累积用户实际消费的金额 (批量)
     * @param $data
     * @return array|false
     * @throws \Exception
     */
    public function onBatchIncExpendMoney($data)
    {
        foreach ($data as $userId => $expendMoney) {
            $this->where(['user_id' => $userId])->setInc('expend_money', $expendMoney);
        }
        return true;
    }


    /** 用户曝光量
     * @param $click
     * @return int|true
     * @throws \think\Exception
     */
    public function setDecClick($click)
    {
        return $this->setDec('click', $click);
    }

    /** 用户曝光量
     * @param $click
     * @return int|true
     * @throws \think\Exception
     */
    public function setIncClick($click)
    {
        return $this->setInc('click', $click);
    }

    /**系统红包
     * @param $balance
     * @return int|true
     * @throws \think\Exception
     */
    public function setDecBalance($balance)
    {
        return $this->setDec('balance', $balance);
    }

    /**
     * @param $balance
     * @return int|true
     * @throws \think\Exception
     */
    public function setIncBalance($balance)
    {
        return $this->setInc('balance', $balance);
    }

    /**佣金
     * @param $money
     * @return int|true
     * @throws \think\Exception
     */
    public function setIncMoney($money)
    {
        return $this->setInc('money', $money);
    }

    /**佣金
     * @param $money
     * @return int|true
     * @throws \think\Exception
     */
    public function setDecMoney($money)
    {
        return $this->setDec('money', $money);
    }


    /**
     * @desc makeAvatar
     */
    public function makeAvatar()
    {
        $file = $this->countFile('/avatar');
        
        $dir = '/avatar/'.rand(1,$file).'.png';

        return $dir;
    }
    
    
    /**
     * @desc  countFile
     * @param $ff
     * @return int
     */
    private function countFile($ff)
    {
        
        $dir    = WEB_PATH. $ff;
        $handle = opendir($dir);
        $i      = 0;
        while (false !== $file = (readdir($handle))) {
            if ($file !== '.' && $file != '..') {
                $i++;
            }
        }
        closedir($handle);
        
        return $i;
    }


}
