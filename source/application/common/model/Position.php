<?php

namespace app\common\model;

/**
 * Class Position
 * @package app\common\model
 */
class Position extends BaseModel
{
    protected $name = 'position';

    /**
     * @param $param
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($param)
    {
        // 商品列表获取条件
        $params = array_merge([
            'search' => '',         // 搜索关键词
            'listRows' => 15,       // 每页数量
        ], $param);
        // 筛选条件
        $filter = [];
        !empty($params['search']) && $filter['name'] = ['like', '%' . trim($params['search']) . '%'];

        // 执行查询
        $list = $this
            ->field('*')
            ->where('is_delete', '=', 0)
            ->paginate($params['listRows'], false, [
                'query' => \request()->request()
            ]);

        return $list;
    }

    /**
     * @param $id
     * @return Position|null
     * @throws \think\exception\DbException
     */
    public static function detail($id)
    {
        return self::get($id);
    }
}
