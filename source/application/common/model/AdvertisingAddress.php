<?php

namespace app\common\model;

/**
 * Class AdvertisingPosition
 * @package app\common\model
 */
class AdvertisingAddress extends BaseModel
{
    protected $name = 'advertising_address';

    /**
     * @param $value
     * @param $data
     * @return string
     */
    public function getPointAttr($value,$data)
    {
        return $data['longitude'].','.$data['latitude'];
    }

    /**
     * @param $value
     * @param $data
     * @return string
     */
    public function getProvinceAttr($value,$data)
    {
        if(empty($data['province_id'])){
            return ;
        }
        return Region::getNameById($data['province_id']);
    }

    /**
     * @param $value
     * @param $data
     * @return string
     */
    public function getCityAttr($value,$data)
    {
        if(empty($data['city_id'])){
            return ;
        }
        return Region::getNameById($data['city_id']);
    }

    /**
     * @param $value
     * @param $data
     * @return string
     */
    public function getRegionAttr($value, $data)
    {
        if(empty($data['region_id'])){
            return ;
        }
        return $data['region_id'] == 0 ? '' : Region::getNameById($data['region_id']);
    }
}