<?php

namespace app\common\model;

use app\store\model\AdvertisingAddress;
use app\store\model\AdvertisingCategory;
use app\store\model\AdvertisingPosition;

/**
 * Class Goods
 * @package app\common\model
 */
class Advertising extends BaseModel
{
    protected $name = 'advertising';

    protected $type = ['extend' => 'array'];
    
    const AD_TYPE = [1,2];
    /**
     * @var array 配置映射字段
     */
    protected $configMpa = [1=> 'ad',2=>'ad_free'];
    //支付方式 10微信 20余额
    const PAY_TYPE = [10,20];

//    protected $append = [
//        'thumb'
//    ];
    

    /**
     * @param $value
     * @return array
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '未审核', 20 => '审核通过',30 => '审核失败'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * @param $value
     * @return array
     */
    public function getStateAttr($value)
    {
        $status = [10 => '上架', 20 => '下架'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * @return \think\model\relation\HasMany
     */
    public function category()
    {
        return $this->hasMany('AdvertisingCategory','ad_id','ad_id');
    }

    /**
     * @return \think\model\relation\HasMany
     */
    public function position()
    {
        return $this->hasMany('AdvertisingPosition','ad_id','ad_id');
    }

    public function address()
    {
        return $this->hasMany('AdvertisingAddress','ad_id','ad_id');
    }
    /**
     * @return \think\model\relation\HasMany
     */
    public function url()
    {
        return $this->hasMany('AdvertisingUrl','ad_id','ad_id')->order(['id' => 'asc']);
    }

    /**
     * @return \think\model\relation\HasOne
     */
    public function user()
    {
        return $this->hasOne('User','user_id','user_id')->field('user_id,nickName,avatarUrl');
    }
    
    /**
     * @desc file
     * @return \think\model\relation\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('UploadFile', 'thumb_id','file_id')
            ->bind(['thumb_path']);
    }
    
    /**
     * @param $param
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($param)
    {
        // 商品列表获取条件
        $params = array_merge([
            'category_id' => 0,     // 分类id
            'position_id' => 0,     // 职业id
            'state' => -1,          // 上下架
            'status' => -1,          // 上下架
            'search' => '',         // 搜索关键词
            'listRows' => 15,       // 每页数量
            'pay_status' => 20,
        ], $param);
        // 筛选条件
       
        $filter = [];
        
        $params['status'] > 0 && $filter['ad.status'] = $params['status'];
        $params['state'] > 0 && $filter['ad.state'] = $params['state'];

        !empty($params['search']) && $filter['ad.ad_name|ad.user_name'] = ['like', '%' . trim($params['search']) . '%'];
    
        //行业
        if(!empty($param['origin'])){
            $filter['ad.origin'] = $param['origin'];
        }
    
        //行业
        if(!empty($param['ad_type'])){
            $filter['ad.ad_type'] = $param['ad_type'];
        }
        //职业
        if(!empty($param['position_id'])){
            $filter['p.position_id'] = $param['position_id'];
        }
        
        //行业
        if(!empty($param['category_id'])){
            $filter['c.category_id'] = $param['category_id'];
        }
    
   
        if(!empty($param['start_time'])){
            $filter['ad.stime'][] = ['>',strtotime($param['start_time'])];
        }
    
        if(!empty($param['end_time'])){
            $filter['ad.stime'][] = ['<',strtotime($param['end_time'])+ 86400,'AND'];
        }
        
        $filter['pay_status'] = 20;
        
        // 执行查询
        $list = $this->alias('ad')
            ->field('ad.*')
            ->with(['category','position', 'url.file','file'])
            ->join('advertising_position p','p.ad_id = ad.ad_id','left')
            ->join('advertising_category c','c.ad_id = ad.ad_id','left')
            ->join('advertising_address aa','aa.ad_id = ad.ad_id','left')
            ->where($filter)
            ->order('ad.ad_id desc')
            ->group('ad.ad_id')
            ->paginate($params['listRows'], false, [
                'query' => \request()->request()
            ]);

        return $list;
    }
    
    /**
     * @desc  detail
     * @param         $adId
     * @param string  $field
     * @return \app\common\model\Advertising
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function detail($adId,$field = '*')
    {
        /* @var $model self */
        $model = (new static)->field($field)->with([
            'category.category',
            'position.position',
            'address',
            'url.file',
            'file'
        ])->where('ad_id', '=', $adId)
            ->find();

        // 整理商品数据并返回
        return $model;
    }

    /**
     * @param $data
     * @return bool
     */
    public function audit($data)
    {
//        if ($data['apply_status'] == '30' && empty($data['reject_reason'])) {
//            $this->error = '请填写驳回原因';
//            return false;
//        }
        // 更新申请记录
        $data['audit_time'] = time();
        $this->allowField(true)->save($data);
        return true;
    }

    /**
     * 软删除
     * @return false|int
     */
    public function setDelete()
    {
        if ($this->status['value'] == 20) {
            $this->error = '广告审核通过不能删除';
            return false;
        }
        return $this->allowField(true)->save(['is_delete' => 1]);
    }

    /**
     * @param $state
     * @return bool
     */
    public function setStatus($state)
    {
        return $this->allowField(true)->save(['state' => $state ? 10 : 20]) !== false;
    }
    
    /** 点击量
     * @desc  setIncClick
     * @param string  $field
     * @param int     $num
     * @return int|true
     * @throws \think\Exception
     */
    public function setIncClick($field = 'user_click',$num = 1)
    {
        return $this->setInc($field, $num);
    }
    
    /** 用户领取红包金额
     * @desc  setIncUserMoney
     * @param $money
     * @return int|true
     * @throws \think\Exception
     */
    public function setIncUserMoney($money)
    {
        return $this->setInc('user_money', $money);
    }

    /**
     * 生成订单号
     * @return string
     */
    public static function createAdNo()
    {
        return date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
    }

    /**
     * @desc  add
     * @param $data
     * @return bool
     * @throws \think\exception\PDOException
     */
    public function add($data)
    {

        $configMap = [1=> 'ad',2=>'ad_free'];

        $ad_config = Setting::getItem($configMap[$data['ad_type']]);

        if($data['ad_type'] == 1){
            //前台发布的商业广告分配红包金额
            if($data['origin'] == 1){
                $data['virtual_money'] = bcmul($data['money'],$ad_config['money_rate']/100,2);
    
                $data['real_money'] = bcsub($data['money'],$data['virtual_money'],2);
            }else{
                $data['virtual_money'] = 0;
                $data['real_money'] = $data['money'];
            }
        }
        
        if(empty($data['stime'])){
            $data['stime'] = time();
        }
        
        $activity = Setting::getItem('activity');

        $data['extend'] = array_merge($data['extend'] ?? [],['config'=> compact('activity','ad_config')]);

        $this->startTrans();
        try {
            $data['ad_no']= self::createAdNo();
            // 添加
            $this->allowField(true)->save($data);
            // 分类
            !empty($data['category']) &&  (new AdvertisingCategory())->add($this['ad_id'],$data['category']);
            //职位
            !empty($data['position']) && (new AdvertisingPosition())->add($this['ad_id'],$data['position']);
            //地址
            (new AdvertisingAddress())->add($this['ad_id'],$data['address']);
        
            // 广告
            $this->addUrl($data['urls']);

            //红包支付
            if($data['origin']==1){
                $this->pay($data);
            }

            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            $this->rollback();
            return false;
        }
        
    }

    /**
     * @param $data
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    private function pay($data)
    {
        $user = \app\api\model\User::detail($data['user_id']);
        //红包支付扣除佣金
        if($data['ad_type'] == 1 && $data['pay_type'] == 20){
            $config = Setting::getItem('activity');
            $balance = ($config['rate']/100)*$user['balance'];

            $balance_pay = ($balance >= $data['money']) ? $data['money'] : $balance;
            $user->setDecBalance($balance_pay);
            $money_pay = ($balance >= $data['money'])? 0 : bcsub($data['money'],$balance_pay,2);
            $user->setDecMoney($money_pay);
        }
        //公益广告扣除曝光量
        if($data['ad_type'] == 2){
            $user->setDecClick($data['click']);
        }
        return true;

    }
    /**
     * @param $adUrl
     * @return int
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    protected function addUrl($adUrl)
    {
        $this->url()->delete();
        $data = array_map(function ($adUrl) {
            return [
                'file_id' => $adUrl['file_id'],
                'ad_url'  => $adUrl['ad_url'] ?? ''
            ];
        }, $adUrl);
        return $this->url()->saveAll($data);
    }
}
