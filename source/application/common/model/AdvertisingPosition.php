<?php

namespace app\common\model;

/**
 * Class AdvertisingPosition
 * @package app\common\model
 */
class AdvertisingPosition extends BaseModel
{
    protected $name = 'advertising_position';
    protected $updateTime = false;
    
    public function position()
    {
        return $this->belongsTo('Position','position_id','position_id')
            ->bind(['name']);
    }
}