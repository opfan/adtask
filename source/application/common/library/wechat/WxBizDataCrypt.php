<?php
/**
 * Desc : WxBizDataCrypt.php
 * User : kjw
 * Date : 2019/12/20 14:00
 * File : WxBizDataCrypt.php
 */

namespace app\common\library\wechat;

/**
 * 微信解码
 * Class WxBizDataCrypt
 * @package app\common\library\wechat
 */
class WxBizDataCrypt extends WxBase
{

    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param  $sessionKey string
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData($sessionKey,$encryptedData, $iv)
    {
        $aesKey = base64_decode($sessionKey);

        if (strlen($iv) != 24) {

            return false;

        }

        $aesIV     = base64_decode($iv);
        $aesCipher = base64_decode($encryptedData);
        $result    = openssl_decrypt($aesCipher, 'AES-128-CBC', $aesKey, 1, $aesIV);

        $dataObj = json_decode($result);

        if ($dataObj == NULL) {

            return false;

        }

        if ($dataObj->watermark->appid != $this->appId) {

            return false;

        }

        return json_decode($result, true);
    }

}